const cluster = require('cluster');
const RenderCache = plugins.require('vue/RenderCache');
const fs      = require('fs');
const Path    = require('path');
const subConsole    = console.create('build-app');

function putCss(html) {
    const css = fs.readFileSync(Path.join(process.cwd(), 'dist', 'bundle.css')).toString();
    return html.replace("<link href='/css/bundle.css' rel='stylesheet' type='text/css' />", `<style>${css}</style>`);
}

function putJs(html) {
    const content = fs.readFileSync(Path.join(process.cwd(), 'dist', 'bundle-client.js')).toString();
    return html.replace('<script src="/lib/bundle.js"></script>', `<script>${content}</script>`);
}

function buildHtml() {
    subConsole.info('building html')
    const cache = new RenderCache(null);
    
    cache.renderToString('/').then((html) => {
        if(html.httpCode !== 200)
            throw(html);

        const server = process.env['my-ip'] ? `http://${process.env['my-ip']}:${process.env['platform-port']}/` : 'https://www.bit4you.io/';
        html = putCss(putJs(html.html));
        
        fs.writeFileSync(Path.join(process.cwd(), '..', 'bit4you-app', 'src', 'screens', 'market', 'chart', 'index.html'), html);
        subConsole.success('Html bundle done, length: ' + html.length);
    }).catch((err) => subConsole.error(err));
}

module.exports = function(value) {

    if(!cluster.isMaster)
        return true;
    
    var bundleDone = false;

    cluster.fork({ cli: JSON.stringify({ build: 'client', production: true }) });
    
    cluster.on('exit', (worker, code, signal) =>
    {
        if (code !== 0)
            process.exit(code);
        
        if(!bundleDone) {
            bundleDone = true;
            buildHtml();
        }
    });

    return false;
}