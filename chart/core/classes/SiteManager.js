const request    = require('request-promise');

class SiteManager extends SuperClass {
    
    api(name, post) {
        const serverUrl = this.getConfig('server')['api-server'];

        if(!this.cookies)
            this.cookies = request.jar();
        
        return request({
                uri:    serverUrl + 'api/' + name,
                json:   true,
                method: 'POST',
                body:  post,
                jar:   this.cookies
            })
    }
 
    preHandle(req, res, prePath)
    {
        if(prePath !== 'api')
            return super.preHandle(req, res, prePath);

        this.server.parseBody(req, res, () => {
            this.api(req.url.substr(5), req.body).then(function(remote) {
                res.writeHead(200, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify(remote));
            }).catch(function(remote) {
                res.writeHead(remote.statusCode, { 'Content-Type': remote.response.headers['content-type'] });
                res.end(JSON.stringify(remote.response.body));
            })
        });
        
        return true;
    }
    
}

module.exports = SiteManager;