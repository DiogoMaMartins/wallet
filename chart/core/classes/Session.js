class Session extends SuperClass {
    
    handleSocketMessage(socket, message) {
        super.handleSocketMessage(socket, message);

        if (message.api) {
            this.siteManager.api(message.api, message.data).then((result) => {
                
                this.sendSocketMessage(socket, {
                    api: message.api,
                    data: result,
                    salt: message.salt
                });
    
            }).catch((remote) => {
                this.sendSocketMessage(socket, {
                    apiError: message.api,
                    error: remote.response ? remote.response.body : { error: remote.message },
                    salt: message.salt
                });
                
            });

        }
    }
    
}

module.exports = Session;