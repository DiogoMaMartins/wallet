export default {
    
  //config = {
  //  type: 'AMA',
  //  color: null,
  //  mapping: null
  //}
  
  setup(chart, dataTable, config) {
      var mapping = config.mapping;
      
      if(!mapping)
          mapping = dataTable.mapAs({
            'high': 'high',
            'low': 'low',
            'open': 'open',
            'close': 'close',
            'value': '"close"',
          });

      const plot = chart.$createPlot();
      plot.legend(true);
      plot.height(config.height || "120px");

      plot.yAxis(1).orientation('right');

      var Stochastic = plot.stochastic(
        mapping,
        config['%k Period'] || 40,
        config['%k MA Period'] || 10,
        config['%d Period'] || 5,
        String(config['%k MA Type'] || 'ema').toLowerCase(),
        String(config['%d MA Type'] || 'ema').toLowerCase(),
        'line',
        'line'
      );

      Stochastic.kSeries().stroke(`1.5 ${config['%k Color'] || '#e24b26'}`).tooltip(false);
      Stochastic.dSeries().stroke(`1.5 ${config['%d Color'] || '#6e9c4e'}`).tooltip(false);

      return { plots: [plot], series: [Stochastic] };
  },

  title: 'Stochastic (%K and %D)',

  dialogConfig: {
      '%k Period': 14,
      '%k MA Period': 1,
      '%d Period': 3,
      '%k MA Type': [ 'EMA', 'SMA' ],
      '%d MA Type': [ 'EMA', 'SMA' ],
      '--1': '--DIVIDER--',
      '%k Color': '#e24b26',
      '%d Color': '#6e9c4e',
  }
}