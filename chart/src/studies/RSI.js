export default {
    
    //config = {
    //  type: 'RSI',
    //  plotIndex: null,
    //  mapping: null,
    //  height: null
    //}
    
    mappingType() {
        return 'v4'; //{'value': 4}
    },
    
    setup(chart, dataTable, config) {
        //ToDo show zones

        var mapping = config.mapping;
        
        if(!mapping)
            mapping = dataTable.mapAs({'value': 'close'});

        // MACD is usually displayed on a separate plot
        const plot = chart.$createPlot();
        plot.legend(true);
        plot.height(config.height || "120px");

        // create RSI indicator with period 14
        var rsi = plot.rsi(mapping, config.Period || 14).series().stroke(config.RSI || '#2c3e50').tooltip(false);

        const RSIData = rsi.data();
        const RSIDataIterator = RSIData.createSelectable().getIterator();

        // ---------------------------------------------------------------------

        // Oversold / Overbought
        var computer = dataTable.createComputer(mapping);
        const uuid = Math.floor(new Date().getTime() + (Math.random() * 100000));
        computer.setContext({
          RSIDataIterator,
          uuid,
        });
        computer.setStartFunction((context) => {
          context.index = 0;
        });
        computer.setCalculationFunction((row, context) => {
            if (context.index === 0) {
              context.RSIDataIterator.reset();
            }
            context.index += 1;
            context.RSIDataIterator.advance();
            const RSI = NaN || context.RSIDataIterator.get('value');
            row.set('computed_overbought_line'+context.uuid, config['OverBought'] || 80);
            row.set('computed_oversold_line'+context.uuid, config['Oversold'] || 20);
            row.set('computed_overbought_high'+context.uuid, RSI >= (config['OverBought'] || 80) ? RSI : NaN);
            row.set('computed_overbought_low'+context.uuid, (config['OverBought'] || 80));
            row.set('computed_oversold_high'+context.uuid, (config['Oversold'] || 20));
            row.set('computed_oversold_low'+context.uuid, RSI <= (config['Oversold'] || 20) ? RSI : NaN);
        });
        computer.addOutputField('computed_overbought_line'+uuid, 'overbought_line'+uuid);
        computer.addOutputField('computed_oversold_line'+uuid, 'oversold_line'+uuid);
        computer.addOutputField('computed_overbought_high'+uuid, 'overbought_high'+uuid);
        computer.addOutputField('computed_overbought_low'+uuid, 'overbought_low'+uuid);
        computer.addOutputField('computed_oversold_high'+uuid, 'oversold_high'+uuid);
        computer.addOutputField('computed_oversold_low'+uuid, 'oversold_low'+uuid);
        // Line Overbought
        var mappingOverbought = dataTable.mapAs({ 'value': 'overbought_line'+uuid });
        var lineOverbought = plot.line(mappingOverbought);
        lineOverbought.name('Rsi Overbought Limit');
        lineOverbought.stroke(config['OverBoughtColor']);
        lineOverbought.tooltip(false);
        lineOverbought.legendItem(false);

        // Line Oversold
        var mappingOversold = dataTable.mapAs({ 'value': 'oversold_line'+uuid });
        var lineOversold = plot.line(mappingOversold);
        lineOversold.name('Rsi Oversold Limit');
        lineOversold.stroke(config['OverSoldColor']);
        lineOversold.tooltip(false);
        lineOversold.legendItem(false);

        if (config['Show Zones']) {
          // Range Overbought (Fill)
          var mappingOverboughtRange = dataTable.mapAs({ 'x': 'x', high: 'overbought_high'+uuid, low: 'overbought_low'+uuid });
          var rangeOverbought = plot.rangeSplineArea(mappingOverboughtRange);
          rangeOverbought.name('Rsi Overbought');
          rangeOverbought.fill(config['OverBoughtColor']);
          rangeOverbought.tooltip(false);
          rangeOverbought.legendItem(false);
  
          // Range Oversold (Fill)
          var mappingOversoldRange = dataTable.mapAs({ 'x': 'x', high: 'oversold_high'+uuid, low: 'oversold_low'+uuid });
          var rangeOversold = plot.rangeSplineArea(mappingOversoldRange);
          rangeOversold.name('Rsi Oversold');
          rangeOversold.fill(config['OverSoldColor']);
          rangeOversold.tooltip(false);
          rangeOversold.legendItem(false);
        }

        return { plots: [plot], series: [rsi, lineOverbought, lineOversold, rangeOverbought, rangeOversold] };
    },

    title: 'Relative Strength Index (RSI)',
    dialogConfig: {
        'Period': 14,
        '--1': '--DIVIDER--',
        'RSI': '#2c3e50',
        'Show Zones': true,
        '--2': '--DIVIDER--',
        'OverBought': 80,
        'OverBoughtColor': '#c0392b',
        'OverSold': 20,
        'OverSoldColor': '#27ae60'
    }
}