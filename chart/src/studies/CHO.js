export default {
    
  //config = {
  //  type: 'AMA',
  //  color: null,
  //  mapping: null
  //}
  
  setup(chart, dataTable, config) {
      var mapping = config.mapping;
      
      if(!mapping)
          mapping = dataTable.mapAs({
            'high': 'high',
            'low': 'low',
            'open': 'open',
            'close': 'close',
            'volume': 'volume',
            'value': 'close',
          });

      const plot = chart.$createPlot();
      plot.legend(true);
      plot.height(config.height || "120px");

      const type = String(config['Show As'] || 'area').toLowerCase();
      var CHO = plot.cho(
        mapping,
        config['Fast Period'] || 3,
        config['Slow Period'] || 10,
        String(config['MA Type'] || 'ema').toLowerCase(),
        String(config['Show As'] || 'area').toLowerCase(),
        type
      ).series().tooltip(false).stroke(`1.5 ${config.Color || '#9b59b6'}`).fill({
        color: (type === 'area') ? (config.Color || '#9b59b6') : 'transparent',
        opacity: (type === 'area') ? 0.3 : 0,
      });

      return { plots: [plot], series: [CHO] };
  },

  title: 'Chaikin Oscillator (CHO)',

  dialogConfig: {
      'Fast Period': 3,
      'Slow Period': 10,
      'MA Type': [ 'EMA', 'SMA' ],
      'Show As': [ 'AREA', 'LINE' ],
      '--1': '--DIVIDER--',
      'Color': '#9b59b6',
  }
}