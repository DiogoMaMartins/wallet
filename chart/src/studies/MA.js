export default {

    setup(chart, dataTable, config) {
        var mapping = config.mapping;
        
        if(!mapping)
            mapping = dataTable.mapAs({'value': (config.Value || 'close').toLowerCase() });

        const plot = chart.plot(0);
        plot.legend(false);
        
        var fn;
        
        switch(config.Type) {
            case 'Exponential':
                fn = plot.ema.bind(plot);
                break;
            case 'Running/Modified':
                fn = plot.mma.bind(plot);
                break;
            //case 'Exponential':
            //    fn = plot.mma.bind(plot);
            //    break;
            //case 'Exponential':
            //    fn = plot.mma.bind(plot);
            //    break;
            default:
                fn = plot.sma.bind(plot);
                break;
        }

        var ma = fn(mapping, config['Period'] || 14)
                        .series()
                        .tooltip(false)
                        .stroke(config.Color || '#9b59b6');

        return { series: [ma] };
    },

    title: 'Moving Average (MA)',

    dialogConfig: {
        'Period': 14,
        'Type': [ 'Simple', 'Exponential', 'Running/Modified' ],
        'Value': [ 'Close', 'Open', 'High', 'Low' ],
        '--1': '--DIVIDER--',
        'Color': '#9b59b6'
    }
}