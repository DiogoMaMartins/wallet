export default {
  
  setup(chart, dataTable, config) {
      var mapping = config.mapping;

      if (!mapping) {
        mapping = dataTable.mapAs({
          'high': 'high',
          'low': 'low',
          'open': 'open',
          'close': 'close',
          'volume': 'volume',
          'value': 'close',
        });
      }

      const plot = chart.$createPlot();
      plot.legend(true);
      plot.height(config.height || "120px");
      
      const adl = plot.adl(mapping, 'line').series();
      adl.stroke(config.Color || '#9b59b6');
      adl.tooltip(false);

      return { plots: [plot], series: [adl] };
  },

  title: 'Accumulation Distribution Line (ADL)',

  dialogConfig: {
      '--1': '--DIVIDER--',
      'Color': '#9b59b6'
  }
}