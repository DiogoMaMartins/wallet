export default {
    
  //config = {
  //  type: 'AMA',
  //  color: null,
  //  mapping: null
  //}
  
  setup(chart, dataTable, config) {
      var mapping = config.mapping;
      
      if(!mapping)
          mapping = dataTable.mapAs({
            'high': 'high',
            'low': 'low',
            'open': 'open',
            'close': 'close',
            'volume': 'volume',
            'value': 'close',
          });

      const plot = chart.$createPlot();
      plot.legend(true);
      plot.height(config.height || "120px");

      var CCI = plot.cci(
        mapping,
        config['Period'] || 20,
        'line'
      ).series().stroke(`1.5 ${config.Color || '#9b59b6'}`).tooltip(false);

      return { plots: [plot], series: [CCI] };
  },

  title: 'Commodity Channel Index (CCI)',

  dialogConfig: {
      'Period': 20,
      '--1': '--DIVIDER--',
      'Color': '#9b59b6',
  }
}