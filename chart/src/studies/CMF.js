export default {
  
  setup(chart, dataTable, config) {
      var mapping = config.mapping;

      if (!mapping) {
        mapping = dataTable.mapAs({
          'high': 'high',
          'low': 'low',
          'open': 'open',
          'close': 'close',
          'volume': 'volume',
          'value': 'close',
        });
      }

      const plot = chart.$createPlot();
      plot.legend(true);
      plot.height(config.height || "120px");

      const cmf = plot.cmf(mapping, config.period || 20, 'line').series().tooltip(false).stroke(config.Color || '#9b59b6');
      return { plots: [plot], series: [cmf] };
  },

  title: 'Chaikin Money Flow (CMF)',

  dialogConfig: {
      '--1': '--DIVIDER--',
      'period': 20,
      'Color': '#9b59b6'
  }
}