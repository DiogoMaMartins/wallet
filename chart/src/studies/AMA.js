export default {
    
    //config = {
    //  type: 'AMA',
    //  color: null,
    //  mapping: null
    //}
    
    setup(chart, dataTable, config) {
        var mapping = config.mapping;

        if(!mapping)
            mapping = dataTable.mapAs({'value': (config.Value || 'close').toLowerCase() });

        const plot = chart.plot(0);
        plot.legend(true);
        
        var ama = plot.ama(mapping, config['Fast MA Period'] || 10, config['Slow MA Period'] || 5, config['Signal Period'] || 20).series();
        ama.tooltip(false);
        ama.stroke(config.Color || '#9b59b6');

        return { series: [ama] };
    },

    title: 'Adaptive Moving Average (AMA)',

    dialogConfig: {
        'Fast MA Period': 10,
        'Slow MA Period': 5,
        'Signal Period': 20,
        'Value': [ 'Close', 'Open', 'High', 'Low' ],
        '--1': '--DIVIDER--',
        'Color': '#9b59b6'
    }
}