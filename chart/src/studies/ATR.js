export default {

    setup(chart, dataTable, config) {
        var mapping = config.mapping;
        
        if(!mapping)
          mapping = dataTable.mapAs({
            'high': 'high',
            'low': 'low',
            'open': 'open',
            'close': 'close',
            'volume': 'volume',
            'value': 'close',
          });

        const plot = chart.$createPlot();
        plot.legend(true);
        plot.height(config.height || "120px");
        
        var atr = plot.atr(mapping, 12, 'spline').series();
        atr.tooltip(false);
        atr.stroke(config.Color || '#9b59b6');

        return { plots: [plot], series: [atr] };
    },

    title: 'Average True Range (ATR)',

    dialogConfig: {
        'Period': 14,
        '--1': '--DIVIDER--',
        'Color': '#9b59b6'
    }
}