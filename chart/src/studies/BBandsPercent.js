export default {
  
  setup(chart, dataTable, config) {
      var mapping = config.mapping;

      if (!mapping) {
        mapping = dataTable.mapAs({
          'high': 'high',
          'low': 'low',
          'open': 'open',
          'close': 'close',
          'volume': 'volume',
          'value': 'close',
        });
      }

      const plot = chart.$createPlot();
      plot.legend(true);
      plot.height(config.height || "120px");

      const bbandsPercent = plot.bbandsB(mapping, config.period || 20, config.deviation || 2, 'line').series();
      bbandsPercent.stroke(config.Color || '#9b59b6');
      bbandsPercent.tooltip(false);

      return { plots: [plot], series: [bbandsPercent] };
  },

  title: 'Bollinger Bands %B',

  dialogConfig: {
      '--1': '--DIVIDER--',
      'period': 20,
      'deviation': 2,
      'Color': '#9b59b6'
  }
}