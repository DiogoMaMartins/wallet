export default {
    
    //config = {
    //  type: 'MACD',
    //  height: null
    //}
    
    setup(chart, dataTable, config) {
        var mapping = dataTable.mapAs({'value': (config.Value || 'close').toLowerCase() });

        // MACD is usually displayed on a separate plot
        const plot = chart.$createPlot(); // .plot(config.plotIndex || chart.getPlotsCount());
        plot.legend(true);
        plot.height(config.height || "200px");

        // create MACD indicator with fast period 12, slow period 26 and signal period 9
        var macd = plot.macd(mapping, config['Fast MA Period'] || 12, config['Slow MA Period'] || 26, config['Signal Period'] || 9,
                                'line', 'line', 'column');
        // MACD consists of three series, MACD and signal are lines by default, histogram is a column
        macd.macdSeries().stroke(config['MACD'] || '#2c3e50').tooltip(false);
        macd.signalSeries().stroke(config['Signal'] || '#c0392b').tooltip(false);
        macd.histogramSeries().fill(config['Increasing Bar'] || '#27ae60').tooltip(false);

        //macd.histogramSeries().negativeFill(config['Decreasing Bar'] || '#27ae60');

        return {
          plots: [plot],
          series: [macd],
        };
    },

    title: 'Moving Average Convergence Divergence (MACD)',

    dialogConfig: {
        'Fast MA Period': 12,
        'Slow MA Period': 26,
        'Signal Period': 9,
        'Value': [ 'Close', 'Open', 'High', 'Low' ],
        '--1': '--DIVIDER--',
        'MACD': '#2c3e50',
        'Signal': '#c0392b',
        'Increasing Bar': '#27ae60',
        'Decreasing Bar': '#27ae60'
    }
}