export default {
    
  //config = {
  //  type: 'AMA',
  //  color: null,
  //  mapping: null
  //}
  
  setup(chart, dataTable, config) {
      var mapping = config.mapping;
      
      if(!mapping)
          mapping = dataTable.mapAs({
            'high': 'high',
            'low': 'low',
            'open': 'open',
            'close': 'close',
            'volume': 'volume',
            'value': 'close',
          });

      const plot = chart.$createPlot();
      plot.legend(true);
      plot.height(config.height || "120px");

      var DMI = plot.dmi(
        mapping,
        config['Period'] || 14,
        config['ADX Period'] || 14,
        config['Wilders Smoothing'] === 'YES',
        'line',
        'line',
        'line'
      );

      DMI.adxSeries().stroke(`1.5 ${config['ADX Color'] || '#9b59b6'}`).tooltip(false);
      DMI.ndiSeries().stroke(`1.5 ${config['NDI Color'] || '#9b59b6'}`).tooltip(false);
      DMI.pdiSeries().stroke(`1.5 ${config['PDI Color'] || '#9b59b6'}`).tooltip(false);

      return { plots: [plot], series: [DMI] };
  },

  title: 'Directional Movement Index (DMI)',

  dialogConfig: {
      'Period': 14,
      'ADX Period': 14,
      'Wilders Smoothing': [ 'YES', 'NO' ],
      '--1': '--DIVIDER--',
      'ADX Color': '#9b59b6',
      'NDI Color': '#c0392b',
      'PDI Color': '#27ae60',
  }
}