export default {

    setup(chart, dataTable, config) {
        var mapping = config.mapping;
        
        if(!mapping)
            mapping = dataTable.mapAs({'high': 'high', 'low': 'low'});

        const plot = chart.$createPlot();
        plot.legend(true);
        plot.height(config.height || "120px");
        
        var atr = plot.aroon(mapping, config['Period'] || 14);
        atr.downSeries().stroke(config['Down Color'] || '#9b59b6').tooltip(false);
        atr.upSeries().stroke(config['Up Color'] || '#27ae60').tooltip(false);

        return { plots: [plot], series: [atr] };
    },

    title: 'Aroon',

    dialogConfig: {
        'Period': 14,
        '--1': '--DIVIDER--',
        'Down Color': '#2980b9',
        'Up Color': '#27ae60'
    }
}