export default {
    
  //config = {
  //  type: 'AMA',
  //  color: null,
  //  mapping: null
  //}
  
  setup(chart, dataTable, config) {
      var mapping = config.mapping;
      
      if(!mapping)
          mapping = dataTable.mapAs({'value': (config.Value || 'close').toLowerCase() });

      const plot = chart.$createPlot();
      plot.legend(true);
      
      var roc = plot.roc(mapping, config.Period || 20, 'column').series().tooltip(false).stroke(config.Color || '#9b59b6');
      
      return { plots: [plot], series: [roc] };
  },

  title: 'Rate of Change (ROC)',

  dialogConfig: {
      'Period': 20,
      'Value': [ 'Close', 'Open', 'High', 'Low' ],
      '--1': '--DIVIDER--',
      'Color': '#9b59b6'
  }
}