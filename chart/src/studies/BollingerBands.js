class BBands {

    constructor(plot, mapping) {
        this.range = plot.rangeArea(mapping);

        this.range.tooltip(true);
        this.range.name("Bollinger");
        
        this.middle = plot.line(mapping);
    }
    
    fill(config) {
        this.range.fill(config);
    }
    
    highStroke(color, thickness, dash) {
        this.range.highStroke(color, thickness, dash);
    }
    
    lowStroke(color, thickness, dash) {
        this.range.lowStroke(color, thickness, dash);
    }
    
    middleStroke(color, thickness, dash) {
        this.middle.stroke(color, thickness, dash);
    }

    setMiddleName(name) {
      this.middle.name(name);
    }
    
    series() {
        return [ this.range, this.middle ]
    }
    
    static normalizeToNaturalNumber(value, opt_default, opt_allowZero) {
        if (typeof(value) !== 'number')
            value = parseFloat(value);
        
        value = Math.round(value);
        // value > 0 also checks for NaN, because NaN > 0 == false.
        return (!isNaN(value) && ((value > 0) || (opt_allowZero && !value))) ?
          value :
          (opt_default !== undefined ? opt_default : opt_allowZero ? 0 : 1);
    };

    static calculateMMA(context, value) {
        var queue      = context.queue;
        var period     = context.period;
        var prevResult = context.prevResult;

        var firstValue = queue.enqueue(value);
        //context.dequeuedValue = firstValue;

        /** @type {number} */
        var result;
        if (queue.getLength() < period) {
            result = NaN;
        } else if (isNaN(prevResult)) {
            result = 0;
        for (var i = 0; i < period; i++) {
            result += /** @type {number} */(queue.get(i));
        }
            result /= period;
        } else { // firstValue should not be undefined here
            var lastValue = /** @type {number} */(queue.get(-1));
            result = prevResult + (lastValue - prevResult) / period;
        }

        //context.prevResult = result;
        return result;
    }
    
    static calculate(context, value) {
        //context.dequeuedValue = context.re;
        
        var prevResult = context.prevResult;
        var math       = anychart.math[context.type];
        var sma;
        
        if(context.type === 'mma') {
            sma = BBands.calculateMMA(context, value);
        } else {
            sma = math.calculate(context, value);
        }

        if (isNaN(sma))
            return [NaN, NaN, NaN];

        var i, dist, result;
        var queue = context.queue;
        var period = context.period;

        if (isNaN(prevResult)) {
            // init calculations
            result = 0;
            for (i = 0; i < period; i++) {
                dist = /** @type {number} */ (queue.get(i)) - sma;
                result += dist * dist;
            }
        } else {
            // process calculations
            var distPrev = context.dequeuedValue - prevResult;
            dist = /** @type {number} */ (queue.get(-1)) - sma;
            var diff = prevResult - sma;
            result =
                context.prevDeviation +
                period * diff * diff +
                diff * (sma + prevResult) -
                2 * context.dequeuedValue * diff +
                dist * dist -
                distPrev * distPrev;
        }
        if (result < 0) result = 0;
        context.prevDeviation = result;
        result = Math.sqrt(result / period) * context.deviation;
        return [
            sma, // middle
            sma + result, // upper
            sma - result // lower
        ];
    }
    
    static calculationFunction(row, context) {
        var value = parseFloat(row.get('value'));
        var rv  = BBands.calculate(context, value);
        var middle = rv[0];
        var upper = rv[1];
        var lower = rv[2];
        row.set('middleResult', middle);
        row.set('upperResult', upper);
        row.set('lowerResult', lower);
    }
    
    static startFunction(context) {
        const fn = anychart.math[context.type];
        fn.startFunction(context);
        context.prevDeviation = NaN;
    }
    
    static initContext(opt_period, opt_deviation, type) {
        var period = BBands.normalizeToNaturalNumber(opt_period, 20, false);
        var deviation = BBands.normalizeToNaturalNumber(opt_deviation, 2, false);
        var context = anychart.math[type].initContext(period);
        context.type      = type;
        context.deviation = deviation;
        context.prevDeviation = NaN;
        context.period = opt_period; // === context.Tb
        context.queue = context.$b;
        context.prevResult = NaN//context.re;
        return (context);
    }
    
    static createComputer(table, mapping, opt_period, opt_deviation, type) {
        var result = table.createComputer(mapping);

        result.setContext(BBands.initContext(opt_period, opt_deviation, type));
        result.setStartFunction(BBands.startFunction);
        result.setCalculationFunction(BBands.calculationFunction);

        result.addOutputField('upperResult');
        result.addOutputField('lowerResult');
        result.addOutputField('middleResult');

        return result;
    }

    static createSeries(plot, mapping) {
        return new BBands(plot, mapping);
    }
    
    static transformType(type) {
        switch(type) {
            case 'Exponential':
                return 'ema';
            case 'Modified/Running':
                return 'mma';
            default:
                return 'sma';
        }
    }
}

export default {
    title: 'Bollinger Bands',

    setup(chart, dataTable, config) {
        const plot = chart.plot(0).legend(false);

        var mapping = config.mapping;

        if (!mapping) {
            mapping = dataTable.mapAs({
                high: 'high',
                low: 'low',
                close: 'close',
                open: 'open',
                value: String(config.Field || 'close').toLowerCase(),
            });
        }
        
        //Calculation functions
        const type     = BBands.transformType(config['Moving Average Type']);
        const computer = BBands.createComputer( dataTable, mapping, config.Period || 20, config['Standard Deviations'] || 2, type )

        const mapping2 = dataTable.mapAs({
            high: computer.getFieldIndex('upperResult'),
            value: computer.getFieldIndex('middleResult'),
            low: computer.getFieldIndex('lowerResult')
        });

        //Create range series
        const series = BBands.createSeries(plot, mapping2);

        series.fill({
            color: config['Channel Fill'] ? config['Fill Color'] : 'transparent',
            opacity: config['Channel Fill'] ? 0.2 : 0,
        });

        series.highStroke(config['Bollinger Bands Top'], 1);
        series.lowStroke(config['Bollinger Bands Bottom'], 1);
        series.middleStroke(config['Bollinger Bands Median'], 1);
        series.setMiddleName(type.toUpperCase());

        return {
            series: series.series()
        }
    },

    dialogConfig: {
        'Field': {
            values: ['Open', 'High', 'Low', 'Close'],
            default: 'Close'
        },
        '--1': '--DIVIDER--',
        'Period': 20,
        'Standard Deviations': 2,
        'Moving Average Type': ['Simple', 'Exponential', 'Modified/Running', /*'Adaptive' /*, 'Time Series', 'Triangular', 'Variable', 'VIDYA', 'Weighted', 'Welles Wilder' */ ],
        'Channel Fill': true,
        '--1': '--DIVIDER--',
        'Bollinger Bands Top': '#000',
        'Bollinger Bands Median': '#000',
        'Bollinger Bands Bottom': '#000',
        'Fill Color': '#aaa'
    }
}
