export default {
  //config = {
  //  type: 'AMA',
  //  color: null,
  //  mapping: null
  //}

  setup(chart, dataTable, config) {
      var mapping = config.mapping;

      if(!mapping)
          mapping = dataTable.mapAs({
            'high':   'high',
            'low':    'low',
            'x':      'x',
            'value':  'close',
          });

      const plot = chart.plot(0);
      config.Days = parseInt(config.Days || 120, 10);
      const devided = Math.round(config.Days / 2);
      
      const iterator = mapping.createSelectable().getIterator();

      for (var i = 0; i <= iterator.F.length - config.Days; i++) {
        iterator.advance();
      }

      const top = [{}, {}];
      const bottom = [{}, {}];
      const start = iterator.get('x');
      let end = iterator.get('x')+1;
      for (var i = iterator.getIndex(); i < iterator.F.length; i++) {
        const index = (i !== (iterator.F.length - 1) && Math.abs(iterator.F.length - i) > devided) ? 0 : 1;
        if (!top[index].value || top[index].value < iterator.get('high')) {
          top[index] = {
            x: iterator.get('x'),
            value: iterator.get('high'),
          };
        }
        if (!bottom[index].value || bottom[index].value > iterator.get('low')) {
          bottom[index] = {
            x: iterator.get('x'),
            value: iterator.get('low'),
          };
        }
        if (i === (iterator.F.length - 1)) end = iterator.get('x');
        iterator.advance();
      }

      // const lineTop = plot.line(top, { x: 'x', 'value': 'value' });
      // const lineBottom = plot.line(bottom, { x: 'x', 'value': 'value' });

      var controller = plot.annotations();
      const anno1 = controller.verticalLine({
          xAnchor: start,
          color: config['Color Period Indicators'] || '#9b59b6',
      });
      const anno2 = controller.line({
          xAnchor: top[0].x,
          valueAnchor: top[0].value,
          secondXAnchor: top[1].x,
          secondValueAnchor: top[1].value,
          color: config['Color Resistance'] || '#c0392b',
      });
      const anno3 = controller.line({
          xAnchor: bottom[0].x,
          valueAnchor: bottom[0].value,
          secondXAnchor: bottom[1].x,
          secondValueAnchor: bottom[1].value,
          color: config['Color Support'] || '#27ae60',
      });
      const anno4 = controller.verticalLine({
          xAnchor: end,
          color: config['Color Period Indicators'] || '#9b59b6',
      });
      
      return { annotations: [anno1, anno2, anno3, anno4] };
  },

  title: 'Trend lines',

  dialogConfig: {
      'Days': 120, // 3 Months ?
      '--1': '--DIVIDER--',
      'Color Resistance': '#c0392b',
      'Color Support': '#27ae60',
      'Color Period Indicators': '#9b59b6',
  }
}