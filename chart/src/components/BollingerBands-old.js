export default {
    title: 'Bollinger Bands',

    setupMA(chart, mapping, config) {
      const plot = chart.plot(0);
      var ma;
    
      switch(String(config['Moving Average Type'] || 'Simple')) {
        case 'Exponential':
          ma = plot.ema(mapping, config.Period || 20).series();
          break;
        case 'Modified Moving':
          ma = plot.mma(mapping, config.Period || 20).series();
          break;
        case 'Adaptive':
          ma = plot.ama(mapping, config.Period || 20, config['Fast Period'] || 20, config['Slow Period'] || 20).series();
          break;
        case 'Time Series':
        case 'Triangular':
        case 'Variable':
        case 'VIDYA':
        case 'Weighted':
        case 'Welles Wilder':
          break;
        case 'Simple':
        default:
          ma = plot.sma(mapping, config.Period || 20).series();
          break;
        //...
      }
    
      ma.stroke(config['Bollinger Bands Median'] || '#9b59b6');
      ma.tooltip(false);
    
      if (!ma.data) {
        console.error('Can not extract data from', config['Moving Average Type'], 'MA type.');
        const maSimple = plot.sma(mapping, config.Period || 20).series();
        maSimple.tooltip(false);
        return maSimple.data ? { series: maSimple, data: maSimple.data() } : { series: {}, data: {} };
      }
      return { series: ma, data: ma.data() };
    },

    setup(chart, dataTable, config) {
        const plot = chart.plot(0);
        plot.legend(true);
        var mapping = config.mapping;

        if (!mapping) {
          mapping = dataTable.mapAs({
            high:   'high',
            low:    'low',
            close:  'close',
            open:   'open',
            value:  String(config.Field || 'close').toLowerCase(),
          });
        }

        // Selected MA Line
        const MA = this.setupMA(chart, mapping, config);

        /*
        if (String(config['Moving Average Type'] || 'Simple') === 'simple') {
          // AnyChart integrated BBANDS with SMA:
          const BBands = plot.bbands(mapping);
          const dataBBands = BBands.upperSeries().data();
          BBands.upperSeries().stroke(config['Bollinger Bands Top'], 2, "2 2").tooltip(false);
          BBands.middleSeries().stroke(config['Bollinger Bands Median'] || '#9b59b6').tooltip(false);
          BBands.lowerSeries().stroke(config['Bollinger Bands Bottom'], 2, "2 2").tooltip(false);
          return BBands.middleSeries();
        }
        */
        
        // BBands temporary for reverse calculation STDEV
        const BBands = plot.bbands(mapping);
        BBands.upperSeries().stroke('transparent').tooltip(false).legendItem(false);
        BBands.middleSeries().stroke('transparent').tooltip(false).legendItem(false);
        BBands.lowerSeries().stroke('transparent').tooltip(false).legendItem(false);

        // SMA temporary for reverse calculation STDEV
        const SMA = plot.sma(mapping, config.Period || 20).series();
        SMA.stroke('transparent');
        SMA.legendItem(false);
        SMA.tooltip(false);
        const dataSMA = SMA.data(); 

        // Custom BBands computer (with select MA)
        const computer = dataTable.createComputer(mapping);
        const uuid = Math.floor(new Date().getTime() + (Math.random() * 100000));
        computer.setContext({
          BBandsIterator: BBands.upperSeries().data().createSelectable().getIterator(),
          SMAIterator: dataSMA.createSelectable().getIterator(),
          MAIterator: MA.data.createSelectable().getIterator(),
          uuid,
        });
        computer.setCalculationFunction(function (row, context) {
          // Advance iterators
          if (row.getIndex() - 1 !== context.BBandsIterator.getIndex()) {
            context.BBandsIterator.reset();
            while (row.getIndex() - 1 !== context.BBandsIterator.getIndex()) {
              context.BBandsIterator.advance();
            }
          }
          if (row.getIndex() - 1 !== context.SMAIterator.getIndex()) {
            context.SMAIterator.reset();
            while (row.getIndex() - 1 !== context.SMAIterator.getIndex()) {
              context.SMAIterator.advance();
            }
          }
          if (row.getIndex() - 1 !== context.MAIterator.getIndex()) {
            context.MAIterator.reset();
            while (row.getIndex() - 1 !== context.MAIterator.getIndex()) {
              context.MAIterator.advance();
            }
          }
          context.BBandsIterator.advance();
          context.SMAIterator.advance();
          context.MAIterator.advance();
          let valueBband = context.BBandsIterator.get('value');
          let valueSMA = context.SMAIterator.get('value');
          let valueMA = context.MAIterator.get('value');

          // STDEV found by reversing the BBANDS function
          const stdev = (valueBband - valueSMA) / (config['Standard Deviations'] || 2);
          row.set('upperBand'+context.uuid, parseFloat(valueMA + (stdev * 2)));
          row.set('lowerBand'+context.uuid, parseFloat(valueMA - (stdev * 2)));
        });
        computer.addOutputField('upperBand'+uuid, 'upperBand'+uuid);
        computer.addOutputField('lowerBand'+uuid, 'lowerBand'+uuid);
        
        const mapping2 = dataTable.mapAs({
          high: computer.getFieldIndex('upperBand'+uuid),
          low: computer.getFieldIndex('lowerBand'+uuid),
        });

        // Add a range spline series using the data from the computer
        const bbandsSeries = plot.rangeArea(mapping2, {
          high: 'high',
          low: 'low',
        });
        bbandsSeries.tooltip(true);
        bbandsSeries.name("Bollinger");
        bbandsSeries.fill({
          color: config['Channel Fill'] ? '#ccc' : 'transparent',
          opacity: config['Channel Fill'] ? 0.1 : 0,
        });
        bbandsSeries.highStroke(config['Bollinger Bands Top'], 2, "2 2");
        bbandsSeries.lowStroke(config['Bollinger Bands Bottom'], 2, "2 2");

        return { series: [MA.series, BBands.upperSeries(), BBands.middleSeries(), BBands.lowerSeries(), SMA, bbandsSeries] };
    },
    
    dialogConfig: {
        'Field': {
            values: [ 'Open', 'High', 'Low', 'Close' ],
            default: 'Close'
        },
        '--1': '--DIVIDER--',
        'Period': 20,
        'Fast Period (AMA)': 2,
        'Slow Period (AMA)': 30,
        '--1': '--DIVIDER--',
        'Standard Deviations': 2,
        'Moving Average Type': [ 'Simple', 'Exponential', 'Modified Moving', 'Adaptive' /*, 'Time Series', 'Triangular', 'Variable', 'VIDYA', 'Weighted', 'Welles Wilder' */ ],
        'Channel Fill': true,
        '--1': '--DIVIDER--',
        'Bollinger Bands Top': '#000',
        'Bollinger Bands Median': '#000',
        'Bollinger Bands Bottom': '#000'
    }
}