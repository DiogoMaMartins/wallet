import Studies from '../studies/index.vue';

export default {
    data() {
        return {
            indicatorConfigs: [],
            plotsCount: 1,
            showIndicatorPlots: true
        };
    },

    computed:  {
        indicators() {
            return Studies.List();
        },
    },
    
    watch: {
        showIndicatorPlots(val, oldval) {
            if (!this.$chart) 
                return;
            
            if (this.disabledPlots <= 0 && this.disabledSeries <= 0) 
                val = false;

            for (var i = 0; i < this.$chart.getPlotsCount(); i += 1) {
                const plot = this.$chart.plot(i);
                if (i > 0) {
                    if (plot.enabled() !== val) {
                        plot.enabled(val);
                        this.disabledPlots += (val ? -1 : 1);
                    }
                } else {
                    for (var x = 1; x < plot.getSeriesCount(); x += 1) {
                        if (plot.getSeriesAt(x) && plot.getSeriesAt(x).enabled() !== val) {
                            plot.getSeriesAt(x).enabled(val);
                            this.disabledSeries += (val ? -1 : 1);
                        }
                    }
                }
            }
        }
    },

    methods: {
        $createPlot() {
            const plot = this.$chart.plot(this.plotsCount);
            this.plotsCount++;
            
            plot.enabled(true);
            plot.removed = false;
            return plot;
        },
        
        $plot(index) {
            if(!this.$chart)
                return null;

            const count = this.$chart.getPlotsCount();
            var current = 0;
            
            for(var i=0; i<count;i++) {
                const plot = this.$chart.plot(i);
                
                if(plot.removed)
                    continue;

                if(current === index)
                    return plot;
                
                current++;
            }

            return null;
        },
        
        share() {
            this.$refs.shareDialog.show();
        },

        showIndicatorDialog(type) {
            this.$refs.studiesDialog.showDialog(type);
        },

        addIndicator(config, replaceRoute = true, isRestoring = false) {
            const setup = Studies.TechnicalSetup(this.$chart, this.$dataTable, config);
            setup.enabled = true;
            if (this.indicatorOrder.length > 0) {
              this.$set(this, 'indicatorOrder', this.indicatorOrder.filter(el => el.enabled));
            }
            this.indicatorOrder.push(setup);
            this.indicatorConfigs.push(config);
            this.showSettings = false;

            /*if (replaceRoute && this.$router) {
                this.$router.replace(this.$route.path, {
                    market: this.marketIso,
                    name: this.marketName
                });
            }*/

            this.$nextTick(() => {
              if(!isRestoring)
                this.$refs.shareDialog.onModified();
            });
        },
        
        removeAllIndicators(deleteConfigs = true) {
            if(!this.$chart)
                return;

            if (deleteConfigs) {
              this.indicatorConfigs = [];
            }
            //this.plotsCount = 1;

            const count = this.$chart.getPlotsCount();

            for(var i=1; i<count; i += 1) {
                const plot = this.$chart.plot(i);
                this.$set(this, 'indicatorOrder', this.indicatorOrder.filter(el => {
                  return !el.plots || el.plots.length === el.plots.filter(el => el !== plot).length;
                }));
                plot.enabled(false).remove();
                plot.removed = true;
            }
            
            const plot1 =  this.$chart.plot(0);
            
            if(!plot1)
                return;

            for (var x = plot1.getSeriesCount(); x >= 1; x -= 1) {
                const serie = plot1.getSeriesAt(x);
                if (serie) {
                  this.$set(this, 'indicatorOrder', this.indicatorOrder.filter(el => {
                    return !el.series || el.series.length === el.series.filter(el => el !== serie).length;
                  }));
                  serie.enabled(false).remove();
                  serie.removed = true;
                  plot1.removeSeriesAt(x);
                }
            }
        },

        restoreIndicators(config) {
            if (this.restoreIndicatorsTimeout) {
                clearTimeout(this.restoreIndicatorsTimeout);
                this.restoreIndicatorsTimeout = null;
            }
            
            if(!config)
                config = this.indicatorConfigs;

            if (!this.$chart || !Studies || !this.$dataTable) {
                this.restoreIndicatorsTimeout = setTimeout(() => {
                    this.restoreIndicatorsTimeout = null;
                    this.restoreIndicators(config)
                }, 500);

                return;
            }

            this.removeAllIndicators();
            config.forEach(el => this.addIndicator(el, false, true));
        }
    }
}
