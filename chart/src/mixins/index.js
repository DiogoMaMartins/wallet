import Data from './data.js';
import Annotations from './annotations.js';
import Indicators from './indicators.js';
import FullScreen  from './fullscreen.js';
import UndoRedo from './undo-redo.js';
import Components from '../components';
import Zoom from './zoom.js';

export default [
    Data,
    Annotations,
    Indicators,
    FullScreen,
    UndoRedo,
    Components,
    Zoom
]