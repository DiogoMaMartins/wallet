export default {
    methods: {
        isFullScreen() {
            if (this.$isServer) return false;
            const $el = this.$el;
            return (document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement) === $el;
        },
        
        exitFullScreen() {
            if (this.$isServer) return false;
            const fn = document.exitFullscreen || document.webkitExitFullscreen || document.mozCancelFullScreen || document.msExitFullscreen;

            if(fn)
                fn.call(document);
        },
        
        fullscreen() {
            if (this.$isServer) return false;
            if(this.isFullScreen())
                return this.exitFullScreen();

            var docElm = this.$el;
            if (docElm.requestFullscreen) {
                docElm.requestFullscreen();
            }
            else if (docElm.mozRequestFullScreen) {
                docElm.mozRequestFullScreen();
            }
            else if (docElm.webkitRequestFullScreen) {
                docElm.webkitRequestFullScreen();
            }
        }
    }
}