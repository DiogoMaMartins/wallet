export default {

    mounted() {
        this.$nextTick(() => {
          const $el = this.$el.querySelector('.chartContainer');
          if ($el.addEventListener) {
              $el.addEventListener("mousewheel", this.handleScroll.bind(this), false);
              $el.addEventListener("DOMMouseScroll", this.handleScroll.bind(this), false);
          } else {
              $el.attachEvent("onmousewheel", this.handleScroll.bind(this));
          }
        });
    },

    methods: {
        handleScroll($e) {
            if (!this.$chart)
                return;

            if ($e && $e.preventDefault && (!$e.isDefaultPrevented || !$e.isDefaultPrevented())) {
              $e.preventDefault();
            }
            const selectedRange = this.$chart.getSelectedRange();

            //zoom
            const zoomIntensity = 0.2;
            const wheel = $e.wheelDelta / 120;
            const zoom = Math.exp(wheel * zoomIntensity);

            const minRange = 1000 * 60 * 5 * this.intervalMinutes();
            const oldRange = Math.min(Math.max(selectedRange.lastSelected - selectedRange.firstSelected, minRange), this.getMaxRange());
            const visibleRange = Math.min(Math.max(oldRange / zoom, minRange), this.getMaxRange());

            //scale
            //const bounds        = { left: 0, width: this.$chart.container().getBounds().width - 50 } //ToDo calculate plot left
            //const mousex        = $e.clientX - bounds.left;
            //console.log(mousex)
            //var mousex = $e.clientX - bounds.left;
            //var mousey = $e.clientY - canvas.offsetTop;
            // Normalize wheel to +1 or -1.
            const x1 = selectedRange.firstSelected + (oldRange - visibleRange);

            this.$chart.selectRange(x1, x1 + visibleRange);
        },

        getMaxRange() {
            if(this.items.length < 2)
                return 500;
            
            const padding = 5 *  this.intervalMinutes() * 60 * 1000;
            const range =  ((this.items[this.items.length-1].time - this.items[0].time) * 1000) + padding;

            return range;
        },
    }
}
