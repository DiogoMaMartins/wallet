export default {

    props: {
        marketName: {
            type: String,
            default: ''
        },
        marketIso: {
            type: String,
            default: ''
        },
        
        marketPrecision: {
            type: Number,
            default: 4
        }
    },

    data() {
        return {
            items: [], //Graph Data
        }
    },

    created()  {
        this.$dataTable = null;
    },

    mounted() {
        if (window.market) {
            this.market.name = window.market.name;
            this.market.iso = window.market.iso;
        }

        if (this.market.iso)
            this.updateApi();

        this.$setInterval(() => { //ToDo get from live stream
            this.$refreshApi('market/ticks', {
                interval: this.intervalMinutes(),
                market: this.market.iso
            });
        }, 1000);
    },

    watch: {
        items() {
            if (this.$_interval)
                clearInterval(this.$_interval);

            if (this.items.length === 0)
                return;

            if (!window.anychart) {
                this.$_interval = this.$setInterval(() => {
                    if (!window.anychart || !window.anychart.data.table)
                        return;

                    clearInterval(this.$_interval);
                    this.setupData();
                }, 100);

                return;
            }

            this.setupData();
        },

        marketName() {
            this.market.name = this.marketName;
        },

        marketIso() {
            this.market.iso = this.marketIso;

            if (this.$dataTable) {
                this.$dataTable.remove();
                this.$dataTable.items = [];
            }

            this.updateApi();
        },

        interval() {
            this.$dataTable.remove();
            this.$dataTable.items = [];
            this.updateApi();
        }
    },

    computed: {
        currencyRate() {
            var currency;
            if (this.shareCurrency) {
              const currencies = this.$getCurrencies();
              if (currencies[this.shareCurrency]) {
                currency = {
                  name: String(this.shareCurrency),
                  symbol: (currencies[this.shareCurrency] || {}).symbol,
                  info: currencies[this.shareCurrency] || {}
                };
              }
            } else if (this.$getCurrency) {
              currency = this.$getCurrency();
            }
            if (currency && currency.info && currency.info.rate) return parseFloat(currency.info.rate);
            return 1;
        }
    },

    methods: {
        setupChart() {
            if (this.$chart)
                return this.$chart;

            const chart = this.$chart = anychart.stock() || null;

            if (chart === null)
                return null;

            chart.$createPlot = this.$createPlot.bind(this);

            chart.padding(0, 60, 0, 0);
            chart.preserveSelectedRangeOnDataUpdate(true);
            chart.scroller().enabled(false);

            const $el = this.$el.querySelector('.chartContainer');
            chart.container($el);

            chart.listen('annotationDrawingFinish', this.onAnnotationDrawingFinish.bind(this));
            chart.listen('annotationSelect', this.onAnnotationSelect.bind(this));
            chart.listen('annotationUnSelect', this.onAnnotationUnSelect.bind(this));
            chart.listen('chartDraw', this.onChartDraw.bind(this));

            //--------------------------

            var plot = chart.plot(0);
            plot.yAxis(0).orientation('right');

            const gridStroke = {
                // set stroke color
                color: "#bdc3c7",
                // set dashes and gaps length
                dash: "3 5"
            };

            plot.xGrid().enabled(true).stroke(gridStroke);
            plot.yGrid().enabled(true).stroke(gridStroke);
            plot.xMinorGrid().enabled(true).stroke(gridStroke);
            plot.legend(false);

            //--------------------------

            const self = this;
            chart.crosshair().yLabel().format(function () {
                return this.rawValue.toFixed(self.marketPrecision);
            });

            chart.tooltip().format(function (){
                const rows = [];
                
                function r(name, value) {
                    return `${name}: ${value.toFixed(self.marketPrecision)}`;
                }

                if(!this.open || !this.close || !this.low || !this.high)
                    return null;
                
                rows.push( r('open', this.open) );
                rows.push( r('close', this.close) );
                rows.push( r('low', this.low) );
                rows.push( r('high', this.high) );
                
                return rows.join("\n");
            });

            return this.$chart;
        },
        
        getDefaultVisibleInterval() {
            const showedTicks = Math.max(Math.round((this.$el.offsetWidth || 0) / 25), 20);
            return 1000 * 60 * showedTicks * this.intervalMinutes();
        },

        setupData() {
            var timeOffset = (new Date()).getTimezoneOffset() * 60;

            const items = this.items.map((obj) => {
                return {
                    x: (obj.time - timeOffset) * 1000,
                    open: obj.open * this.currencyRate,
                    high: obj.high * this.currencyRate,
                    low: obj.low * this.currencyRate,
                    close: obj.close * this.currencyRate,
                    volume: obj.volume
                }
            });

            if (this.$dataTable) {
                var from = this.$dataTable.items.find((item) => {
                    const nItem = items.find((obj) => obj.x === item.x);
                    if (!nItem)
                        return false;

                    if (nItem.open !== item.open || nItem.high !== item.high || nItem.low !== item.low || nItem.close !== item.close || nItem.volume !== item.volume)
                        return true;

                    return false;
                });
                
                if(!from) {
                    if(!this.$dataTable.items || this.$dataTable.items.length === 0)
                        from = { x: 0 };
                    else
                        from = this.$dataTable.items[ this.$dataTable.items.length - 1 ];
                }

                this.$dataTable.remove(from.x, Date.now());
                this.$dataTable.items = this.$dataTable.items.filter((obj) => obj.x < from.x);

                from = from.x;

                this.$dataTable.addData(items.filter((obj) => obj.x >= from));

                if (!this.$dataTable.items || this.$dataTable.items.length === 0) {
                    var dx = items[items.length - 1].x;
                    this.$chart.selectRange(dx - this.getDefaultVisibleInterval(), dx + (1000 * 60 * this.intervalMinutes()));
                }

                this.$dataTable.items = items;
                
                //Overloading calculations:
                //this.removeAllIndicators(false);
                //this.restoreIndicators();

                //ToDo move selected range to new viewport
                return;
            }

            //-------------------------------------------------

            if (!anychart || !anychart.data || !anychart.data.table) return;
            const dataTable = anychart.data.table('x');
            const chart = this.setupChart();

            dataTable.remove();
            dataTable.addData(items);
            dataTable.items = items;

            // map loaded data
            this.$dataTable = dataTable;

            // create plot on the chart
            var plot = chart.plot(0);
            let series;
            var mapping = dataTable.mapAs({
                'open': 'open',
                'high': 'high',
                'low': 'low',
                'close': 'close',
                volume: 'volume',
                value: 'close'
            });
            if (this.graphType === 'line') {
                if (this.$series) this.$series.seriesType('line');
                const line = plot.line(mapping);
                line.name(this.market.name);
                line.stroke('2px #64b5f6');
                line.legendItem(false);

                series = line;
            } else {
                if (this.$series) this.$series.seriesType('candlestick');

                // create candle series
                const candle = plot.candlestick(mapping);
                candle.name(this.market.name);
                candle.stroke('2px #64b5f6');
                candle.legendItem(false);

                candle.risingFill("#16a085");
                candle.fallingFill("#c0392b");
                candle.risingStroke("#000");
                candle.fallingStroke("#000");

                series = candle;
            }

            // initial scale
            var dx = items[items.length - 1].x;
            chart.selectRange(dx - this.getDefaultVisibleInterval(), dx);

            this.$series = series;

            chart.draw(true);
            const credentials = this.$el.querySelector('.anychart-credits');
    
            this.removeAllIndicators(false);
            this.restoreIndicators();

            if (credentials)
                credentials.remove();
        },

        updateApi() {
            this.$replaceApi('market/ticks', {
                interval: this.intervalMinutes(),
                market: this.market.iso
            });
        },
    },
}
