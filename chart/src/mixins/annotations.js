function AnnotationType(icon) {
    return {
        icon,
        active: false
    }
}

function MarkerType(icon, marker) {
    return {
        icon,
        marker: marker || icon
    }
}

export default {

    props: {
        initialDrawer: {
            type: Boolean,
            default: true
        }
    },
    
    data() {
        return {
            orderAnnotations: {},

            drawers: {
                //'Edit Drawing Style': 'format_paint', //stroke color, fill color, size (px), type solid/dash/..
                //'Text Label': MarkerType('label', 'label'),
                'Diagonal Line': AnnotationType('infinite-line'),
                'Ray Line': AnnotationType('ray'),
                'Horizontal Line': AnnotationType('horizontal-line'),
                'Vertical Line': AnnotationType('vertical-line'),
                'Limited Line': AnnotationType('line'),
                'Rectangle': AnnotationType('rectangle'),
                'Triangle': AnnotationType('triangle'),
                'Circle': AnnotationType('ellipse'),
                'Channel': AnnotationType('trend-channel'),
                'Pitchfork': AnnotationType('andrews-pitchfork'),

                'Fibonacci': {
                    icon: 'fibonacci-retracement',
                    active: false,
                    subItems: {
                        'Fibonacci Fan': 'fibonacci-fan',
                        'Fibonacci Arc': 'fibonacci-arc',
                        'Fibonacci Retracement': 'fibonacci-retracement',
                        'Fibonacci Time Zones': 'fibonacci-timezones',
                    }
                }
            },
            drawFigures: {
                'Up Arrow': MarkerType('arrow-up-square', 'arrowUp'),
                'Down Arrow': MarkerType('arrow-down-square', 'arrow-down'),
                'Left Arrow': MarkerType('arrow-left-square', 'arrow-left'),
                'Right Arrow': MarkerType('arrow-right-square', 'arrow-right'),
                'Head Arrow': MarkerType('head-arrow', 'arrowHead'),
                'Cross': MarkerType('cross', 'cross'),
                'Diagonal Cross': MarkerType('diagonal-cros', 'diagonal-cross'),
                'Diamond': MarkerType('diamond'),
                'Pentagon': MarkerType('pentagon'),
                //'Square': MarkerType('square'),
                'Star 1': MarkerType('star-1', 'star10'),
                'Star 2': MarkerType('star-2', 'star4'),
                'Star 3': MarkerType('star-3', 'star5'),
                'Star 4': MarkerType('star-4', 'star6'),
                'Star 5': MarkerType('star-5', 'star7'),
                'Trapezium': MarkerType('trapezium'),
                //'Triangle Up': MarkerType('triangle-up'),
                //'Triangle Down': MarkerType('triangle-down'),
                //'Triangle Left': MarkerType('triangle-left'),
                //'Triangle Right': MarkerType('triangle-right'),
            },
            drawTool: {
                fillColor: {
                    hex: '#3498db55'
                },
                color: {
                    hex: '#000'
                },
                size: 1,
                markerSize: 40,
                showDialog: false
            }
        };
    },
    
    mounted() {
        this.onKeyup = this.onKeyup.bind(this);
        document.addEventListener("keyup", this.onKeyup);
    },
    
    destroyed() {
        document.removeEventListener("keyup", this.onKeyup);
    },

    methods: {
        
        serializeAnnotations() {
            var result = [];
            
            if(!this.$chart)
                return result;
            
            var length = this.$chart.getPlotsCount();

            for(var i=0; i<length; i++) {
                const plot = this.$chart.plot(i);
                if(plot.removed)
                    continue;
                
                let json = plot.annotations().toJson();
                if (this.orderAnnotations.enabled) {
                  var currencyRate = (this.$getCurrency() || { info: {} }).info.rate || 1;
                  var floorTP = Math.floor(this.orderAnnotations.order.tp * currencyRate);
                  var floorRate = Math.floor(this.orderAnnotations.order.rate * currencyRate);
                  var floorSL = Math.floor(this.orderAnnotations.order.sl * currencyRate);
                  json.annotationsList = json.annotationsList.filter((el) => {
                    // Filter order annotations
                    if (el.type === 'horizontal-line') {
                      const value = Math.floor(el.valueAnchor);
                      return !(
                        (el.color === '#27ae60' && floorTP === value)
                        || (el.color === '#006aff' && floorRate === value)
                        || (el.color === '#c0392b' && floorSL === value)
                      );
                    }
                    return true;
                  });
                }

                json.annotationsList = json.annotationsList.filter((obj) => {
                    return obj.enabled;
                });

                result.push( json );
            }

            return result;
        },
        
        restoreAnnotations(annotations) {
            if(!Array.isArray(annotations))
                return;

            if (!this.$chart) {
              return this.$setTimeout(() => {
                this.restoreAnnotations(annotations);
              }, 300);
            }

            this.$chart.annotations().removeAllAnnotations();
            var length = this.$chart.getPlotsCount();

            for(var i=0; i<annotations.length && i<length; i++) {
                if(annotations[i].annotationsList.length > 0) {
                  this.$chart.plot(i).annotations().fromJson(annotations[i]);
                  this.indicatorOrder.push({
                    annotations: [annotations[i]],
                    enabled: true,
                  });
                }
            }
        },
        
        onKeyup(e) {
            if(e.keyCode !== 8 || !this.$chart)
                return;

            const selected = this.$chart.annotations().getSelectedAnnotation();
            
            if(selected) {
                this.$set(this, 'indicatorOrder', this.indicatorOrder.filter(el => {
                  return !el.annotations || el.annotations.length === el.annotations.filter(el => el !== selected).length;
                }));
                selected.enabled(false);
                selected.remove();
            }

            this.$refs.shareDialog.onModified();
        },
        
        showFormatDialog() {
            this.drawTool.showDialog = true;
            this.$chart.annotations().cancelDrawing();
            this.onAnnotationDrawingFinish();
        },

        onAnnotationSelect($e) {
            //const annotation = $e.annotation;
        },

        onAnnotationUnSelect() {
            this.$refs.shareDialog.onModified();
            for(var key in this.drawers) {
                if(this.drawers[key].active)
                    this.drawers[key].active = false;
            }

            if(this.fibonacciActive)
                this.fibonacciActive = false;
        },

        onAnnotationDrawingFinish() {
            this.onAnnotationUnSelect();
            this.$refs.shareDialog.onModified();
        },
        
        clearAnnotations() {
            if(this.$chart) {
                var plot = this.$chart.plot(0);
                if (!plot) return;

                /*
                // Remove annotations from indicatorOrder
                const annotations = plot.annotations().toJson();
                if (annotations.annotationsList) {
                  annotations.annotationsList.forEach((annotation) => {
                    this.$set(this, 'indicatorOrder', this.indicatorOrder.filter(el => {
                      return !el.annotations || el.annotations.length === 0;
                    }));
                  });
                }
                */

                this.$chart.annotations().removeAllAnnotations();
                this.$refs.shareDialog.onModified();
            }
        },

        drawOrderAnnotations(order) {
            if(!this.$chart)
                return;

            var plot = this.$chart.plot(0);
            var currencyRate = (this.$getCurrency() || { info: {} }).info.rate || 1;
            var controller = plot.annotations();

            if (this.orderAnnotations && this.orderAnnotations.enabled) {
              this.orderAnnotations.tp.valueAnchor(order.tp * currencyRate);
              this.orderAnnotations.rate.valueAnchor(order.rate * currencyRate);
              this.orderAnnotations.sl.valueAnchor(order.sl * currencyRate);
              this.orderAnnotations.order = order;
              return true;
            }

            const tp = controller.horizontalLine({
                valueAnchor: order.tp * currencyRate,
                color: '#27ae60',
                allowEdit: false,
            });
            const rate = controller.horizontalLine({
                valueAnchor: order.rate * currencyRate,
                color: '#006aff',
                allowEdit: false,
            });
            const sl = controller.horizontalLine({
                valueAnchor: order.sl * currencyRate,
                color: '#c0392b',
                allowEdit: false,
            });

            var legend = plot.legend();
            legend.enabled(true);

            // Set items settings.
            /*
            // @todo: This would replace entire legend, adding would be better.
            legend.items([
                {index: 0, text: "Take Profit", iconType: "circle", iconFill: "#27ae60"},
                {index: 1, text: "Order Rate", iconType: "circle", iconFill: "#006aff" },
                {index: 2, text: "Stop Loss", iconType: "circle", iconFill: "#c0392b"},
            ]);
            */

            this.orderAnnotations = {
              enabled: true,
              tp,
              rate,
              sl,
              order,
            };

            this.indicatorOrder.push({
              annotations: [tp, rate, sl],
              enabled: true,
            });
        },

        useAnnotation(item, type) {
            //https://github.com/anychart-solutions/anystock-drawing-tools-and-annotations-demo/blob/master/js/common.js
            process.nextTick(() => {
                if(!this.$chart)
                    return;

                const chart    = this.$chart;
                const isActive = item.active;

                for(var key in this.drawers) {
                    if(this.drawers[key].active)
                        this.drawers[key].active = false;
                }

                if(isActive) {
                    chart.annotations().cancelDrawing();
                }
                else {
                    item.active = true;

                    if(type && type.indexOf('fibonacci-') !== -1) {
                        this.drawers.Fibonacci.active = true;
                    }

                    chart.annotations().startDrawing({
                        type: item.marker ? 'marker' : (type || item.icon),
                        size: parseFloat(this.drawTool.markerSize),
                        color: this.drawTool.color.hex,
                        fill: this.drawTool.fillColor.hex,
                        markerType: item.marker,
                        //anchor: markerAnchor
                    });

                    const annotation = chart.annotations().getSelectedAnnotation();
                    const stroke = {
                        thickness: parseFloat(this.drawTool.size)
                    };

                    annotation.stroke(stroke);
                    annotation.hovered().stroke(stroke);
                    annotation.selected().stroke(stroke);

                    this.indicatorOrder.push({
                      annotations: [annotation],
                      enabled: true,
                    });
                }
            });
        }
    }
}