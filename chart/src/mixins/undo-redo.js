export default {

    data() {
        return {
            indicatorOrder: [],
            undoEnabled: false,
            redoEnabled: false
        }
    },

    watch: {
      indicatorOrder() {
        this.updateUndoRedoEnabled();
      },
    },

    methods: {
        setterOrder(last) {
          const index = parseInt(last.index);
          delete last.index;
          const indicators = this.indicatorOrder;
          indicators[index] = last;
          this.indicatorOrder = indicators;
          this.updateUndoRedoEnabled();
          return true;
        },

        undo() {
            if (!this.$chart)
                return false;

            const indicators = this.indicatorOrder.map((el, index) => { el.index = index; return el; }).filter(el => el.enabled);
            if (indicators.length === 0) return false;
            const lastIndicator = indicators.slice(-1)[0];

            if (lastIndicator.plots) {
              lastIndicator.plots.forEach((plot, index) => {
                if (plot && plot.enabled) plot.enabled(false);
              });
            }

            if (lastIndicator.annotations) {
              lastIndicator.annotations.forEach((annotation, index) => {
                if (annotation && annotation.enabled) annotation.enabled(false);
              });
            }

            if (lastIndicator.series) {
              lastIndicator.series.forEach((serie, index) => {
                if (serie && serie.enabled) serie.enabled(false);
              });
            }

            lastIndicator.enabled = false;
            return this.setterOrder(lastIndicator);
        },

        redo() {
            if (!this.$chart)
                return false;

            const indicators = this.indicatorOrder.map((el, index) => { el.index = index; return el; }).filter(el => !el.enabled);
            if (indicators.length === 0) return false;
            const lastIndicator = indicators.slice(0, 1)[0];

            if (lastIndicator.plots) {
              lastIndicator.plots.forEach((plot, index) => {
                if (plot && plot.enabled) plot.enabled(true);
              });
            }

            if (lastIndicator.annotations) {
              lastIndicator.annotations.forEach((annotation, index) => {
                if (annotation && annotation.enabled) annotation.enabled(true);
              });
            }

            if (lastIndicator.series) {
              lastIndicator.series.forEach((serie, index) => {
                if (serie && serie.enabled) serie.enabled(true);
              });
            }

            lastIndicator.enabled = true;
            return this.setterOrder(lastIndicator);
        },
        
        updateUndoRedoEnabled() {
          this.$nextTick(() => {
            if (this.indicatorOrder.length === 0) {
              this.undoEnabled = false;
              this.redoEnabled = false;
            } else {
              this.$set(this, 'undoEnabled', this.indicatorOrder.filter(el => !!el.enabled).length > 0);
              this.$set(this, 'redoEnabled', this.indicatorOrder.filter(el => !el.enabled).length > 0);
            }
          });
        }
    }
}
