/*!
 * Random number generator with ArcFour PRNG
 * 
 * NOTE: For best results, put code like
 * <body onclick='SecureRandom.seedTime();' onkeypress='SecureRandom.seedTime();'>
 * in your main HTML document.
 * 
 * Copyright Tom Wu, bitaddress.org  BSD License.
 * http://www-cs-students.stanford.edu/~tjw/jsbn/LICENSE
 */
//var shajs = require('sha.js')

class SecureRandom  {

    constructor(uuid) {
        // Properties
        this.state = null;
        this.pool = new Array();
        this.pptr = 0;
        //this.poolCopyOnInit;

        // Pool size must be a multiple of 4 and greater than 32.
        // An array of bytes the size of the pool will be passed to init()
        this.poolSize = 256;
 
        if(process.env.VUE_ENV === 'client')
            this.initClient();
        else
            this.initServer(uuid);
    }

    initClient() {
        this.init(window, navigator);
    }
    
    initServer(uuid) {
        const window = {
            screen: {
                height: 600,
                width: 200,
                colorDepth: -5,
                availHeight: 500,
                availWidth: 500,
                pixelDepth: 4
            },
            history: [],
            location: '/' + uuid
        }
        
        const navigator = {
            mimeTypes: [],
            userAgent: 'test',
            plugins: [],
            cookieEnabled: true,
            language: 'en'
        }

        this.init(window, navigator);
    }
    
    init(window, navigator) {
        var t;
        while (this.pptr < this.poolSize) { // extract some randomness from Math.random()
            t = Math.floor(65536 * Math.random());
            this.pool[this.pptr++] = t >>> 8;
            this.pool[this.pptr++] = t & 255;
        }
        this.pptr = Math.floor(this.poolSize * Math.random());
        this.seedTime();
        // entropy
        var entropyStr = "";
        // screen size and color depth: ~4.8 to ~5.4 bits
        entropyStr += (window.screen.height * window.screen.width * window.screen.colorDepth);
        entropyStr += (window.screen.availHeight * window.screen.availWidth * window.screen.pixelDepth);
        // time zone offset: ~4 bits
        var dateObj = new Date();
        var timeZoneOffset = dateObj.getTimezoneOffset();
        entropyStr += timeZoneOffset;
        // user agent: ~8.3 to ~11.6 bits
        entropyStr += navigator.userAgent;
        // browser plugin details: ~16.2 to ~21.8 bits
        var pluginsStr = "";
        for (var i = 0; i < navigator.plugins.length; i++) {
            pluginsStr += navigator.plugins[i].name + " " + navigator.plugins[i].filename + " " + navigator.plugins[i].description + " " + navigator.plugins[i].version + ", ";
        }
        var mimeTypesStr = "";
        for (var i = 0; i < navigator.mimeTypes.length; i++) {
            mimeTypesStr += navigator.mimeTypes[i].description + " " + navigator.mimeTypes[i].type + " " + navigator.mimeTypes[i].suffixes + ", ";
        }
        entropyStr += pluginsStr + mimeTypesStr;
        // cookies and storage: 1 bit
        entropyStr += navigator.cookieEnabled + typeof (sessionStorage) + typeof (localStorage);
        // language: ~7 bit
        entropyStr += navigator.language;
        // history: ~2 bit
        entropyStr += window.history.length;
        // location
        entropyStr += window.location;

        //ToDo reactivate funcion
        /*var entropyBytes = shajs('sha256').update(entropyStr).digest();
        for (var i = 0 ; i < entropyBytes.length ; i++) {
        	this.seedInt8(entropyBytes[i]);
        }*/
    }

    // Mix in the current time (w/milliseconds) into the pool
    // NOTE: this method should be called from body click/keypress event handlers to increase entropy
    seedTime() {
        return this.seedInt(new Date().getTime());
    }

    seedInt(x)  {
        this.seedInt8(x);
        this.seedInt8((x >> 8));
        this.seedInt8((x >> 16));
        this.seedInt8((x >> 24));
        
        return this;
    }

    seedInt16(x) {
        this.seedInt8(x);
        this.seedInt8((x >> 8));
        return this;
    }

    seedInt8(x) {
        this.pool[this.pptr] ^= x & 255;
        this.pptr++;
        if (this.pptr >= this.poolSize) this.pptr = 0;
        return this;
    }

    toString() {
        return Array.from(this.pool, function (byte) {
            return ('0' + (byte & 0xFF).toString(16)).slice(-2);
        }).join('');
    }
    
    restore(hex) {
        this.state = null;
        this.pptr = 0;
        this.pool = [];

        for(var i=0; i<hex.length; i+=2) {
            this.pool.push(parseInt(hex.substr(i, 2), 16));
        }

        this.pool.length = Math.min(this.pool.length, this.poolSize);

        for(var i=this.pool.length;i<this.poolSize; i++) {
            this.pool.push(0);
        }
    }
}

module.exports = SecureRandom;
