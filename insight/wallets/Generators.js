// Import required coin libs
const BitcoinJS = require('./../libs/bitcoinjs-lib/exporter');
const EthereumJS = require('./../libs/ethereum-lib/exporter');
const NeoJS = require('./../libs/neo-lib/exporter');
const TronJS = require('./../libs/tron-lib/exporter');
const Bip39Coins = require('./../libs/bip39-coins-lib/exporter');

const Generator = require('./GeneratorsBase.js'); //plugins.require('Wallets/GeneratorsBase');
const walletGenerator = new Generator();

walletGenerator.setLibraries({
  BitcoinJS: BitcoinJS,
  EthereumJS: EthereumJS,
  NeoJS: NeoJS,
  TronJS: TronJS,
  Bip39Coins: Bip39Coins,
});

module.exports = walletGenerator;