// Seperated Object for Client/Server generators

class WalletGenerator {
    setLibraries(libs) {
      this.BitcoinJS = libs.BitcoinJS;
      this.EthereumJS = libs.EthereumJS;
      this.NeoJS = libs.NeoJS;
      this.TronJS = libs.TronJS;
      this.Bip39Coins = libs.Bip39Coins;
      this.IOTA = libs.IOTA;
    }

    BitcoinAndForks(seed, coin = 'BTC') {
        try {
          var keyPair = this.BitcoinJS.generateFromString(seed, coin);
          return {
              private: keyPair.private || keyPair.toWIF(),
              public: keyPair.public || keyPair.getAddress(),
          };
        } catch (e) {
            console.error(e)
            return false;
        }
    }

    IOTA(seed) {
        const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ9';
        var privateKey = '';

        var str = '';
        for (var i in seed)  {
            if (i % 2 === 1) {
                const val = parseInt(seed[i - 1] + seed[i], 16) % chars.length;
                privateKey += chars[val];
            }
        }

        privateKey = privateKey.substr(0, 81);

        var publicKey;

        if(process.env.VUE_ENV === 'client') {
          const IOTA = this.IOTA;
          var iota = new IOTA(); // Window Global var from iota.js

          iota.api.getNewAddress(
              privateKey, {
                  total: 1,
                  security: 3,
              },
              (err, result) => {
                  if (err) return false;
                  publicKey = typeof result === 'string' ? result : result[0];
              },
          );
        } else {
          // @todo:
          console.error('IOTA generator todo...');
          return false;
        }

        return {
            private: privateKey,
            public: publicKey,
        };
    }

    BTC(seed) {
        return this.BitcoinAndForks(seed, 'BTC');
    }
    USDT(seed) {
        return this.BitcoinAndForks(seed, 'BTC');
    }

    BCC(seed) {
      return this.BitcoinAndForks(seed, 'BCC');
    }
    BCH(seed) {
      return this.BCC(seed);
    }

    BTG(seed) {
        return this.BitcoinAndForks(seed, 'BTG');
    }

    ETH(seed) {
        var i = 0;
        while (true) {
            var buffer = Buffer.from(seed.substr(i, 64), "hex");

            const addr = this.EthereumJS.privateToAddress(buffer);
            console.log(addr)
            if (addr[0] <= 3) {
                return {
                    private: '0x' + buffer.toString('hex').toLowerCase(),
                    public: '0x' + addr.toString('hex').toLowerCase()
                }
            }

            i++;
            if (i > seed.length - 64) {
                return 'error'
            }
        }
    }

    ETC(seed) {
        return this.ETH(seed);
    }

    ADA(seed) {

    }

    DASH(seed) {
        return this.BitcoinAndForks(seed, 'DASH');
    }

    DOGE(seed) {
        return this.BitcoinAndForks(seed, 'DOGE');
    }

    LTC(seed) {
        return this.BitcoinAndForks(seed, 'LTC');
    }

    XMR(seed) {

    }

    XEM(seed) {

    }

    NEO(seed) {
        const wif = this.NeoJS.getWifFromHex(seed);
        const privateKey = this.NeoJS.getWifFromHex(this.NeoJS.getHexFromWif(wif));
        const publicKey = this.NeoJS.getAddrFromPri(this.NeoJS.getHexFromWif(privateKey));
        return {
            private: privateKey,
            public: publicKey,
        };
    }

    NXT(seed) {
        const keyPair = this.Bip39Coins.generateFromString(seed);
        return {
            private: keyPair.getPrivateKey(),
            public: keyPair.getAddress(),
        };
    }

    OMG(seed) {
        return this.ETH(seed);
    }

    QTUM(seed) {

    }

    XRP(seed) {

    }

    SC(seed) {

    }

    XLM(seed) {

    }

    TRX(seed) {
      var hex = seed.substr(0, 64);
      while (hex.length < 64) {
        hex = '0' + hex;
        i += 1;
      }

      return this.TronJS.generateAccount(hex);
    }

    ZEC(seed) {
        return this.BitcoinAndForks(seed, 'ZEC');
    }

    XVG(seed) {

    }

    VRC(seed) {

    }

}

module.exports = WalletGenerator;