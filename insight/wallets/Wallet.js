const crypto = require('crypto');
const gen    = require('./Generators.js');
const random = require('./SecureRandom.js');

class Wallet {

    constructor(uuid) {
        this.uuid            = uuid;
        this.seed            = null;
        this.seedWallets     = {};
        this.additional_keys = null;
        this.consumed_addresses = {};
        this.key             = null;
    }

    serialize(json) {
        const res = {
            seed:               this.seed,
            seedWallets:        this.seedWallets,
            additional_keys:    this.additional_keys,
            consumed_addresses: this.consumed_addresses
        }

        if(json)
            return JSON.stringify(res);

        return res;
    }

    unserialize(data) {
        if(typeof(data) === 'string')
            data = JSON.parse(data);

        this.seed            = data.seed;
        this.seedWallets     = data.seedWallets || {};
        this.additional_keys = data.additional_keys || null;
        this.consumed_addresses = data.consumed_addresses || {};

        return true;
    }

    unserializeEncrypted(content, key) {
        const res = this.decrypt(content, key);

        if(res === false)
            return false;

        return this.unserialize(res);
    }

    encrypt() {
        const data   = this.serialize(true);
        const secret = this.key;

        const decipher = crypto.createCipheriv('aes-256-cbc', secret.substr(24, 32), secret.substr(8, 16));
        decipher.setAutoPadding(true);

        var dec = decipher.update(data, 'utf8', 'base64');
        dec += decipher.final('base64');

        return dec;
    }

    decrypt(content, key) {
        try
        {
            var secret = this.generateToken(key);
            content = Buffer.from(content, 'base64');

            const decipher = crypto.createDecipheriv('aes-256-cbc', secret.substr(24, 32), secret.substr(8, 16));
            decipher.setAutoPadding(true);

            var dec = decipher.update(content);
            dec += decipher.final('utf8');

            return dec;
        }
        catch (e)
        {
            return false;
        }
    }

    get encrypted() {
        return this.encrypt();
    }

    valueOf() {
        return serialize();
    }

    setKey(key) {
        this.key = this.generateToken(key);
    }

    //---------------------------------------

    generateToken(input) {
        return Wallet.generateToken(this.uuid, input);
    }

    static generateToken(uuid, input) {
        const hash = crypto.createHash('sha256');
        hash.update(uuid + input + 'B4Y-Wallet');
        return hash.digest('hex');
    }

    static generate(words) {
        const uuid = words.join("")
        const wallet = new Wallet(uuid);
        const seed = new random(uuid); //Test if used same uuid twice, same seed regenerated ?
        seed.seedTime();
        seed.seedInt(Math.random());
        wallet.seed = seed.toString();
        return wallet;
    }

    //---------------------------------------
    
    getSeed(n, recvAddress) {
        n = n || 0;

        if(n === 0 && !recvAddress)
            return this.seed;

        const r = new random();
        r.restore(this.seed);

        if(recvAddress)
            r.pptr = 100;
        else
            r.pptr = 20;

        r.seedInt(n + 1);
        return r.toString();
    }

    importWallet(currency, key) {
        this.__modified = true;

        //ToDo verify key
        //ToDo check if is private or not

        this.additional_keys = this.additional_keys || [];
        this.additional_keys.push({
            type: currency,
            time: Date.now(),
            private: key
        })
    }

    addWallet(currency) {
        this.__modified = true;
        this.seedWallets[currency] = this.seedWallets[currency] || [];

        this.seedWallets[currency].push({
            time: Date.now(),
            //archived: false
        })

        return this.getWallet(currency, this.seedWallets[currency].length - 1);
    }

    getWallet(currency, n) {
        n = n || 0;

        if(n < 0) {
            if(this.additional_keys === null)
                throw('We could not load any imported address');

            n = Math.abs(n + 1);
            
            if(n >= this.additional_keys.length)
                throw('The requested imported address index for the exceeded the total imported wallets size.');

            return this.additional_keys[n];
        }
        else {
            //Return seeded wallet

            if(!this.seedWallets[currency])
                this.addWallet(currency)

            if(n >= this.seedWallets[currency].length)
                throw('The requested wallet index for the currency ' + currency + ' exceeded the total wallets size of this currency.');

            const walletInfo = this.seedWallets[currency][n];

            const seed   = this.getSeed(n);
            const res    = gen[currency](seed);
            if (
              typeof res !== 'object'
              || !res.private
              || !res.public
            ) {
              console.error('Something went wrong creating a new wallet. Please try again later.');
              console.error(res);
              throw 'Something went wrong creating a new wallet. Please try again later.';
            }
            res.time     = walletInfo.time;
            res.archived = walletInfo.archived || false;

            return res;
        }
    }

    getReceiveAddress(currency, n) {
        if(!currency)
            throw('No currency has been specified for the receive address');

        if(!n) {
            this.__modified = true;
            this.consumed_addresses[currency] = (this.consumed_addresses[currency] || 0) + 1;
            n = this.consumed_addresses[currency] - 1;
        }

        const seed = this.getSeed(n, true);
        return gen[currency](seed);
    }

    getWalletsCount(currency) {
        if(currency)
            return (this.seedWallets[currency] || []).length;

        return Object.keys(this.seedWallets).map((x) => this.seedWallets[x].length).reduce((a, b) => a+b, 0);
    }

    getWallets( iso = null ) {
        const res = Object.keys(this.seedWallets).map((currency) => {
            if(iso !== null && iso !== currency)
                return [];

            const length = this.seedWallets[currency].length;
            var all = [];
            
            for(var i=0; i<length; i++) {
                const r = this.getWallet(currency, i);
                r.type  = currency;
                r.n     = i;

                all.push(r);
            }

            return all;
        }).reduce((a, b) => a.concat(b), []);

        if(this.additional_keys) {
            this.additional_keys.forEach((item, index) => {
                const wallet = this.getWallet(item.type, -index-1);
                wallet.type  = item.type;
                wallet.n     = -index-1;
                
                res.push(wallet)
            });
        }

        return res.reduce((a, b) => a.concat(b), []).sort((a, b) => a.time < b.time ? -1 : 1);
    }

    archiveWallet(currency, n, archived = true) {
        this.__modified = true;

        if(!n) {
            if(this.additional_keys) {
                this.additional_keys.forEach((obj) => {
                    if(obj.type === currency)
                        obj.achived = archived;
                });
            }
            
            if(this.seedWallets[currency]) {
                this.seedWallets[currency].forEach((obj) => {
                    obj.achived = archived;
                });
            }
        }
        else if(n < 0 ){
            if(this.additional_keys === null)
                throw('We could not load any imported address');

            n = Math.abs(n + 1);
            
            if(n >= this.additional_keys.length)
                throw('The requested imported address index for the exceeded the total imported wallets size.');

            this.additional_keys[n].archived = archived;
        } else {
            if(!this.seedWallets[currency])
                throw('No wallets has been created yet for the requested currency ' + currency);

            if(n >= this.seedWallets[currency].length)
                throw('The requested wallet index for the currency ' + currency + ' exceeded the total wallets size of this currency.');

            this.seedWallets[currency][n].archived = archived;
        }
    }

    unarchive(currency, n) {
        return this.archiveWallet(currency, n, false);
    }

    //---------------------------------------

    async save() {
        throw('todo');
    }

    static async load(pin) {
        throw('todo');;
    }
}

module.exports = Wallet;

/*const wallet = Wallet.generate("+32491731813");
wallet.setKey('test');
wallet.addWallet('BTC');
console.log( wallet.encrypt() );


const wallet2 = new Wallet(wallet.uuid);
console.log( wallet2.unserializeEncrypted( wallet.encrypted, 'test' ) );
console.log( wallet2.getWallets() );*/