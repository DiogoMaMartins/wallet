module.exports = {
    "BTC": [
        {
            "ui": "https://insight.bit4you.io/btc/",
            "api": "https://insight.bit4you.io/api/btc/"
        },
        {
            "ui": "https://insight.bitpay.com/",
            "api": "https://insight.bitpay.com/api/"
        },
        {
            "ui": "https://www.localbitcoinschain.com/",
            "api": "https://www.localbitcoinschain.com/api/"
        },
        {
            "ui": "https://search.bitaccess.co/",
            "api": "https://search.bitaccess.co/insight-api/"
        },
    ],
    "BTG": [
        /*{
            "ui": "https://insight.bit4you.io/btg/",
            "api": "https://insight.bit4you.io/api/btg/"
        },*/
        {
            "ui": "https://btgexplorer.com/",
            "api": "https://btgexplorer.com/api/"
        },
        {
            "ui": "https://explorer.bitcoingold.org/insight/",
            "api": "https://explorer.bitcoingold.org/insight-api/"
        }
    ],
    "BCH": [
        /*{
            "ui": "https://insight.bit4you.io/bch/",
            "api": "https://insight.bit4you.io/api/bch/"
        },*/
        {
            "ui": "https://bch-insight.bitpay.com/#/",
            "api": "https://bch-insight.bitpay.com/api/"
        },
        {
            "ui": "https://explorer.bitcoin.com/bch/",
            "api": "https://explorer.bitcoin.com/api/bch/"
        }
    ],
    "DASH": [
        {
            "ui": "https://insight.bit4you.eu/dash/",
            "api": "https://insight.bit4you.eu/api/dash/"
        },
        {
            "ui": "https://insight.dash.org/insight/",
            "api": "https://insight.dash.org/insight-api/"
        }
    ],
    "LTC": [
        {
            "ui": "https://insight.bit4you.eu/ltc/",
            "api": "https://insight.bit4you.eu/api/ltc/"
        },
        {
            "ui": "https://ltc.explorer.berrywallet.io/",
            "api": "https://ltc.explorer.berrywallet.io/api/"
        },
        {
            "ui": "https://localbitcoinschain.com/",
            "api": "https://localbitcoinschain.com/api/"
        }
    ],
    "ZEC": [
        /*{
            "ui": "https://insight.bit4you.io/zec/",
            "api": "https://insight.bit4you.io/api/zec/"
        },*/
        {
            "ui": "https://zcashnetwork.info/",
            "api": "https://zcashnetwork.info/api/"
        },
        {
            "ui": "http://insight.mercerweiss.com/",
            "api": "http://insight.mercerweiss.com/api/"
        }
    ],
    "ETH": [
        {
            "ui": "https://insight.bit4you.eu/eth/",
            "api": "https://insight.bit4you.eu/api/eth/"
        }
    ],
    "ETC": [
        {
            "ui": "https://insight.bit4you.io/etc/",
            "api": "https://insight.bit4you.io/api/etc/"
        }
    ],
    "OMG": [
        {
            "ui": "https://insight.bit4you.io/omg/",
            "api": "https://insight.bit4you.io/api/omg/"
        }
    ],
    "TRX": [
        {
            "ui": "https://insight.bit4you.io/trx/",
            "api": "https://insight.bit4you.io/api/trx/"
        }
    ],
    "USDT": [
        {
            "ui": "https://insight.bit4you.io/usdt/",
            "api": "https://insight.bit4you.io/api/usdt/"
        }
    ]
}