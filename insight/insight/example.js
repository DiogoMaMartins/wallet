const Insight = require('./btc');
const insight = new Insight({ iso: 'BTC' });
const wallets = require('./wallets.json');

async function main() {

    //const unspents = await insight.unspents('3Q6eZFnrkH98CWgo2aYaZ7GsRr6joe47Fm');
    //console.log(unspents);

    const txBuild = insight.createTransactions();
    txBuild.source = '1E9UMGaNsisAUDTPpT1Du4oS4rF7fwcu3z';
    //txBuild.source = '3BZRWLD8XcbJwNpxm1PhXZJ7TFZxTykjt7';
    //txBuild.source = '3ESymcTzWG6sttqKM3iR1Ajixdd1oYzFP6';

    const tx = await txBuild.sign({
        outputs: {
            'address x': 500
        },
        keys: wallets,
        overflowAddress: '1E9UMGaNsisAUDTPpT1Du4oS4rF7fwcu3z',
        minConfirm: 0
    });

    console.log(tx);

    //const broadcast = await insight.broadcast( tx.hex );
    //console.log('TX Broadcasted ', tx.txid);
}

main().catch(console.error);