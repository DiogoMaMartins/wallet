const axios     = require('axios');
const servers   = require("./servers.js");
const TxBuilder = require("./tx-builder.js");
const coins     = require("./crypto-assets.json");

class Insight {
    constructor({ iso, ApiEnvironment }) {
        this.$api = ApiEnvironment || false;
        this.isServer = !!ApiEnvironment;
        this.servers = [];
        this.refCount = 0;
        this.iso = iso;
        this.cacheTtl = 900000; // 15 * 60 * 1000;
        this.cache = {};
        //this.connected  = false;

        if (iso) this.addServers(servers[iso]);

        this.__listenCacheClear();
    }

    api(name, post) {
        if (this.$api)
            return this.$api.api(name, post);

        return axios.post('/api/' + name, post);
    }

    get info() {
        return coins.find(o => o.iso === this.iso);
    }

    get digits() {
        return this.info.digits || 8;
    }

    addServers(server) {
        if (Array.isArray(server)) this.servers = this.servers.concat(server);
        else this.servers.push(server);
    }

    //-------------

    address(addr) {
        return this.$get("addr/" + addr);
    }

    balance(addr, both = false) {
        return this.$get("addr/" + addr + "/balance").then(res => {
            res = Number(res); // Hex transforms.

            if (this.iso === "BCH") {
                return parseFloat(res);
            }

            if (both) {
                return {
                    amount: res / Math.pow(10, this.digits),
                    satoshis: res,
                };
            }

            return res / Math.pow(10, this.digits);
        });
    }

    transactions(addr, page = 0) {
        return this.$get("txs?address=" + addr + "&pageNum=" + page, {
            cache: false
        });
    }

    transaction(txid) {
        return this.$get("tx/" + txid);
    }

    recommendedFees() {
        return this.$get("utils/estimatefee?nbBlocks=1,2,3").then(res => {
            console.log(res)
            if (/*res[1] === -1 ||*/ res[2] === -1 || res[3] === -1) {
                throw new Error("no data");
            }

            var spb1 = Math.round((res[1] / 1000) * 1e8);
            var spb2 = Math.round((res[2] / 1000) * 1e8);
            var spb3 = Math.round((res[3] / 1000) * 1e8);
            const r2 = {
                fastestFee: spb1,
                halfHourFee: spb2,
                hourFee:spb3
            };

            if (this.iso === "BTC") {
                for (var key in r2) r2[key] = Math.round(r2[key] / 1.2); // / 4
            }

            return r2;
        });
    }

    unspents(addr) {
        switch (this.iso) {
            // case 'USDT': 
            // case 'BCH':
            // case 'BTG': 
            // case 'DASH': 
            // case 'LTC': 
            // case 'ZEC':
            case 'BTC':
                {
                    if (Array.isArray(addr))
                        return this.$get("addrs/" + addr.join(",") + "/utxo", {
                            cache: false
                        });

                    return this.$get("addr/" + addr + "/utxo", {
                        cache: false
                    });
                }
                // case 'OMG':
                // case 'ETC':
            case 'ETH':
                {
                    return result.map(async (wallet) => {
                        const balance = await this.balance(wallet.address, true);
                        return Object.assign(balance, {
                            address: wallet.address,
                            txid: '',
                            vout: 0,
                            scriptPubKey: '',
                            height: 0,
                            confirmations: 9999,
                        });
                    });
                }
            case 'TRX':
            default:
                {
                    throw 'Coming soon...';
                }
        }
    }

    broadcast(hex) {
        return this.$post("tx/send", {
            rawtx: hex
        });
    }

    //-------------

    createTransactions() {
        return new TxBuilder(this);
    }

    //-------------

    $url(path, api = false) {
        if (path.indexOf("://") !== -1) return path;

        if (this.servers.length < 1) return "";

        const server = this.servers[0];
        return (api ? server.api : server.ui) + path;
    }

    $get(url, config = {}) {
        return (this.isServer ? this.$getServer(url, config) : this.$getBrowser(url, config));
    }

    $getBrowser(url, config = {}) {
        if (this.cache[url]) {
            const cache = this.cache[url].data; //copy in local variable in order to prevent that the cache has been removed on the next tick (Promise)
            return new Promise((resolve) => resolve(cache));
        }

        return axios.get(this.$url(url, true), config).then((res) => {
            res = res.data;
            if (config.cache === false)
                return res;

            this.cache[url] = {
                put: Date.now(),
                data: res
            };

            return res;
        }).catch(Insight.transformError)
    }

    $getServer(url, config = {}) {
        if (this.cache[url]) {
            const cache = this.cache[url].data; //copy in local variable in order to prevent that the cache has been removed on the next tick (Promise)
            return new Promise(resolve => resolve(cache));
        }

        return this.$api.httpGet(this.$url(url, true), config)
            .then(res => {
                res = res.data || res.result || res;
                if (config.cache === false) return res;

                this.cache[url] = {
                    put: Date.now(),
                    data: res
                };

                return res;
            })
            .catch(Insight.transformError);
    }

    $post(url, params = {}, config = {}) {
        return (this.isServer ? this.$postServer(url, params, config) : this.$postBrowser(url, params));
    }

    $postBrowser(url, params) {
        return axios.post(this.$url(url, true), params);
    }

    $postServer(url, params = {}, config = {}) {
        return this.$api.httpPost(this.$url(url, true), params, config);
    }

    __listenCacheClear() {
        setInterval(() => {
            for (var key in this.cache) {
                if (this.cache[key].put < Date.now() - this.cacheTtl)
                    delete this.cache[key];
            }
        }, 1000).unref();
    }

    //-------------

    static get(iso, ApiEnvironment = false) {
        Insight.instances = Insight.instances || {};
        if (!Insight.instances[iso]) Insight.instances[iso] = new Insight({
            iso,
            ApiEnvironment
        });

        Insight.instances[iso].refCount++;
        return Insight.instances[iso];
    }

    release() {
        this.refCount--;
        //Wait x seconds before closing websocket if no one is listening any more
    }

    static transformError(err) {
        if (err.error) throw err;

        if (err.response) {
            const r = err.response.data;
            if (r.error) throw r;

            throw {
                error: r || err.message,
                code: err.response.status
            };
        }

        if (err.body) {
            throw err;
        }

        if (err.message) throw {
            error: err.message,
            $e: err
        };

        if (typeof err === "string") throw {
            error: err
        };

        throw {
            error: "An unexpected error occured while connecting to the insight servers"
        };
    }
}

module.exports = Insight;
