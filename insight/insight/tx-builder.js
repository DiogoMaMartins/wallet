const feeutil = require('bitcoin-util-fee');
const CAValidator = require('cryptocurrency-address-validator');

class TxBuilder {
    constructor(insight) {
        this.$insight = insight;
        this.sources = [];
        this.fee = null;
    }

    verifyAddress(address) {
        var iso = this.$insight.iso;
        if (iso === 'USDT')
            iso = 'BTC';
        else if (iso === 'OMG') //ERC20 ??
            iso = 'ETH';

        return CAValidator.validate(address, iso);
    }

    set source(address) {
        if (!Array.isArray(address)) {
            address = [address];
        }

        address.forEach((el) => {
            if (!this.verifyAddress(el))
                throw (`The given source address ${el} is not valid`);

            this.sources.push(el);
        });
    }

    _setupFeesControl() {
        if ((!this.fee || this.fee === 'regular' || this.fee === 'fast' || this.fee === 'slow') && !this.feePerByte) {
            return this.$insight.recommendedFees().then((recommended) => {
                if (this.fee === 'slow')
                    this.feePerByte = recommended.hourFee;
                else if (this.fee === 'regular' || !this.fee)
                    this.feePerByte = recommended.halfHourFee;
                else
                    this.feePerByte = recommended.fastestFee;

                this.fee = null;
            });
        }

        return new Promise((resolve) => resolve());
    }

    calcFees(inputs, outputs) {
        if (this.fee)
            return this.fee;

        feeutil.BASE_SATOSHI_PER_BYTE = this.feePerByte;
        console.log(this.feePerByte)

        //const p2sh_tx_calc_fee_2of3 = feeutil.p2sh_tx_calc_fee_create(2, 3);
        //const satoshi = p2sh_tx_calc_fee_2of3(number_of_input, number_of_output)

        const fee = feeutil.p2pkh_tx_calc_fee(inputs.length, Object.keys(outputs).length + 1);
        console.log('bytes', Math.round(fee / this.feePerByte))
        return fee;
    }


    _setupInputs(outputs, unspents, minConfirm, overflowAddress) {
        var remaining = Object.keys(outputs).map((k) => outputs[k]).reduce((a, b) => a + b, 0);
        var inputs = [];

        unspents = unspents.sort((a, b) => a.confirmations > b.confirmations ? -1 : 1);
        for (const tx of unspents) {
            if (tx.confirmatinos < minConfirm)
                continue;

            remaining -= tx.satoshis;
            inputs.push({
                txid: tx.txid,
                vout: tx.vout,
                addr: tx.address,
                satochis: tx.satoshis
            });

            // @todo: calcFees() applies to ETH (others) aswell ??
            const fee = this.calcFees(inputs, outputs)
            console.log('fee:', fee)
            if (remaining <= -fee) {
                remaining += fee;
                break;
            }
        }

        if (remaining > 0)
            throw ('You do not have suffisent assets available to proceed this transaction, missing: ' + remaining);

        remaining *= -1;
        if (remaining > 0.00000001)
            outputs[overflowAddress || this.sources[0]] = (outputs[overflowAddress || this.sources[0]] || 0) + remaining;

        return {
            inputs: inputs,
            outputs: outputs
        };
    }

    _sendToApiServer(tx, key_token) {
        tx.key_token = key_token;
        tx.iso = this.$insight.iso;
        return this.$insight.api('wallet/sign-and-broadcast', tx);
    }

    _sendToApiBrowser(tx, $api) {
        tx.key_token = $api.getKeyToken();
        tx.iso = this.$insight.iso;
        return $api.post('wallet/sign-and-broadcast', tx);
    }

    prepare(outputs, minConfirm = 3, overflowAddress = null) {
        for (var addr in outputs) {
            if (!this.verifyAddress(addr))
                throw ('The given destination address is not valid');
        }

        return this._setupFeesControl().then(() =>
            this.$insight.unspents(this.sources).then((unspents) => this._setupInputs(outputs, unspents, minConfirm, overflowAddress))
        );
    }

    generateScript(privateKey) {
        const bitcoinjs = require('../bitcoinjs-lib/src');
        const keypair   = bitcoinjs.ECPair.fromWIF(privateKey);
        const publicKey = keypair.getPublicKeyBuffer();

        let pubkeys = [
            publicKey.toString('hex'),
            "public key 2",
            "public key 3",
            "public key 4",
        ].map((hex) => Buffer.from(hex, 'hex'));

        const redeem = bitcoinjs.payments.p2ms({ m: 3, pubkeys });
        //const script =  bitcoinjs.payments.p2sh({ redeem });

        return {
            //address: script.address,
            keypair,
            redeem,
            //script
        }
    }

    async signManualInOut(inputs, outputs, keys) {
        const bitcoinjs = require('../bitcoinjs-lib/src');
        const tx        = new bitcoinjs.TransactionBuilder();

        for (var vin of inputs)
            tx.addInput(vin.txid, vin.vout);

        for (var addr in outputs)
            tx.addOutput(addr, outputs[addr]);

        for (var i in inputs) {
            const vin = inputs[i];
            if (!keys[vin.addr])
                throw(vin.addr + ' private key not found');

            if(vin.addr.substr(0, 1) !== '3') {
                tx.sign(parseInt(i), bitcoinjs.ECPair.fromWIF(keys[vin.addr]));
            } else {
                const script  = this.generateScript(keys[vin.addr])

                tx.sign(parseInt(i), script.keypair, script.redeem.output);
                for(var key in keys) {
                    if(key.substr(0, 2) === '*.')
                        tx.sign(parseInt(i), bitcoinjs.ECPair.fromWIF(keys[key]), script.redeem.output);
                }
            }
        }

        //------------------------------------------------------

        const txbuild = tx.build();
        const buff    = txbuild.getHash();
        var txid      = new Buffer(buff.length);

        for (var i = buff.length - 1; i >= 0; i--)
            txid[buff.length - i - 1] = buff[i];

        // @todo: ToDo add extra security checks to avoid malissious transactions with abnormal amounts

        return {
            txid: txid.toString('hex'),
            hex: txbuild.toHex(),
        }
    }

    async sign({ outputs, keys, minConfirm, overflowAddress }) {
        const txi       = await this.prepare(outputs, minConfirm, overflowAddress);
        const bitcoinjs = require('../bitcoinjs-lib/src');
        const tx        = new bitcoinjs.TransactionBuilder();

        for (var addr in txi.outputs)
            tx.addOutput(addr, txi.outputs[addr]);

        const scripts = {};
        const signatures = [];
        
        var i = 0;
        for (var vin of txi.inputs) {
            if (!keys[vin.addr])
                throw(vin.addr + ' private key not found');

            tx.addInput(vin.txid, vin.vout);
            if(vin.addr.substr(0, 1) !== '3') {
                const bitcoinjs = require('../bitcoinjs-lib/src');
                signatures.push({
                    keys: [ bitcoinjs.ECPair.fromWIF(keys[vin.addr]) ]
                });
            } else {
                if(!scripts[vin.addr])
                    scripts[vin.addr]  = this.generateScript(keys[vin.addr])

                const sign = { output: scripts[vin.addr].redeem.output, keys: [ scripts[vin.addr].keypair ] };
                for(var key in keys) {
                    if(key.substr(0, 2) === '*.')
                        sign.keys.push(bitcoinjs.ECPair.fromWIF(keys[key]));
                }

                signatures.push(sign);
            }

            i++;
        }

        for(var i in signatures) {
            const sign = signatures[i];
            for(var key of sign.keys) {
                tx.sign(parseInt(i), key, sign.output);
            }
        }

        //------------------------------------------------------

        const txbuild = tx.build();
        const buff    = txbuild.getHash();
        var txid      = new Buffer(buff.length);

        for (var i = buff.length - 1; i >= 0; i--)
            txid[buff.length - i - 1] = buff[i];

        // @todo: ToDo add extra security checks to avoid malissious transactions with abnormal amounts

        return {
            txid: txid.toString('hex'),
            hex: txbuild.toHex(),
        }

    }
    
    sendBrowser(outputs, minConfirm = 3, $api) {
        if (typeof (minConfirm) === 'object') {
            $api = minConfirm;
            minConfirm = 3;
        }

        return this.prepare(outputs, minConfirm).then((tx) =>
            this._sendToApiBrowser(tx, $api)
        );
    }

    sendServer(outputs, key_token, minConfirm = 3) {
        return this.prepare(outputs, minConfirm).then((tx) =>
            this._sendToApiServer(tx, key_token)
        );
    }

    send(a, b, c) {
        if (this.$insight.isServer)
            return this.sendServer(a, b, c);

        return this.sendBrowser(a, b, c);
    }
}

module.exports = TxBuilder;
