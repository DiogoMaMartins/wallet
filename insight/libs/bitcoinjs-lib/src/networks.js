// https://en.bitcoin.it/wiki/List_of_address_prefixes
// Dogecoin BIP32 is a proposed standard: https://bitcointalk.org/index.php?topic=409731

module.exports = {
    bitcoin: {
        messagePrefix: '\x18Bitcoin Signed Message:\n',
        bech32: 'bc',
        bip32: {
            public: 0x0488b21e,
            private: 0x0488ade4
        },
        pubKeyHash: 0x00,
        scriptHash: 0x05,
        wif: 0x80
    },
    bitcoingold: {
        messagePrefix: '\x18Bitcoin Gold Signed Message:\n',
        bip32: {
            public: 0x0488b21e,
            private: 0x0488ade4
        },
        pubKeyHash: 0x26,
        scriptHash: 0x17,
        wif: 0x80
    },
    bitcoincash: {
        messagePrefix: '\x18Bitcoin Cash Signed Message:\n',
        bip32: {
            public: 0x0488b21e,
            private: 0x0488ade4
        },
        networkMagic: 0xf9beb4d9,
        pubKeyHash: 0x00,
        scriptHash: 0x05,
        wif: 0x80
    },
    dogecoin: {
        messagePrefix: '\x19Dogecoin Signed Message:\n',
        bip32: {
            public: 0x02facafd,
            private: 0x02fac398
        },
        pubKeyHash: 0x1e,
        scriptHash: 0x16,
        wif: 0x9e,
        dustThreshold: 0 // https://github.com/dogecoin/dogecoin/blob/v1.7.1/src/core.h#L155-L160
    },
    dash: {
        messagePrefix: '\x19Dash Signed Message:\n',
        bip32: {
            public: 0x0488b21e,
            private: 0x0488ade4
        },
        pubKeyHash: 0x4c,
        scriptHash: 0x10,
        wif: 0xcc,
        dustThreshold: 5460 // https://github.com/dashpay/dash/blob/v0.12.0.x/src/primitives/transaction.h#L144-L155
    },
    litecoin: {
        messagePrefix: '\x19Litecoin Signed Message:\n',
        bip32: {
            public: 0x019da462,
            private: 0x019d9cfe
        },
        pubKeyHash: 0x30,
        scriptHash: 0x32,
        wif: 0xb0
    },
    viacoin: {
        bip32: {
            public: 0x0488b21e,
            private: 0x0488ade4
        },
        dustThreshold: 560,
        feePerKb: 100000,
        magicPrefix: '\x18Viacoin Signed Message:\n',
        messagePrefix: '\x18Viacoin Signed Message:\n',
        pubKeyHash: 0x47,
        scriptHash: 0x21,
        wif: 0xc7
    },
    zcash: {
      messagePrefix: '\x18Zcash Signed Message:\n',
      bip32: {
        public: 0x0488B21E,
        private: 0x0488ADE4,
      },
      pubKeyHash: 0x1CB8,
      scriptHash: 0x1CBD,
      wif: 0x80,
    },
}
