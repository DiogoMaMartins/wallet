const bitcoin   = require('./src');
const bigi      = require('bigi');
const bchaddr   = require('bchaddrjs');

function getNetwork(coin) {
  switch (coin.toUpperCase()) {
    case 'LTC':
      return bitcoin.networks.litecoin;
    case 'BTG':
      return bitcoin.networks.bitcoingold;
    case 'BCH':
    case 'BCC':
      return bitcoin.networks.bitcoincash;
    case 'ZEC':
      return bitcoin.networks.zcash;
    case 'DOGE':
      return bitcoin.networks.dogecoin;
    case 'DASH':
      return bitcoin.networks.dash;
    default:
      return bitcoin.networks.bitcoin;
  }
}

function getBCHPublicKey(keyPair) {
  var key = '';
  if (keyPair.getAddress) key = keyPair.getAddress();
  key = String(bchaddr.toCashAddress(String(key || '')));
  key = key.split('bitcoincash:').join('');
  return key;
}

function generateFromString(str, coin = 'BTC') {
  coin = String(coin).toUpperCase();
  var network = getNetwork(coin);
  var hash = bitcoin.crypto.sha256(Buffer.from(str))
  var d = bigi.fromBuffer(hash)
  var keyPair = new bitcoin.ECPair(d, null, { network });
  switch (coin) {
    case 'BCC':
    case 'BCH':
      return {
        private: keyPair.toWIF(),
        public: getBCHPublicKey(keyPair),
      };
    default:
      return keyPair;
  }
}

function generateFromWIF(wif, coin = 'BTC') {
  coin = String(coin).toUpperCase();
  var network = getNetwork(coin);
  return bitcoin.ECPair.fromWIF(wif, network);
}

module.exports = Object.assign({}, bitcoin, {
  generateFromString,
  generateFromWIF,
  getBCHPublicKey,
  getNetwork,
});


// browserify exporter.js --standalone BitcoinJS > bitcoin-js.js

// window.BitcoinJS



/*

// -- MANUAL CHANGE bitcoin-js.js FOR ZCASH !!!!! 
// See: https://github.com/iancoleman/bip39/pull/212/commits/0702ecd3520c44cb8016f80329dcb5a3c8df88fc

// Replace toBase58Check function:

function toBase58Check (hash, version) {
  var payload;
  if (version < 256){
    typeforce(types.tuple(types.Hash160bit, types.UInt8), arguments)
    payload.writeUInt8(version, 0)
    hash.copy(payload, 1)
  } else {
    typeforce(types.tuple(types.Hash160bit, types.UInt16), arguments)
    payload = Buffer.allocUnsafe(22)
    payload.writeUInt16BE(version, 0)
    hash.copy(payload, 2)
  }
  return bs58check.encode(payload)
}

// Replace the following
pubKeyHash: typeforce.UInt8,   ->   pubKeyHash: typeforce.oneOf(typeforce.UInt8, typeforce.UInt16),
scriptHash: typeforce.UInt8,   ->   scriptHash: typeforce.oneOf(typeforce.UInt8, typeforce.UInt16),

*/
