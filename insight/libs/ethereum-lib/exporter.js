const ethereum = require('./src');

/*
function getNetwork(coin) {
  switch (coin.toUpperCase()) {
    case 'LTC':
      return bitcoin.networks.litecoin;
    case 'BTG':
      return bitcoin.networks.bitcoingold;
    case 'BCH':
      return bitcoin.networks.bitcoincash;
    default:
      return bitcoin.networks.bitcoin;
  }
}
*/

function privateToAddress(buffer) {
  return ethereum.privateToAddress(buffer);
}

module.exports = {
  privateToAddress,
};


// browserify exporter.js --standalone EthereumJS > ethereum-js.js

// window.EthereumJS