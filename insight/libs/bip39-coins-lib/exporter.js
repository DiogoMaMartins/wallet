const nxtjs = require('coinnxtjs');
const bip39 = require('bip39');

function generateFromString(str, coin = 'NXT') {
  var keyPair = false;
  switch (coin.toUpperCase()) {
    case 'NXT':
    default:
      var seed = bip39.mnemonicToSeed(str);  
      var node = nxtjs.fromSeedBuffer(seed, 'NXT');
      keyPair = node.derivePath(`m/44'/30'/0'/0/0`);
      break;
  }
  return keyPair;
}

module.exports = {
  generateFromString,
};



// browserify exporter.js --standalone Bip39Coins > bip39coins-js.js

// window.Bip39Coins