const neo = require('./src');

/*
function getNetwork(coin) {
  switch (coin.toUpperCase()) {
    case 'LTC':
      return bitcoin.networks.litecoin;
    case 'BTG':
      return bitcoin.networks.bitcoingold;
    case 'BCH':
      return bitcoin.networks.bitcoincash;
    default:
      return bitcoin.networks.bitcoin;
  }
}
*/

function getCurvePtFromHex(x) {
  return neo.crypto.getCurvePtFromHex(x);
}
function getPubFromHex(x) {
  return neo.crypto.getPubFromHex(x);
}
function getWifFromHex(x) {
  return neo.crypto.getWifFromHex(x);
}
function getHexFromWif(x) {
  return neo.crypto.getHexFromWif(x);
}
function getAddrFromPri(x) {
  return neo.crypto.getAddrFromPri(x);
}
function genPriKey() {
  return neo.crypto.genPriKey();
}

module.exports = {
  getCurvePtFromHex,
  getPubFromHex,
  getWifFromHex,
  getHexFromWif,
  getAddrFromPri,
  genPriKey,
};


// browserify exporter.js --standalone NeoJS > neo-js.js

// window.EthereumJS