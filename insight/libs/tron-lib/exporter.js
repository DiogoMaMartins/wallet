const TronAccount = require('./src/client/src/utils/account');

function generateAccount(key) {
  var account;
  if (key && key.length >= 64) {
    const private = key.slice(0, 64);
    account = TronAccount.generateAccount(private);
    // account.privateKey = private;
  } else {
    account = TronAccount.generateAccount();
  }
  return {
    private: account.privateKey,
    public: account.address,
  };
}

module.exports = {
  generateAccount: generateAccount,
};


// browserify exporter.js --standalone TronJS > tron-js.js

// window.TronJS