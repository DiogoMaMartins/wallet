const byteArray2hexStr = require("./bytes").byteArray2hexStr;
const base64EncodeToString = require("../lib/code").base64EncodeToString;
const hexStr2byteArray = require("../lib/code").hexStr2byteArray;
const {getBase58CheckAddress, genPriKey, getAddressFromPriKey} = require("./crypto");

/**
 * Generate a new account
 */
function generateAccount(str) {
  let priKeyBytes = str ? hexStr2byteArray(str) : genPriKey();
  let addressBytes = getAddressFromPriKey(priKeyBytes);
  let address = getBase58CheckAddress(addressBytes);
  let password = base64EncodeToString(priKeyBytes);
  let privateKey = byteArray2hexStr(priKeyBytes);

  return {
    privateKey,
    address,
    password,
  }
}

module.exports = {
  generateAccount,
};
