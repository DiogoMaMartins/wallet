const path = require('path');

module.exports = {
    '@components': path.join(__dirname, '../@components'),
    '@vuetify': path.join(__dirname, '../@vuetify'),
    '@config': path.join(__dirname, '../@config'),
    '@locales': path.join(__dirname, '../@locales'),
    '@menu': path.join(__dirname, '../@menu'),
    '@screens': path.join(__dirname, '../@screens'),
    '@pages': path.join(__dirname, '../@pages'),
    '@addons': path.join(__dirname, '../../src/addons'),
    '@assets': path.join(__dirname, '../../assets'),
    '@insight': path.join(__dirname, '../../insight'),
    '@tools': path.join(__dirname, '../tools'),

    //Vue only:
    "vue-native-core": path.join(__dirname, '../../src/phone/node_modules/sky-vue-native-core'),
    "vue-native-helper": path.join(__dirname, '../../src/phone/node_modules/sky-vue-native-helper'),
    "vue-native-template-compiler": path.join(__dirname, '../../src/phone/node_modules/sky-vue-native-template-compiler'),
}