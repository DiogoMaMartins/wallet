const path = require('path');

module.exports = function(platforms) {
    const mobile = platforms.indexOf('native') !== -1;

    return {
        type: "component",
        recursive: true,
        globalRegistrationFile: mobile ? path.resolve(__dirname, '../../src/addons/phone/components.js') : undefined,
        extensions: platforms.map((o) => [ `.${o}.js`, `.${o}.vue` ]).concat(['.js', '.vue']).reduce((a, b) => a.concat(b)),

        reactIgnoreCss: ['borderBottomStyle', 'borderLeftStyle', 'borderRightStyle', 'borderTopStyle'],

        folders: [
            path.resolve(__dirname, "../@components"),
            path.resolve(__dirname, "../@vuetify"),
            {
                path: path.resolve(__dirname, "../@screens"),
                pre: 'screen'
            },
            !mobile ? {
                path: path.resolve(__dirname, "../@pages"),
                routes: true,
            } : null,
            !mobile ? {
                path: path.resolve(__dirname, "../@tools"),
            } : null,
        ],
    }
}