export default {
  ios: {
    regular: {
      fontFamily: 'Exo2-Regular, sans-sarif',
    },
    medium: {
      fontFamily: 'Exo2-Medium',
    },
    extraBoldRegular: {
      fontFamily: 'Abadi MT Condensed Extra Bold Regular'
    },
    bold: {
      fontFamily: 'Exo2-Bold',
      fontWeight: 'bold',
    },
  },
  android: {
    regular: {
      fontFamily: 'Exo2-Regular',
    },
    medium: {
      fontFamily: 'Exo2-Medium',
    },
    light: {
      fontFamily: 'sans-serif-light',
    },
    condensed: {
      fontFamily: 'sans-serif-condensed',
    },
    condensed_light: {
      fontFamily: 'sans-serif-condensed',
      fontWeight: 'light',
    },
    black: {
      // note(brentvatne): sans-serif-black is only supported on Android 5+,
      // we can detect that here and use it in that case at some point.
      fontFamily: 'sans-serif',
      fontWeight: 'bold',
    },
    thin: {
      fontFamily: 'sans-serif-thin',
    },
    bold: {
      fontFamily: 'Exo2-Bold',
      fontWeight: 'bold',
    },
  },
};
