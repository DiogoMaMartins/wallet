import React from "react";
import { View, AppState } from "react-native";

import sha1 from "@addons/apiManager/sha1";
import Modal from "./message-modal";
import LoadingModal from "./loading-modal";

import ApiManager from "@addons/apiManager";
import Formatter from "@addons/formatter";
import DataBase from '@addons/database';
import Translator from '@addons/translator';
import Router from '@addons/phone/router';

import { Permissions, Notifications, Constants } from "expo";

export default class ApiComponent extends React.Component {
  constructor(props) {
    super(props);
    this.calledApiList = {};
    this.refreshing = false;
    this.__visible = true;
    this.__listenNavigation();
    this.changeListeners = {};
    if (this.$api.isTrail && !this.$api.isSimulation) {
      this.$api.setCookies({
        virtual: 1,
      });
    }
  }

  get $api() {
    return ApiManager;
  }

  get $data() {
    return DataBase;
  }

  get $format() {
      return Formatter;
  }

  get $router() {
      return Rotuer;
  }

  get $translator() {
    return Translator;
  }

  $t(...args) {
    return Translator.$t(...args);
  }

  shouldComponentUpdate() {
    const result =
      DataBase.currencies.length !== this.changeListeners.currencies || -1;
    this.changeListeners.currencies = DataBase.currencies.length;
    return result;
  }

  /*
  // Testing::
  shouldComponentUpdate() {
    const result1 =this.$data.currencies.length !== this.changeListeners.currencies || -1;
    const result2 = this.$api.isSimulation !== this.changeListeners.virtual || -1;
    const result3 = this.$data.lang !== this.changeListeners.language || -1;
    const result = Boolean(result1 || result2 || result3);
    console.log(result, this.changeListeners.virtual, this.$api.isSimulation);
    if (result) {
      this.changeListeners.currencies = this.$data.currencies.length;
      this.changeListeners.virtual = this.$api.isSimulation;
      this.changeListeners.language = this.$data.lang;
    }
    return (result || super.shouldComponentUpdate());
  }*/

  componentDidMount() {
    AppState.addEventListener("change", this._handleAppStateChange);
    this.__apiMountReady = true;

    const profile = DataBase.getProfile();
    if (profile.connected && !DataBase.getNotificationToken()) {
      this._saveNotificationToken("sending");
      this._requestNotificationToken()
        .then(expoToken => {
          this._saveNotificationToken(expoToken);
        })
        .catch(() => {
          this._saveNotificationToken(false);
        });
    }
  }

  componentWillUnmount() {
    this.__apiMountReady = false;
    AppState.removeEventListener("change", this._handleAppStateChange);

    if (this.__apiListeners) {
      this.__apiListeners.forEach(listener => {
        this.$api.unbindListener(listener);
      });

      this.__apiListeners = null;
    }

    if (this.__willBlurListener) {
      this.__willBlurListener.remove();
      this.__willBlurListener = null;
    }

    if (this.__willFocusListener) {
      this.__willFocusListener.remove();
      this.__willFocusListener = null;
    }

    if (super.componentWillUnmount) return super.componentWillUnmount();
  }

  on(name, cb) {
    var salt;

    const id = ApiManager.on(name, (data) => {
      if (!this.__apiMountReady) {
        console.warn("Event received after api unmount", name, id, this.constructor.name);
        ApiManager.unbindListener(id);
      } else if (this.__visible) {
        cb(data);
      } else {
        if (!salt) salt = sha1(Math.random() + name);

        this.__visibleWaiters[salt] = {
          cb,
          data
        };
      }
    });

    if (!this.__apiListeners) this.__apiListeners = [];

    this.__apiListeners.push(id);

    return id;
  }

  require(name, data, cb) {
    if (typeof data === "function" || data === undefined || data === null) {
      cb = data;
      data = {};
    }
    this.on(name, cb);
    this.$api.post(name, data);
  }

  bindApi(name, post, stateName) {
    // post = (post || {});
    post = ApiManager.mergePost(post || {});
    const hash = sha1(JSON.stringify({ name, post }));
    if (!this.calledApiList[hash])
        this.calledApiList[hash] = { name, post };

    const errorCbs = [];
    const dataCbs = [];

    function onData(data) {
      dataCbs.forEach(cb => {
        try {
          cb(data);
        } catch (e) {
          console.error(e);
        }
      });
    }

    function equals(a, b) {
      return JSON.stringify(a || {}) === JSON.stringify(b || {}); //ToDo accelerate method
    }

    dataCbs.push(data => {
      if (this.__apiMountReady !== undefined && !this.__apiMountReady) return;

      if (!this.__apiMountReady) {
        console.warn('[Catched!]', 'setState called in unmounted component', this.constructor.name, data);
        return;
      }

      if (stateName) {
        const state = {};
        state[stateName] = data;
        if (this.state && !equals(data, this.state[stateName])) {
          this.setState(state);
        } else if (this[stateName] && !equals(data, this[stateName])) {
          this[stateName] = data;
        }
        return;
      } else if (Array.isArray(data)) {
        if (!this.state) return;

        if (data.length === 0 && this.state.items.length === 0) return;

        if (this.state && !equals(data, this.state["items"]))
          this.setState({ items: data });
      } else {
        if (!this.state) return;
        var isSame = true;

        for (var key in data) {
          if (!equals(data[key], this.state[key])) {
            isSame = false;
            break;
          }
        }

        if (!isSame) this.setState(data);
      }
    });

    this.on(name, onData.bind(this));

    this.api(name, post)
      .then(onData.bind(this))
      .catch(function(err) {
        if (ApiManager.isDev)
            console.warn(err);

        errorCbs.forEach(cb => {
          try {
            cb(err);
          } catch (e) {
            console.error(e);
          }
        });
      });

    return {
      error(cb) {
        errorCbs.push(cb);
        return this;
      },

      data(cb) {
        dataCbs.push(cb);
        return this;
      }
    };
  }

  api(name, post) {
    return ApiManager.post(name, post);
  }

  toggleMode() {
    const newValue = Boolean((this.$api.isSimulation && this.$api.isTrial) ? true : !this.$api.isSimulation);
    this.$api.setCookies({
      virtual: Number(newValue),
    });
    setImmediate(() => {
      this.$api.post('portfolio/list', {});
    });
    return newValue;
  }

  _refreshApis() {
    if (this.calledApiList) {
      this.refreshing = this.calledApiList.length;
      if (this.setState && this.state && this.state.refreshing !== this.refreshing > 0) {
        this.setState({
          refreshing: this.refreshing > 0,
        });
      }
      Object.keys(this.calledApiList).forEach(key => {
        this.api(this.calledApiList[key].name, this.calledApiList[key].post).then(() => {
          this.refreshing -= 1;
          if (this.setState && this.state && this.state.refreshing !== this.refreshing > 0) {
            this.setState({
              refreshing: this.refreshing > 0,
            });
          }
        })
      });
    }
  }

  //------------------------------------------------

  __listenNavigation() {
    this.__visible = true;
    this.__visibleWaiters = {};

    if (!this.props.navigation) return;

    this.__visible = false;
    this.__willBlurListener = this.props.navigation.addListener(
      "willBlur",
      () => {
        this.__visible = false;
      }
    );

    this.__willFocusListener = this.props.navigation.addListener(
      "willFocus",
      () => {
        this.__visible = true;

        for (var key in this.__visibleWaiters) {
          const fn = this.__visibleWaiters[key];
          delete this.__visibleWaiters[key];
          fn.cb(fn.data);
        }
      }
    );
  }

  _handleAppStateChange = nextAppState => {
    if (
      this.appState &&
      this.appState === "background" &&
      nextAppState === "active"
    ) {
      this._refreshApis();
    }
    this.appState = nextAppState.match(/inactive|background/)
      ? "background"
      : "active";
  };

  _saveNotificationToken(expoToken) {
    if (!!expoToken || expoToken === false) {
      DataBase.setNotificationToken(expoToken);
      if (expoToken !== "sending" && expoToken !== false) {
        const OS = Object.keys(Constants.platform)[0];
        const OSInfo = OS && Constants.platform[OS];
        this.api("notifications/save-token", {
          token: expoToken,
          os: OS || null,
          osVersion: !OSInfo ? null : (OSInfo.systemVersion || OSInfo.systemVersion || OSInfo.version || '?'),
          device: Constants.deviceName,
        }).catch(() => {
          DataBase.setNotificationToken(false);
        });
      }
    }
  }

  async _requestNotificationToken() {
    try {
      const { status: existingStatus } = await Permissions.getAsync(
        Permissions.NOTIFICATIONS
      );
      let finalStatus = existingStatus;

      // only ask if permissions have not already been determined, because
      // iOS won't necessarily prompt the user a second time.
      if (existingStatus !== "granted") {
        // Android remote notification permissions are granted during the app
        // install, so this will only ask on iOS
        const { status } = await Permissions.askAsync(
          Permissions.NOTIFICATIONS
        );
        finalStatus = status;
      }

      // Stop here if the user did not grant permissions
      if (finalStatus !== "granted") return false;

      // Get the token that uniquely identifies this device
      return Notifications.getExpoPushTokenAsync();
    } catch (e) {
      return false;
    }
  }

  //------------------------------------------------

  saveProfile(Apiresult = {}, connected = true) {
    Apiresult.connected = connected;
    DataBase.setProfile(Apiresult);
  }

  getProfile() {
    return DataBase.getProfile();
  }

  getCredentials() {
    return ApiManager.credentials;
  }

  getCredentials() {
    return ApiManager.credentials;
  }

  saveCredentials(Apiresult = {}) {
    DataBase.key_token = (Apiresult.key_token || ApiManager.credentials.key_token || null);

    const credentials = {
      phone: Apiresult.phone,
    };

    if (Apiresult.phone) {
      ApiManager.isTrial = String(Apiresult.phone).indexOf('TRIAL-') === 0;
    }

    ApiManager.credentials = credentials;
    ApiManager.setCookies({
        auth: Apiresult
    });
  }

  //------------------------------------------------

  loaderApi(name, post, messages = {}) {
    this.loadingModal.show();

    return new Promise(async (resolve, reject) => {
      this.api(name, post || {})
      .then(obj => {
        this.loadingModal.hide();
        setImmediate(() => {
          if (obj.err || obj.error) {
            setImmediate(() => {
              if (messages.error) {
                if (typeof messages.error === 'string') {
                  messages.error = {
                    message: messages.error,
                  }
                } else if (typeof messages.error === 'function') {
                  const message = messages.error(err.error || err.message || err);
                  messages.error = (typeof message === 'object' ? message : {
                    message,
                  });
                }
              } else {
                messages.error = {
                  message: obj.err || obj.error,
                }
              }
              this.showError(messages.error.message, messages.error.title, messages.error.button);
            });
          } else if (messages.success) {
            if (typeof messages.success === 'string') {
              messages.success = {
                message: messages.success,
              };
            } else if (typeof messages.success === 'function') {
              const message = messages.success(obj.message || obj);
              messages.success = (typeof message === 'object' ? message : {
                message,
              });
            }
          } else if (obj.message) {
            messages.success = {
              message: obj.message,
            };
          }
          if (messages.success) {
            this.showSuccess(messages.success.message, messages.success.title, messages.success.button);
          }
          setImmediate(() => {
            resolve(obj);
          });
        });
      }).catch(err => {
        this.loadingModal.hide();
        setImmediate(() => {
          if (messages.error) {
            if (typeof messages.error === 'string') {
              messages.error = {
                message: messages.error,
              };
            } else if (typeof messages.error === 'function') {
              const message = messages.error(err.error || err.message || err);
              messages.error = (typeof message === 'object' ? message : {
                message,
              });
            } 
          } else {
            messages.error = {
              message: err.error || err.message || (typeof err === 'object' ? JSON.stringify(err) : err),
            };
          }
          if (messages.error.message) {
            this.showError(messages.error.message, messages.error.title, messages.error.button);
          }
          setImmediate(() => {
            reject(messages.error.message || this.$t('global.error1'));
          });
        });
      });
    });
  }

  showModal(type = "showError", message, title, btnText) {
    if (!this.messageModal) {
      console.error("No error modal", message);
      return;
    }
    if (typeof this.messageModal[type] !== "function") {
      console.error("Modal type", type, "not found!");
      return;
    }

    this.messageModal[type](message, title, btnText);
    return this;
  }

  showError(message, title = "Error", btnText = this.$t('global.btn15')) {
    return this.showModal("showError", message, title, btnText);
  }

  showInfo(message, title = "Info", btnText = this.$t('global.btn15')) {
    return this.showModal("showInfo", message, title, btnText);
  }

  showSuccess(message, title = "Success", btnText = this.$t('global.btn15')) {
    return this.showModal("showSuccess", message, title, btnText);
  }

  showWarning(message, title = "Warning", btnText = this.$t('global.btn15')) {
    return this.showModal("showWarning", message, title, btnText);
  }

  apiLoaderView(children) {
    return (
      <View style={{ flex: 1 }}>
        {children}
        <Modal
          ref={modal => {
            this.messageModal = modal;
          }}
        />
        <LoadingModal
          ref={modal => {
            this.loadingModal = modal;
          }}
        />
      </View>
    );
  }
}

ApiComponent.MOBILE_KEY = 'MvT952HsAHf9BN3f9WbSwQzFtsN8NY'; //ToDo disable mobile key to respect openid standards..

//------------------------------------------------
//------------------------------------------------
//------------------------------------------------
//Debug state

/*if (process.env.NODE_ENV === "development") {
  const superState = React.Component.prototype.setState;

  React.Component.prototype.setState = function(state) {
    if (
      typeof state !== "object" ||
      [
        "CardFlip",
        "NavigationContainer",
        "Transitioner",
        "YellowBox",
        "SvgUri",
        "CoinView",
        "SaveView",
        "SafeView",
        "WebView",
        "Carousel",
        "LoadingModal",
        "MessageModel",
        "AnimatableComponent",
        "ReactNativeModal",
        'PortfolioView',
        'MarketSlider',
      ].indexOf(this.constructor.name) !== -1
    )
      return superState.apply(this, arguments);

    try {
      var isSame = true;

      console.log("SetState", this.constructor.name);

      for (var key in state) {
        if (key === "refs") break;
        if ((typeof state[key] !== 'object' || typeof this.state[key] !== 'object') && state[key] !== this.state[key]) {
          isSame = false;
          break;
        } else if (JSON.stringify(state[key]) !== JSON.stringify(this.state[key])) {
          isSame = false;
          break;
        }
      }

      if (isSame)
        console.warn(
          "State replace by same state",
          this.constructor.name,
          '||',
          'New State:',
          (state),
          'Old State:',
          Object.entries(this.state).map(([key, value]) => {
            if (this.state.wallet) console.log('wallet', this.state.wallet);
            if (typeof value === 'object') return `${key}: '<object>'`;
            return `${key}: ${value}`;
          }),
        );
    } catch (e) {
      console.error(e);
    }

    try {
      return superState.apply(this, arguments);
    } catch (e) {
      console.warn(e, this.constructor.name, arguments);
    }
  };
}*/
