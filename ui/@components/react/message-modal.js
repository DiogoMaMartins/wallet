import React from 'react';
import Modal from './modal';
import Text from '@vuetify/v-text';
import Button from '@vuetify/v-btn';
import Colors from '@config/colors';
        
export default class MessageModel extends React.Component {
    state = {
        title: 'Error',
        message: null,
        btnText: 'Oke',
        color: Colors.red,
    }
    
    showError(message, title = 'Error', btnText = 'Oke') {
        this.setState({
          title,
          message,
          btnText,
          color: Colors.red,
        });
    }
    
    showSuccess(message, title = 'Success', btnText = 'Oke') {
      this.setState({
        title,
        message,
        btnText,
        color: Colors.green,
      });
    }
    
    showInfo(message, title = 'Info', btnText = 'Oke') {
        this.setState({
          title,
          message,
          btnText,
          color: Colors.blue,
        });
    }
    
    showWarning(message, title = 'Warning', btnText = 'Oke') {
        this.setState({
          title,
          message,
          btnText,
          color: Colors.yellow,
        });
    }
    
    render() {
        if (this.state.message === null) return null;
        return (
            <Modal key={String(this.state.title)+String(this.state.message)+String(this.state.btnText)} visible={true} onHide={() => { this.setState({message: null}); }} >
                <Text style={{ fontSize: 22, paddingTop: 10, paddingBottom: 10, textAlign: 'center', borderBottomWidth: 1, borderBottomColor: '#000', fontFamily: 'Exo2-Bold', color: this.state.color }}>{this.state.title}</Text>
                <Text style={{ fontSize: 16, paddingTop: 20, paddingBottom: 20 }}>{this.state.message}</Text>
                <Button round color={this.state.color + " white--text"} onClick={() => { this.setState({message: null}); }}>{this.state.btnText}</Button>
            </Modal>
        )
    }
    
}