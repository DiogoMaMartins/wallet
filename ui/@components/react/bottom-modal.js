import React from 'react';
import { View, Image, StyleSheet, Dimensions, Platform } from 'react-native';
import NativeModal from 'react-native-modal';
import { server } from '@config/server';

export default class BottomModal extends React.Component {

  state = {
      visible: false
  };

  show() {
      this.setState({
          visible: true
      })
  }

  hide() {
      this.setState({
          visible: false
      })
  }

  renderTopImage(url) {
      if(url.substr(0, 1) === '/')
          url = server + url.substr(1);

      return (
        <View style={{ alignItems: 'center', top: 0, zIndex: 3, borderRadius: 42, overflow: 'hidden' }}>
            <Image source={{ uri: url }}
                    style={{ width: 80, height: 80,  borderWidth: 2, borderColor: '#fff', backgroundColor: '#fff', borderRadius: 40 }} />
        </View>
      )
  }

  render() {
      if (!this.state.visible && !this.props.visible) return null;
      let {children, onHide, topImage} = this.props;
      onHide = onHide || (() => { this.hide() });
      return (
        <NativeModal isVisible={true} onBackdropPress={onHide} onBackButtonPress={onHide} style={styles.wrapper}>
			{topImage ? this.renderTopImage(topImage) : null}
            <View style={[styles.content, (topImage ? { paddingTop: 75, top: -40, marginBottom: -40 } : { paddingTop: 20 }), { paddingBottom: ifIphoneX(30, 20) }]}>
                {children}
            </View>
        </NativeModal>
    )
  }

}

function ifIphoneX(iphoneXStyle, regularStyle) {
  let dimen = Dimensions.get('window');

  if (Platform.OS === 'ios' &&
      !Platform.isPad &&
      !Platform.isTVOS &&
      (dimen.height >= 812 || dimen.width >= 812)) {
      return iphoneXStyle;
  } else {
      return regularStyle
  }
}

const styles = StyleSheet.create({
    wrapper: {
        alignItems: 'center',
        justifyContent: 'flex-end',
        margin: 0,
    },
    content: {
      zIndex: 2,
      paddingTop: 20,
      paddingLeft: 40,
      paddingRight: 40,
      backgroundColor: '#fff',
      width: '100%',
      borderTopLeftRadius: 15,
      borderTopRightRadius: 15,
    },
})

BottomModal.Styles = styles;