import React from 'react';
import { View, Image, StyleSheet } from 'react-native';
import NativeModal from 'react-native-modal';
import Text from '@vuetify/v-text';
import Translator from '@addons/translator';

export default class Modal extends React.Component {

  get isVisible() {
      return (this.props.item && true) || (this.props.visible && true) || false;
  }

  renderTopImage(url) {
      return (
        <View style={{ alignItems: 'center', top: 0, zIndex: 3, borderRadius: 42, overflow: 'hidden' }}>
            <Image source={{ uri: url }}
                    style={{ width: 80, height: 80,  borderWidth: 2, borderColor: '#fff', backgroundColor: '#fff', borderRadius: 40 }} />
        </View>
      )
  }
    
  render() {
      if (!this.isVisible) return null;
      let {children, topImage, onHide} = this.props;
      onHide = onHide || (() => {});
      return (
        <NativeModal isVisible={true} onBackdropPress={onHide} onBackButtonPress={onHide} style={styles.wrapper}>
            {topImage ? this.renderTopImage(topImage) : null}
            <View style={[styles.content, (topImage ? { paddingTop: 75, top: -40, marginBottom: -40 } : { paddingTop: 20 })]}>
                {children}
            </View>
        </NativeModal>
    )
  }

}


Modal.InfoView = ({name, children, style, color}) => (
    <View style={[{ flexDirection: 'row', marginBottom: 10 }, style]}>
        <Text style={{ width: 100, fontFamily: "Exo2-Bold" }}>{name && name.length > 0 ? Translator.$(name) + ':' : ''}</Text>
        <Text style={{ fontFamily: "Exo2-Medium", color: color }}>{children}</Text>
    </View>
)


const styles = StyleSheet.create({
    wrapper: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    content: {
      zIndex: 2,
      paddingTop: 75,
      paddingLeft: 40,
      paddingRight: 40,
      paddingBottom: 20,
      backgroundColor: '#fff',
      marginLeft: 10,
      marginRight: 10,
      borderRadius: 8
    },
})

Modal.Styles = styles;