import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import Modal from './modal';
import Text from '@vuetify/v-text';
import Button from '@vuetify/v-btn';
import Colors from '@config/colors';
import Translator from '@addons/translator';
        
export default class LoadingModal extends React.Component {
    state = {
        _show: false,
        __error: false,
    }
    
    show() {
        this.setState({_show: true, __error: false});
    }
    
    setError(error) {
        this.setState({_show: true, __error: error});
    }
    
    hide() {
        this.setState({_show: false});
    }
    
    onHideRequest() {
        this.setState({_show: false, __error: false});
    }

    //-----------
    
    renderLoading() {
        return (    
            <Modal key={'renderLoading'+String(this.state._show)+String(this.state.__error)} visible={this.state._show}>
                <Text style={{ fontSize: 22, paddingTop: 10, paddingBottom: 10, textAlign: 'center', fontFamily: 'Exo2-Bold' }}>{Translator.$t('global.label1')}...</Text>
                <View style={{ height: 100, justifyContent: 'center' }}>
                    <ActivityIndicator size="large" color={Colors.primary} />
                </View>
            </Modal>
        )
    }
    
    renderError() {
        return (
            <Modal key={'renderError'+String(this.state._show)+String(this.state.__error)} visible={this.state._show} onHide={this.onHideRequest.bind(this)}>
                <Text style={{ fontSize: 22, paddingTop: 10, paddingBottom: 10, textAlign: 'center', borderBottomWidth: 1, borderBottomColor: '#000', fontFamily: 'Exo2-Bold', color: Colors.red }}>{Translator.$t('global.label6')}</Text>
                <Text style={{ fontSize: 16, paddingTop: 20, paddingBottom: 20 }}>{this.state.__error}</Text>
                <Button rounded white color={Colors.red} onPress={this.onHideRequest.bind(this)}>Oke</Button>
            </Modal>
        )
    }
    
    render() {
        return !!this.state.__error ? this.renderError() : this.renderLoading();
    }
    
}