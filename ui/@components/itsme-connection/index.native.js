import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import Text from '@vuetify/v-text';
import SvgUri from 'react-native-svg-uri';
const itsmeLogo = require('@assets/images/svg/itsme-logo-whiteBackground.svg');

const ItsmeConnection = props => {
   onPress = props.onPress || (() => {});
   return (
    <TouchableOpacity onPress={onPress}>
      <View style={{ backgroundColor: '#FF4612', padding: 10, marginHorizontal: 15, borderRadius: 10, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
        <SvgUri
          width={40}
          height={40}
          style={{ alignItems: 'center', marginVertical: 5 }}
          source={itsmeLogo}
          style={{ marginRight: 10 }}
        />
        <Text style={{ fontWeight: 'bold', color: '#fff', fontSize: 18, marginLeft: 10 }}>Connect with Itsme</Text>
      </View>
    </TouchableOpacity>
  );
}

export default  ItsmeConnection;



