import React from "react";
import { View, WebView, ActivityIndicator, StyleSheet, ScrollView } from 'react-native';
import Text from '@vuetify/v-text';
import Button from '@vuetify/v-btn';
import Translator from '@addons/translator';
import Colors from '@config/colors';
import Toolbar from '@components/toolbar';

export default class webView extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
          loading: false
      };
  }

  errorView(err) {
    return (
      <View style={[this.props.errorStyle, styles.container]}>
        {this.props.errorToolbar ? <Toolbar
          leftIcon={null}
          close
          flex={0}
          dark={true}
          centerText={this.props.errorToolbar === true ? 'error' : this.props.errorToolbar}
        /> : null}

        {/*<SvgUri width={107} height={99} style={{ alignItems: 'center' }} source={require('../../assets/images/svg/no-wifi.svg')} />*/}
        <ScrollView keyboardShouldPersistTaps="always" style={{ flex: 1 }}>
          <Text style={styles.title}>
            {Translator.$t('wifi.error1')}
          </Text>
          <Text style={styles.message}>
            {Translator.$t('wifi.error2')}
          </Text>

          <Button style={styles.button} onClick={() => this.webview.reload()} color="primary">
            {Translator.$t('global.btn9')}
          </Button>
        </ScrollView>
      </View>
    );
  }

    loadingView() {
      return (<View style={[this.props.errorStyle, { flex: 1, backgroundColor: '#fff' }]}>
                {this.props.errorToolbar ? <Toolbar
                  leftIcon={null}
                  close
                  flex={0}
                  dark={true}
                  centerText={this.props.errorToolbar === true ? 'error' : this.props.errorToolbar}
                /> : null}
                <View style={{flex: 1, justifyContent: 'center'}}>
                    <ActivityIndicator size="large" color={Colors.primary} />
                </View>
             </View>)
  
    }

    render() {
        if(this.state.loading)
            return this.loadingView()

        return (
          <WebView
            ref={(item) => this.webview = item}
            style={{ backgroundColor: '#fff' }}
            renderLoading={this.loadingView.bind(this)}
            renderError={this.errorView.bind(this)}
            startInLoadingState={typeof this.props.source === 'object' && this.props.source.uri && this.props.source.uri.indexOf('.pdf') === -1}
            {...this.props}
          />
        );
    }

    setLoading(loading = true) {
      if (this.state.loaded !== loading) {
        this.setState({ loading })
      }
    }
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: '#fff',
  },

  button: {
    marginLeft: 20,
    marginRight: 20,
  },

  title: {
    marginTop: 50,
    fontSize: 20,
    textAlign: 'center',
    color: Colors.black,
    fontFamily: 'Exo2-Medium',
  },

  message: {
    textAlign: 'center',
    color: Colors.black,
    fontFamily: 'Exo2-Regular',
    marginLeft: 40,
    marginRight: 40,
    marginTop: 20,
    marginBottom: 50,
    fontSize: 14,
    lineHeight: 24,
  },
});

