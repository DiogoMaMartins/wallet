import React from 'react';
import Icon from '@vuetify/v-icon';
import { fontSize } from '@vuetify/helpers//responsive';
import { SearchBar as Search } from 'react-native-elements';
import $emit from '@vuetify/helpers/emit'

export default class SearchBar extends React.Component {

  constructor(props) {
    super(props);
    const searching = Boolean(this.props.value && this.props.value.length);
    this.state = {
      searching,
    };
  }

  componentWillReceiveProps() {
    if (this.props.value) {
      const searching = Boolean(this.props.value && this.props.value.length);
      if (searching !== this.state.searching) {
        this.setState({
          searching,
        });
      }
    }
  }

  changeText(search) {
    search = search && search.length > 0 ? search : null;
    $emit.model(this.props, 'change', search);
  }

  focus() {
      if(this.bar)
        this.bar.focus();
      else
          this.__focus = true;
  }

  componentDidMount() {
    if(this.props.focus)
        this.bar.focus();
  }
    
  render() {
    return (
      <Search
        onChangeText={this.changeText.bind(this)}
        ref={(bar) => this.bar = bar }
        lightTheme
        leftIconContainerStyle={{
          flexDirection: 'row',
          alignItems: 'center',
          alignContent: 'center',
          justifyContent: 'center',
        }}
        rightIconContainerStyle={{
          flexDirection: 'row',
          alignItems: 'center',
          alignContent: 'center',
          justifyContent: 'center',
        }}
        searchIcon={
          <Icon
            style={{
              width: 20,
              color: '#fff',
              fontSize: 18,
              padding: 0,
              margin: 0,
              marginLeft: 5,
            }}
          >
            search
          </Icon>
        }
        clearIcon={
          <Icon
            style={{
              width: 20,
              color: '#fff',
              fontSize: 18,
              padding: 0,
              margin: 0,
              marginRight: 5,
            }}
          >
            close
          </Icon>
        }
        onClear={( () => (this.changeText.call(this, '')) ).bind(this)}
        containerStyle={{
          borderRadius: 5,
          backgroundColor: '#E9EFF7',
          flexDirection: 'row',
          alignItems: 'center',
          alignContent: 'center',
          justifyContent: 'center',
          marginHorizontal: 15,
        }}
        inputContainerStyle={{
          flexDirection: 'row',
          alignItems: 'center',
          alignContent: 'center',
          justifyContent: 'center',
          paddingBottom: 4,
        }}
        inputStyle={{
          backgroundColor: 'transparent',
          fontFamily: 'Exo2-Regular',
          flex: 1,
          paddingTop: 5,
          paddingLeft: 30,
          fontSize: fontSize(18),
        }}
      />
    );
  }
};