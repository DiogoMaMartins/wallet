import React from 'react';
import { TextInput as NativeTextInput } from 'react-native';
import { fontSize } from '@vuetify/helpers/responsive';
import $emit from '@vuetify/helpers/emit'

const TextInput = (props) => {
  var { style, onChange, ...rest } = props;
  var _style = style;
  if (Array.isArray(style)) {
    _style = {};
    style.forEach(s => {
      _style = Object.assign(_style, s);
    });
  }
  _style = Object.assign({}, _style || {});
  _style.fontSize = (style.fontSize || 16);

  return (
    <NativeTextInput style={style} {...rest} onChange={(val) => $emit.model(props, 'change', val.nativeEvent.text)} />
  )
};

export default TextInput;