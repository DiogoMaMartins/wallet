import React from 'react';
import { View } from 'react-native';
import Colors from '@config/colors';
import Text from '@vuetify/v-text';
import Icon from '@vuetify/v-icon';
import $emit from '@vuetify/helpers/emit';
import TextInput from '@components/editable-input/text-input';
import Translator from '@addons/translator';

const EditableInput = (props) => {
  const { disabled, containerStyle, value, noIcon, topLabel, center, onChange, color, children, placeholder, icon, keyboardType, childWidth, ...rest } = props;
 
 

console.log(style)


  return (<View style={[styles.priceContainer, { alignItems: (topLabel ? 'flex-start' : 'center'), flexDirection: (topLabel ? 'column' : 'row') }, containerStyle]}>
      { !!children && (typeof children === 'string' ? (
        <Text style={[styles.text, (childWidth === false ? { paddingRight: 20 } : { width: 80 }), (topLabel ? { width: '100%' } : {})]}>
          {children}
        </Text>
      ) : children)}
      <View style={[(center ? { justifyContent: 'center', alignItems: 'center' } : {}), { flex: 1, flexDirection: 'column', width: '100%' }]}>
        <TextInput
          editable={!disabled}
          selectTextOnFocus={!disabled}
          value={value} 
          placeholder={Translator.$t(placeholder)}
          keyboardType={keyboardType || 'numeric'}
          keyboardDismissMode="on-drag" 
          underlineColorAndroid='transparent'
          style={{ textAlign: (center ? 'center' : 'left'), fontFamily: 'Exo2-Medium', flex: 1, marginTop: -3, color: color || Colors.black }}
          onChangeText={value => $emit.model(props, 'change', value)}
          {...rest} 
        />
      </View>
      { !noIcon && (<Icon style={{ fontSize: 20, color: color || Colors.black }}>{icon || 'write'}</Icon>) }
  </View>
  )
};

const styles = {
  text: {
    fontFamily: 'Exo2-Bold',
  },
  priceContainer: {
      marginTop: 15,
      marginLeft: 15,
      marginRight: 15,

      flexDirection: 'row', 
      justifyContent: 'center',
      alignItems: 'center',

      paddingVertical: 10,
      paddingHorizontal: 25,
      borderRadius: 6,
      height: 64,

      backgroundColor: '#fff',
      
      shadowColor: '#000',
      shadowOffset: { height: 0, width: 1 },
      shadowOpacity: 0.1,
      shadowRadius: 6,
      elevation: 3,
  },
};

export default EditableInput;
EditableInput.styles = styles;

export {
  EditableInput,
  styles,
};
