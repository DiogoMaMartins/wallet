import React from 'react';
import { FlatList } from 'react-native';
import { $emit } from '@vuetify/helpers';

export default class RefreshableList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
        };
    }

    _onRefresh() {
        this.setState({
            refreshing: true
        });
        $emit(this.props, 'refresh');
    }

    setLoading(enabled = true) {
        this.setState({
            refreshing: enabled,
        });
    }

    render() {
        const { items, onEndReached, onRefresh, keyField, children, ...rest } = this.props

        //not scrolling on android:
        //contentContainerStyle={{ flex: 1, flexGrow: 1, justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}

        return (<FlatList
            {...rest}
            data={items}
            renderItem={({ item, index }) => {
                if (children && children.render)
                    return children.render({ item, index });
                else if (children)
                    return children;
                else if (this.props.renderItem)
                    return this.props.renderItem({ item });
                else
                    return null;
            }}
            keyExtractor={(item) => keyField ? item[keyField] : (String(item.id || item.name || item.iso || JSON.stringify(item)))}
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh.bind(this)}
            onEndReached={($e) => $emit(this.props, 'endReached', $e)}
            onEndReachedThreshold={0.8}
        />);
    }
}
