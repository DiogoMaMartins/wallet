import React from 'react'
import { View, Animated, Dimensions } from 'react-native';
import { TapGestureHandler, PinchGestureHandler, PanGestureHandler, State } from 'react-native-gesture-handler';

//import { AreaChart } from '@vjsingh/react-native-svg-charts';
import * as shape from 'd3-shape';
import Color from 'color';

import Format from '@addons/formatter';
import VText from '@vuetify/v-text';
import Tooltip from './tooltip';

const deviceWidth = Dimensions.get('window').width;

/*
const Shadow = ({ line }) => {
    return ( <Path
        key={'shadow'}
        y={offset}
        d={line}
        fill={'none'}
        strokeWidth={shadowWidth}
        stroke={fillColor}
    />)
};

class DateLine extends React.PureComponent {
  render() {
    let { x, y, index, value, dateIndex, height, stroke } = this.props;
    return (
        <G x={ 0 } y={ 0 }>
          <Line
              x1={Math.floor(((dateIndex + 1) * (deviceWidth / 5)))}
              x2={Math.floor(((dateIndex + 1) * (deviceWidth / 5)))}
              y1={ 20 }
              strokeWidth={ 1 }
              y2={ y(value.y) - 1 }
              stroke={ stroke }
          />
        </G>
    );
  }
};
*/

export default class ChartMarket extends React.PureComponent {

  initialX = 0;
  lastTicksCount = 0;

  state = {
    tooltipIndex: null,
    startPoint: 0,
    showedPoints: 50,
    offset: 0,
  };

  stopVelocityScroll() {
      if(this.velocityScroll) {
          clearInterval(this.velocityScroll);
          this.velocityScroll = null;
      }
  }

  _onPinchGestureEvent(event) {
      const showedPoints = Math.min(this.length, Math.max((this.props.minPoints || 7) + 2, this.scalingPoint.count / event.nativeEvent.scale));
      const ticksWidth   = (this.width / (showedPoints - 2));

      const newX = ((this.scalingPoint.index - this.state.startPoint) * ticksWidth) - this.state.offset;
      this.scrollBy(newX - this.scalingPoint.x, {
          showedPoints: showedPoints,
      })
  }

  _onPinchHandlerStateChange(event) {
      this.stopVelocityScroll();
      if (event.nativeEvent.oldState !== State.ACTIVE) {
          const pointIndex = Math.round(event.nativeEvent.focalX * this.state.showedPoints / deviceWidth);

          this.scalingPoint = {
              x: event.nativeEvent.focalX,
              index: pointIndex + this.state.startPoint,
              count: this.state.showedPoints
          }
      }
  }

  _onTiltGestureEvent(event) {
      this.scrollBy(this.initialX-event.nativeEvent.x);
      this.initialX = event.nativeEvent.x;
  }

  _onTiltGestureStateChange(event) {
      this.stopVelocityScroll();
      if (event.nativeEvent.oldState !== State.ACTIVE) {
        this.initialX = event.nativeEvent.x;
      } else {
          var done = 0;
          var total = 200 / 10;
          var velocity = event.nativeEvent.velocityX;

          var interval = setInterval(() => {
              var step = -(velocity / (total * (1 + (done * 20 / total))));
              this.scrollBy(step);

              done++;
              if(done >= total) {
                  clearInterval(interval);
                  if(this.velocityScroll === interval)
                      this.velocityScroll = null;
              }
          }, 10);

          this.velocityScroll = interval;
      }
  }

  scrollBy(x, addState = {}) {
      const tickWidth = this.tickWidth;
      var offset = this.state.offset + x;

      var offsetTicks = Math.floor(offset / tickWidth)
      offset = offset - (offsetTicks * tickWidth);

      if(this.state.startPoint + offsetTicks < 0) {
          offsetTicks = -this.state.startPoint;
          offset = 0;
      } else if(this.state.startPoint + offsetTicks + this.state.showedPoints >= this.length) {
          offsetTicks = (this.length - this.state.showedPoints) - this.state.startPoint;
          offset = 0;
      } else if(this.state.startPoint + this.state.showedPoints >= this.length && offset > 0) {
          offset = 0;
      }

      this.setState(Object.assign({
          offset,
          startPoint: this.state.startPoint + offsetTicks,
          tooltipIndex: null
      }, addState || {}))
  }

  _onTapGestureEvent(event) {
      //console.log('tap gesture')
  }

  _onTapGestureStateChange(event) {
    if (event.nativeEvent.oldState !== State.ACTIVE) return;
    if (event.nativeEvent.state !== State.END) return;

    this.stopVelocityScroll();
    const tooltipIndex = Math.round(event.nativeEvent.absoluteX * this.state.showedPoints / deviceWidth);
    this.setState({
        tooltipIndex,
    });
  }

  get length() {
      const { data } = this.props;
      return data ? data.length : 0;
  }

  get data() {
      const { data } = this.props;
      if(!data)
          return [];

      return data.concat([{ y: 0 }]).slice(Math.max(0, Math.min(this.state.startPoint, data.length - this.state.showedPoints)), this.state.startPoint + this.state.showedPoints);
  }

  get tickWidth() {
      return this.width / (this.state.showedPoints - 2);
  }

  get width() {
      return (this.bounds && this.bounds.width) || deviceWidth;
  }

  get yMin() {
      if(this.length < 2)
          return 0;

      return this.data.reduce((a, b) => Math.min(a.y || a, b.y)) * 0.9;
  }

  render() {
    let {
      strokeWidth,
      shadowWidth,
      shadow,
      grid,
      height,
      color,
      data,
      fillColor,
      offset,
      tooltip,
      style,
      market,
    } = this.props;

    color = color || '#651FFF';
    fillColor = fillColor || Color(color).lighten(0.9).hex();
    offset = offset || 2;
    height = height || 200;
    grid = !!(grid || false);
    shadow = !!(shadow || false);
    tooltip = !!(tooltip || false);
    strokeWidth = strokeWidth || 4;
    shadowWidth = shadowWidth || 2;

    if(this.length > this.lastTicksCount) {
        if(this.state.startPoint + this.state.showedPoints >= this.lastTicksCount) {
            this.state.offset = 0;
            this.state.startPoint = this.length - this.state.showedPoints;
        }

        this.lastTicksCount = this.length;
    }

    const tooltipLine = Color(color).lighten(0.6).hex();
    const tooltipCard = Color(color).hex();
    let tooltipIndex = !this.data.length ? null : (
        this.state.tooltipIndex >= 0 ? this.state.tooltipIndex : this.data.length - 1
    );

    const xoffset = this.state.offset;
    const dates = [];
    const datesX = [];
    const dateWidth = (this.width) / 5;
    for(var i=0; i<5; i++) {
      const index = Math.round(this.state.startPoint + Math.round(this.state.showedPoints * (i + 1) / 6));
      if(index >= this.length || !data[index] || !data[index].x)
        continue;
      var d = new Date(data[index].x * 1000);
      yyyy = d.getFullYear(),
      dd = ('0' + d.getDate()).slice(-2),
      mm = ('0' + (d.getMonth() + 1)).slice(-2);
      datesX.push(data[index].x);
      dates.push(dd + '/' +  mm + '/' + yyyy);
    }

    return (
      <View style={[{ marginLeft: -40, marginRight: -40, flex: 1, overflow: 'hidden', height: 235, maxHeight: 235 }, style]}>
          <View style={{ flexDirection: 'row', marginHorizontal: 20, zIndex: 0 }}>
            {
              dates.map((date) => (
                <View style={{ alignItems: 'center', justifyCenter: 'center', flex: 1, width: dateWidth }} key={date}>
                    <VText medium>{date.substr(0, 5)}</VText>
                    <VText color="grey">{date.substr(6)}</VText>
                </View>
              ))
            }
          </View>
          <PanGestureHandler
                ref={this.panRef}
                onGestureEvent={this._onTiltGestureEvent.bind(this)}
                onHandlerStateChange={this._onTiltGestureStateChange.bind(this)}
                minPointers={1}
                maxPointers={2}
                activeOffsetX={[-10, 10]}
                avgTouches>
              <Animated.View style={{ flex: 1, overflow: 'hidden', zIndex: 3 }}>
                  <PinchGestureHandler
                    ref={this.pinchRef}
                    style={{ zIndex: 3 }}
                    simultaneousHandlers={this.panRef}
                    onGestureEvent={this._onPinchGestureEvent.bind(this)}
                    onHandlerStateChange={this._onPinchHandlerStateChange.bind(this)}>
                      <Animated.View style={{ flex: 1, marginLeft: -xoffset, marginRight: xoffset, zIndex: 3 }}>
                          <TapGestureHandler
                              style={{ zIndex: 3 }}
                              simultaneousHandlers={this.pinchRef}
                              onGestureEvent={this._onTapGestureEvent.bind(this)}
                              onHandlerStateChange={this._onTapGestureStateChange.bind(this)}>
                              <Animated.View style={{ flex: 1, marginLeft: -this.tickWidth, marginRight: -this.tickWidth, zIndex: 3 }} onLayout={(event) => { this.bounds = {x, y, width, height} = event.nativeEvent.layout; }}>
                                  <AreaChart
                                      style={{ height: 200, width: '100%', zIndex: 3 }}
                                      data={this.data.map((o) => o.y)}
                                      strokeWidth={strokeWidth}
                                      gridMin={0}
                                      showGrid={false}
                                      animate={false}
                                      animationDuration={0}
                                      yMin={this.yMin}
                                      curve={shape.curveLinear}
                                      yAccessor={({ item, index }) => item}
                                      xAccessor={({ item, index }) => index}
                                      svg={{ fill: fillColor, stroke: color, strokeWidth }}
                                      contentInset={{ top: 40, bottom: -2, left: -1, right: -1 }}
                                    >
                                      {/*this.data.map((el, index) => {
                                        if (!el.x || datesX.indexOf(el.x) === -1) return null;
                                        return (
                                          <DateLine
                                            key={el.x}
                                            stroke={ fillColor }
                                            dateIndex={ datesX.indexOf(el.x) }
                                            dateWidth={dateWidth}
                                            value={ el }
                                          />
                                        );
                                      })*/}
                                      {this.data.map((el, index) => {
                                        if (tooltipIndex === null || index !== tooltipIndex) return null;
                                        var text = ''
                                        if(market.baseMarket !== 'USDT')
                                          text = market.baseMarket + ' ' + (el.y).toFixed(market.precision || 2)
                                        else
                                          text = Format.price(el.y, market.precision)

                                        return (
                                          <Tooltip
                                            key={this.state.tooltipIndex}
                                            stroke={ tooltipLine }
                                            pointStroke={ color }
                                            pointFill={ 'white' }
                                            cardColor={ tooltipCard }
                                            key={ index }
                                            index={ index }
                                            value={ el }
                                            text={ text }
                                          />
                                        );
                                      })}
                                  </AreaChart>
                                    {/*
                                      <XAxis
                                          style={{ marginHorizontal: -10 }}
                                          data={ _data }
                                          formatLabel={ (value, index) => index }
                                          contentInset={{ left: 10, right: 10 }}
                                          svg={{ fontSize: 10, fill: 'black' }}
                                      />
                                    */}
                              </Animated.View>
                            </TapGestureHandler>
                        </Animated.View>
                   </PinchGestureHandler>
              </Animated.View>
            </PanGestureHandler>
            <View style={{ position: 'absolute', left: 0, top: 0, right: 0, bottom: 0, zIndex: 0 }}>
              <View style={{ flexDirection: 'row', marginHorizontal: 20, zIndex: 0 }}>
                {
                  dates.map((date) => (
                    <View style={{ alignItems: 'center', justifyCenter: 'center', flex: 1, width: dateWidth }} key={date}>
                        <View style={{ height: 180, backgroundColor: '#E6F0FF', width: 1 }}></View>
                    </View>
                  ))
                }
              </View>
            </View>
        </View>
    );
  }
};
