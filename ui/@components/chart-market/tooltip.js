import React from 'react'
import { Circle, G, Line, Rect, Text } from 'react-native-svg'
import { Dimensions } from 'react-native'

const deviceWidth = Dimensions.get('window').width;

const Tooltip = ({ x, y, value, index, height, text, stroke, pointStroke, pointFill, cardColor, onPress }) => {
    onPress = onPress || (() => {});

    const width = Math.max(120 + 2, Math.floor(String(text).length * 10) + 2);
    
    const check1 = (x(index) <= width);
    const check2 = (x(index) + width >= deviceWidth);
    
    let xOffset = x(index) - 20;
    if (check1) xOffset = (width / 2) + 10;
    else if (check2) xOffset = deviceWidth - (width / 2) - 10;
    
    const date = new Date(value.x ? (value.x * 1000) : Date.now());
    let h = date.getHours();
    let m = date.getMinutes();
    if (h < 10) h = '0'+h;
    if (m < 10) m = '0'+m;
    return (
        <G onPress={onPress}>
            <Line
                x1={ x(index) }
                y1={ height }
                x2={ x(index) }
                strokeWidth={ 2 }
                y2={ y(value.y) }
                stroke={ stroke }
            />
            <Circle
                cx={ x(index) }
                cy={ y(value.y) }
                r={ 4 }
                stroke={ pointStroke }
                strokeWidth={ 2 }
                fill={ pointFill || 'white' }
            />
            <G x={ xOffset } y={ 20 }>
                <Rect
                    x={ -41 }
                    y={ -16 }
                    width={ width }
                    height={ 22 }
                    fill={ cardColor }
                    rx={ 2 }
                    ry={ 2 }
                />
                <Rect
                    x={ -40 }
                    y={ -15 }
                    width={ width - 2 }
                    height={ 20 }
                    fill={ 'white' }
                    rx={ 2 }
                    ry={ 2 }
                />
                <Text
                    x={ Math.round(-41 + (width / 2)) }
                    fontSize="12"
                    textAnchor="middle"
                    fill={cardColor}
                    fontWeight="normal"
                    
                >
                    { text }
                </Text>
            </G>
            {!!value.x && (
              <G x={ xOffset } y={ height - 23 }>
                  <Rect
                      x={ -41 }
                      y={ -1 }
                      width={ width }
                      height={ 22 }
                      fill={ cardColor }
                      rx={ 2 }
                      ry={ 2 }
                  />
                  <Rect
                      x={ -40 }
                      y={ 0 }
                      width={ width - 2 }
                      height={ 20 }
                      fill={ 'white' }
                      rx={ 2 }
                      ry={ 2 }
                  />
                  <Text
                      x={ Math.round(-41 + (width / 2)) }
                      y={ 14 }
                      fontSize="12"
                      textAnchor="middle"
                      fill={cardColor}
                      fontWeight="normal"
                  >
                      { date.toLocaleDateString() }
                  </Text>
              </G>
            )}
            {/*
            <G x={ deviceWidth / 2 } y={ 20 }>
                <Rect
                    x={ -75 + 19 }
                    y={ height - 44 }
                    width={ 150 }
                    height={ 22 }
                    fill={ cardColor }
                    rx={ 2 }
                    ry={ 2 }
                />
                <Rect
                    x={ -75 + 21 }
                    y={ height - 42 }
                    width={ 146 }
                    height={ 18 }
                    fill={ 'white' }
                    rx={ 2 }
                    ry={ 2 }
                />
                <Text
                    x={ 21 }
                    y={ height - 28 }
                    height={ 22 }
                    fontSize="12"
                    textAnchor="middle"
                    fill={cardColor}
                    fontWeight="normal"
                >
                    { [date.toLocaleDateString(), [new Date(date).getHours(), new Date(date).getMinutes()].join(':')].join(' ') }
                </Text>
            </G>
            */}
        </G>
    )
}

export default Tooltip