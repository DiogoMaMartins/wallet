import React from 'react';
import { View, TouchableOpacity, ScrollView } from 'react-native';
import Toolbar from '../toolbar';
import Colors from '@config/colors';
import Text from '@vuetify/v-text';
import $emit from '@vuetify/helpers/emit';

const scrollItemStyle = {
    color: '#fff',
    fontSize: 12,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 15,
    paddingBottom: 15
}

const scrollItemActive = {
    borderBottomWidth: 4,
    borderColor: '#fff',
    marginBottom: 1
}

let TabsView = ({ style, children, light }) => (
    <View style={[{ height: 50 }, (light && { backgroundColor: '#fff' })]}>
        <ScrollView style={[{ backgroundColor: Toolbar.Color, flex: 1 }, style, (light && { backgroundColor: '#fff' })]} horizontal showsHorizontalScrollIndicator={false}>
            {children}
        </ScrollView>
    </View>
)

TabsView.Item = ({children, active, onPress, onClick, i18n, light}) => {
    onPress = onPress || (() => $emit({ onClick }, 'click'));
    return (
        <TouchableOpacity style={ [(active ? scrollItemActive : null), (light && { backgroundColor: '#fff', borderColor: Colors.black })] } onPress={onPress} activeOpacity={1}>
            <Text style={[scrollItemStyle, (light && { color: Colors.black })]} i18n={i18n}>{children}</Text>
        </TouchableOpacity>
    )
}

export default TabsView