import React from 'react';
import { View } from 'react-native';
import { CheckBox } from 'react-native-elements';
import Text from '@vuetify/v-text';
import Icon from '@vuetify/v-icon';
import Button from '@vuetify/v-btn';
import { styles as EditableInputStyles } from '../editable-input';
import { ImagePicker, Permissions } from 'expo';
import colors from '@config/colors';
import { connectActionSheet } from '@expo/react-native-action-sheet';

class ImagePickerComponent extends React.Component {
  state = {
    image: null,
  };

  setImage(image) {
    const { onChange } = this.props;
    this.setState({ image }, () => {
      if (onChange) onChange(image);
    });
  }

  render() {
    let { successText, changeText, uploadText, label, height } = this.props;
    successText = successText || 'Picture selected!';
    changeText = changeText || 'Change picture';
    uploadText = uploadText || 'Upload image';
    height = height || -1;
    label = label || null;
    const hasImage = !!this.state.image;
    return (
      <View style={{ flexDirection: 'row', justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>
        <View style={[EditableInputStyles.priceContainer, (!!label ? { height: 96 } : { height: 72 }), (height > -1 ? { height } : {}), { flex: 1, padding: 10, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', borderStyle: 'dashed', borderWidth: 3, borderColor: (hasImage ? colors.green : colors.lightGray) }]}>
          { !!label && !this.state.image && (
            <Text style={EditableInputStyles.text}>{label}</Text>
          )}
          { !!this.state.image && (
            <View style={{ flexDirection: 'row', paddingVertical: 5 }}>
              <Icon style={{ color: colors.green }}>check</Icon>
              <Text style={[EditableInputStyles.text, { color: colors.green, marginLeft: 10 }]}>{successText}</Text>
            </View>
          )}
          <Button
            fontSize={14}
            round
            color={hasImage ? 'grey white--text' : 'success white--text'}
            buttonStyle={{ paddingHorizontal: 10, paddingVertical: 0, height: 32 }}
            style={{ marginVertical: 3, justifyContent: 'center', alignItems: 'center' }}
            onClick={(() => { this._pickImage.call(this, false); }).bind(this)}
          >
            <Icon size={12}>{ hasImage ? 'icon-undo' : 'icon-copy' }</Icon> { hasImage ? changeText : uploadText }
          </Button>
        </View>
      </View>
    );
  }

  _pickImage () {
    if (this.state.image) {
      this.setImage('');
    }

    const options = ['Take a picture', 'Select from gallery', 'Cancel'];
    const cancelButtonIndex = 2;
    
    this.props.showActionSheetWithOptions({
      options,
      cancelButtonIndex,
    },
    async (buttonIndex) => {
      if (buttonIndex === cancelButtonIndex) return;
      const isCamera = buttonIndex === 0;
      const p1 = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      const p2 = await Permissions.askAsync(Permissions.CAMERA);
      if (p1.status === 'granted' && p2.status === 'granted') {
        ImagePicker[isCamera ? 'launchCameraAsync' : 'launchImageLibraryAsync']({
          allowsEditing: false,
          aspect: [16, 9],
          quality: 0.8,
          base64: true,
          exif: false
        })
        .then((result) => {
          if (!result.cancelled) {
            this.setImage(result.base64)
          }
        })
        .catch(err => console.error(err));
      }
    });
  };
}

export default connectActionSheet(ImagePickerComponent);
