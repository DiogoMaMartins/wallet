/* https://introjs.com/docs/ */

const tutorialMixin = require("./mixin-tutorials");

module.exports = {
  mixins: [tutorialMixin],
  metaInfo() {
    return {
      script: [
        {
          src: 'https://cdnjs.cloudflare.com/ajax/libs/intro.js/2.9.3/intro.min.js',
        }
      ],
    };
  },
  created() {
    if (this.$isServer) return;
    // Vue meta on page refresh not adding script fix...
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://cdnjs.cloudflare.com/ajax/libs/intro.js/2.9.3/intro.min.js';
    head.appendChild(script);
  },
  data() {
    return {
      windowProp: 'introJs',
      stopChangingCb: false,
      completed: false,
      steps: [],
      interval: null,
      disableComplete: false,
      api_send: false,
      options: {
        nextLabel: this.$translate("btn.9"),
        prevLabel: this.$translate("btn.1"),
        doneLabel: this.$translate("btn.18"),
        skipLabel: this.$translate("btn.20"),
        closeTooltip: this.$translate("btn.3"),
        tooltipClass: 'introjs-tooltip',
        highlightClass: 'introjs-highlight',
        showProgress: false,
        showBullets: false,
        showButtons: true,
        keyboardNavigation: false,
        scrollToElement: false,
        showStepNumbers: false,
        exitOnOverlayClick: false,
        exitOnEsc: true,
        hideSkip: true,
        hideNext: true,
        hidePrev: true,
        overlayOpacity: 0.3
      },
    };
  },

  methods: {
    waitForEl(selector) {
      return new Promise((resolve) => {
        // this.running && this.running.exit(true);
        let timeout = null;
        const __waitForEl = (tries = 1) => {
          if (tries > 20) return resolve(false);
          timeout = setTimeout(() => {
            if (document.querySelectorAll(selector).length) {
              clearTimeout(timeout);
              return this.$nextTick(() => {
                resolve(true);
              });
            }
            return __waitForEl();
          }, 120);
        };
        __waitForEl();
      });
    },
    startTutorial(retry = this.retry, step = 1, firstStep = true) {
      this.completed = false;
      this.running = null;
      this.$set(
        this.options,
        'nextLabel',
        this.$translate(firstStep ? 'btn.19' : 'btn.9')
      );
      this.$set(
        this.options, 
        'skipLabel', 
        this.$translate(firstStep ? 'btn.20' : 'btn.18')
      );
      const running = introJs()
        .addSteps(this.steps.map((s) => {
          if (s.selector) s.element = document.querySelectorAll(s.selector)[0];
          else if (s.id) s.element = document.querySelectorAll(`#${s.id}`)[0];
          else if (s.class) s.element = document.querySelectorAll(`.${s.class}`)[0];
          return s;
        }))
        .goToStepNumber(step)
        .onchange(() => {
          if (!this.running || this.stopChangingCb) return;
          const currentActive = (running && running._currentStep || 0);
          this.steps.some((s, index) => {
            const Indexed = (index === (currentActive - 1));
            if (Indexed && s.onNext) {
              this.stopChangingCb = true;
              running.previousStep();
              s.onNext().then(() => {
                this.$nextTick(() => {
                  this.ready(() => {
                    this.startTutorial(retry, currentActive + 1, false);
                    this.$nextTick(() => {
                      this.stopChangingCb = false;
                    });
                  });
                });
              });
              this.running.refresh();
              return false;
            } else {
              this.stopChangingCb = false;
              this.startTutorial(retry, currentActive + 1, false);
            }
            return Indexed || currentActive === -1;
          });
          this.running.refresh();
          if (this.$ga) {
            try {
              this.$ga.event({
                eventCategory: `tutorial-${this.getName()}${
                  this.tour.variant ? `:v-${this.tour.variant}` : ''
                }`,
                eventAction: "step-complete",
                eventLabel: `Step ${step}`,
                eventValue: 1
              });
            } catch (e) {
              // Nothing
            }
          }
        })
        .oncomplete((e) => {
          this.endTutorial(true);
        })
        .onexit((e) => {
          this.endTutorial(true);
        })
        .setOptions(Object.assign(
          {},
          this.options,
          (step > 1 && step < this.steps.length ? { showProgress: true } : {})
        ))
        .start();
      this.running = running;
      this.$emit("start", running);
      if (!this.interval) {
        this.interval = setInterval(() => {
          try {
            var el = document;
            var event = document.createEvent('HTMLEvents');
            event.initEvent('resize', true, false);
            el.dispatchEvent(event);
          } catch (e) { console.error(e); }
          // $(window).trigger("resize");
          if (!this.running) {
            clearInterval(this.interval);
          }
        }, 3000);
      }
      return running;
    },
    endTutorial(completed, justApi = false) {
      if (this.$isServer) return;
      if (!justApi) {
        this.running = null;
        this.$emit("end", true);
      }
      this.completed = completed || false;
      try {
        if (!this.api_send && this.$ga) {
          this.$ga.event({
            eventCategory: `tutorial-${this.getName()}${
              this.tour.variant ? `:v-${this.tour.variant}` : ""
            }`,
            eventAction: `tutorial-${(completed ? 'completed' : 'ended')}`,
            eventLabel: 'Ending',
            eventValue: 1
          });
        }
      } catch (e) {
        // Nothing
      }
      if (!completed && (this.completed || this.disableComplete)) return;
      if (!justApi) {
        if (this.afterTutorial) this.afterTutorial(this.getName());
        if (this.$root.afterTutorial) this.$root.afterTutorial(this.getName());
      }
      if (!this.api_send) {
        this.setCompleted();
      }
      this.api_send = true;
    }
  }
};
