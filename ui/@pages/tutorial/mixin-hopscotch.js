/* http://linkedin.github.io/hopscotch/ */

const tutorialMixin = require('./mixin-tutorials');

module.exports = {
    mixins: [tutorialMixin],

    metaInfo() {
        return {
            script: [{
                src: 'https://cdnjs.cloudflare.com/ajax/libs/hopscotch/0.3.1/js/hopscotch.min.js',
            }],
            link: [{
                rel: 'stylesheet',
                href: 'https://cdnjs.cloudflare.com/ajax/libs/hopscotch/0.3.1/css/hopscotch.min.css'
            }]
        };
    },

    data() {
      return {
        windowProp: 'hopscotch',
      };
    },

    mounted() {
      this.checkStepTargets();
    },

    methods: {
        checkStepTargets() {
          if (
            process.env.NODE_ENV !== 'development'
            || this.$isServer
            || !this.tour
            || !this.tour.steps
          ) return;
          this.tour.steps.forEach((el) => {
            if (!document.querySelector('#'+el.target)) {
              console.warn('Step target not found:', `#${el.target}`, `(${el.title})`);
            }
          });
        },
        startTutorial() {
          var tour = Object.assign({}, this.tour, {
              showNextButton: true,
              showPrevButton: true,
              showSkip: false,
              skipIfNoElement: true,
              showCloseButton: true,
              i18n: {
                  nextBtn: this.$translate('btn.9'),
                  prevBtn: this.$translate('btn.1'),
                  doneBtn: this.$translate('btn.10'),
                  skipBtn: this.$translate('btn.11'),
                  closeTooltip: this.$translate('btn.3'),
              },
              onNext: () => {
                if (this.$ga) {
                  try {
                    this.$ga.event({
                      eventCategory: `tutorial-${this.getName()}${this.tour.variant ? `:v-${this.tour.variant}` : ''}`,
                      eventAction: 'step-complete',
                      eventLabel: `Step ${window.hopscotch.getCurrStepNum()}`,
                      eventValue: 1,
                    });
                  } catch (e) {
                    // Nothing
                  }
                }
                if (this.tour.onNext) this.tour.onNext();
              },
              onError: () => {
                  if (this.tour.onError) this.tour.onError();
                  this.startTutorial();
              },
              onStart: () => {
                  window.hopscotch.showStep(0);
                  this.$emit('start', true);
              },
              onEnd: () => {
                  try {
                    this.$ga.event({
                      eventCategory: `tutorial-${this.getName()}${this.tour.variant ? `:v-${this.tour.variant}` : ''}`,
                      eventAction: 'tutorial-ended',
                      eventLabel: `Ending`,
                      eventValue: 1,
                    });
                  } catch (e) {
                    // Nothing
                  }
                  if (this.$root.afterTutorial) this.$root.afterTutorial();
                  this.$emit('end', true);
                  this.endTutorial();
              },
          });
          this.$nextTick(() => {
              window.hopscotch.startTour(tour, 0);
          });
        },
        endTutorial(fromTutorial) {
            if (this.$isServer) return;
            window.hopscotch.resetDefaultOptions();
            if (window.hopscotch.getCurrTour()) {
                window.hopscotch.endTour(true);
            }
            this.$nextTick(() => {
                $('.hopscotch-bubble').remove();
            });
            if (fromTutorial === false) return;
            this.setCompleted();
        },
    },
};
