module.exports = {

    data() {
        return {
            api: 'platform/tutorials',
            initialRoute: String(this.$route.path).toLowerCase(),
            tutorials: {},
            changingRoute: false,
            retry: 0,
            started: false,
        };
    },

    watch: {
        $route() {
            if (this.changingRoute) {
              this.changingRoute = false;
            } else if (String(this.$route.path).toLowerCase().indexOf(this.initialRoute) === -1) {
              this.endTutorial(false);
            }
        },
    },

    mounted() {
      this.$nextTick(this.runTutorial);
    },

    beforeDestroy() {
      this.endTutorial(false);
    },

    methods: {
        getName(name) {
            return String(name
              || this.tutorial || this.name || '__tutorial').toLowerCase();
        },
        checkCompleted() {
            const tutorial = this.getName();
            return (this.tutorials[tutorial] || false);
        },
        ready(callback) {
            if (this.$isServer) return true;
            if (document.readyState != 'loading') callback();
            else if (document.addEventListener) document.addEventListener('DOMContentLoaded', callback);
            else document.attachEvent('onreadystatechange', function () {
                if (document.readyState == 'complete') callback();
            });
        },
        runTutorial(retry = this.retry) {
            if (retry < 100) {
                this.timeout = setTimeout(() => {
                    this.retry = retry + 1;
                    this.prepareTutorial(this.retry);
                }, 350);
            }
        },
        prepareTutorial() {
          this.started = false;
          this.ready(() => {
            if (!window[this.windowProp] || Object.keys(this.tutorials).length === 0) {
              return this.runTutorial();
            }
            if (this.checkCompleted()) {
              return;
            }
            if (this.$root.preTutorial) { 
              this.$root.preTutorial(this.getName());
            }
            this.retry = 0;
            this.started = true;
            this.startTutorial();
          });
        },
        setCompleted() {
          if (!this.started) return;
          const tutorial = this.getName();
          if (tutorial) {
            this.$api.post('platform/tutorials', {
              tutorial,
            }).catch(console.error);
          }
        },
    },

};