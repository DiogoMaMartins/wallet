module.exports = {

    mounted() {
        /*if (this.$isServer) return;
        this.$nextTick(() => {
            //this is wrong => it force demo mode everytime we change of page even if the tutorial is not active..
          this.setMode(true);
        });*/
    },
    methods: {
        setMode(virtual) {
            if (this.$isServer)
                return;

            virtual = Boolean(virtual);
            if (virtual !== this.$api.isSimulation) {
                this.$api.isSimulation = virtual;
                this.$emit('update:virtualPortfolio', virtual);
            }
        },
    },

};
