import Servers from '@config/server';

export default {
    mounted() {
        var field = this.marketIso ? 'lastDepositsFetchs' + this.marketIso : 'lastDepositsFetchs'
        if(this.$db[field] > Date.now() - (30 * 1000))
            return;

        //Load eth, btc, bch, ltc & broadcast tx ids
        this.$db[field] = Date.now()
        this.$api.post('wallet/balances').then((res) => {
            this.fetchDeposits(res)
        }).catch((err) => {
            console.error(err)
        })

    },
    methods: {
        fetchDeposits(balances) {
            console.log('Fetching all new deposits', this.marketIso);
            balances = balances.filter((o) => ((['ETH', 'BTC', 'BCH', 'LTC'].indexOf(o.iso) !== -1 && !this.marketIso) || this.marketIso === o.iso) && o.address)
            for(var balance of balances) {
                this.fetchAdrdess(balance.iso, balance.address)
            }
        },
        fetchAdrdess(iso, address) {
            var uri = Servers.insight[iso][0].api + 'addr/' + address;
            this.$api.get(uri).then((o) => {
                if(o.transactions.length > 0)
                    return this.$api.post('wallet/add-deposits', { iso, txs: o.transactions })
            }).then((o) => {
                if(o.result) {
                    this.$api.refresh('wallet/balances')
                    this.$api.refresh('wallet/transactions', { iso })
                }
            }).catch(console.error)
        }
    },
}