const d3 = Object.assign({},
    require("d3-selection"),
    require("d3-scale"),
    require("d3-shape"),
    require("d3-transition")
);

export default function (
    options //selector, data, middleSetup, middleShow, middleHide
) {
    function draw() {
        var data = options.data.filter(function (i) {
            return i.val > 0;
        });

        data = data.sort(function (a, b) {
            return a.val - b.val;
        });

        var others = 0;
        var othersNames = [];
        if (data.length > 6)
            while (data.length > 5) {
                var obj = data[0];
                data.splice(0, 1);
                others += obj.val;
                if ($.inArray(obj.name, othersNames) === -1) othersNames.push(obj.name);
            }

        if (others > 0 && data.length == 5) {
            data.push({
                name: "Others",
                val: others
            });
        }

        var totalCount = 0;
        for (var key in data) {
            totalCount += data[key].val;
        }

        for (var key in data) {
            data[key].val = Math.round(data[key].val * 10000 / totalCount) / 100;
        }

        var _data = [];

        //------------------------
        for (var i = 0; i < Math.floor(data.length / 2); ++i) {
            _data.push(data[data.length - i - 1]);
            _data.push(data[i]);
        }

        if (data.length % 2 != 0) {
            _data.push(data[Math.floor(data.length / 2)]);
        }

        data = _data;

        //------------------------

        var svg = d3.select(options.selector).data([data]);
        var jobject = window.document.querySelector(options.selector);
        if (Array.isArray(jobject) && jobject.length) jobject = jobject[0];
        if (jobject) jobject.innerHTML = '';

        var colors = [
      "#006AFF",
      "#4c96ff",
      "#1978ff",
      "#004ab2",
      "#00357f",
      "#001f4c"
    ];
        var w = jobject.width(),
            h = jobject.height(),
            r = Math.min(w, h) / 1.2,
            labelr = r + 30, // radius for label anchor
            color = d3.scaleOrdinal(d3.schemeCategory20),
            donut = d3.pie();

        var outerRadius = r * 0.8;
        var arc = d3
            .arc()
            .innerRadius(r * 0.55)
            .outerRadius(outerRadius);
        var centerObject = null;

        if (options.middleSetup) {
            centerObject = options.middleSetup(svg, w / 2, h / 2, outerRadius); //.attr("transform", "scale(0.4)");// "translate(" + (w / 2)+ "," + (h/2) + ")");
        }

        //Pie chart
        var arcs = svg
            .selectAll("g.arc")
            .data(
                donut
                .value(function (d) {
                    return d.val;
                })
                .sort(null)
            )
            .enter()
            .append("svg:g")
            .attr("class", "arc")
            .attr("transform", "translate(" + w / 2 + "," + h / 2 + ")")
            .on("click", function (d) {
                if (typeof options.clickCallback === "function") {
                    options.clickCallback({
                        selected: d.data.name,
                        others: othersNames
                    });
                }
            });

        //Animation effect
        var arcOver = d3
            .arc()
            .innerRadius(r * 0.55)
            .outerRadius(r * 0.9);
        var transitionTime = 200;

        arcs
            .append("svg:path")
            .attr("fill", function (d, i) {
                return colors[i];
            })
            .attr("d", arc)
            .on("mouseover", function (d) {
                d3
                    .select(this)
                    .transition()
                    .duration(transitionTime)
                    .attr("d", arcOver);

                if (options.middleHover) {
                    options.middleHover(centerObject, d);
                }
            })
            .on("mouseout", function (d) {
                d3
                    .select(this)
                    .transition()
                    .duration(transitionTime)
                    .attr("d", arc);

                if (options.middleOut) {
                    options.middleOut(centerObject, d);
                }
            });

        if (options.middleOut)
            options.middleOut(centerObject, {
                value: 0,
                data: {
                    value: 0
                }
            });

        // Converts from radians to degrees.
        Math.degrees = function (radians) {
            return radians * 180 / Math.PI;
        };

        Math.radians = function (degrees) {
            return degrees * Math.PI / 180;
        };

        //--------------------------------------------

        function angleTransform(position, isLeft, isTop) {
            var offset = 15;
            return {
                x: isLeft ? position.x - offset : position.x + offset,
                y: isTop ? position.y - offset : position.y + offset
            };
        }

        function lineTransform(endAngle, isLeft) {
            var width = 80;
            return {
                x: isLeft ? endAngle.x - width : endAngle.x + width,
                y: endAngle.y
            };
        }

        function findStartPosition(midleAlpha) {
            midleAlpha = midleAlpha - Math.radians(90);
            var O = Math.sin(midleAlpha) * outerRadius;
            var A = Math.cos(midleAlpha) * outerRadius;
            return {
                x: A,
                y: O
            };
        }
        var change = 30;
        var positions = [];

        //Paths
        arcs
            .append("path")
            .attr("class", "pointer")
            .style("fill", "none")
            .style("stroke", function (d, i) {
                return colors[i];
            })
            .attr("d", function (d, index) {
                var alpha = (d.endAngle - d.startAngle) / 2 + d.startAngle;
                var isLeft = alpha > 3.14159;

                var startPosition = findStartPosition(alpha);
                var endAngle = angleTransform(
                    startPosition,
                    isLeft,
                    alpha < 1.5708 || alpha > 4.71239
                );
                var endLine = lineTransform(endAngle, isLeft);
                var endline_X = isLeft ? endLine.x + change : endLine.x - change;
                positions[index] = endLine;
                return (
                    "M " +
                    startPosition.x +
                    " " +
                    startPosition.y +
                    " L " +
                    endAngle.x +
                    " " +
                    endAngle.y +
                    " L " +
                    endline_X +
                    " " +
                    endLine.y
                );
            });

        //Percentage textes
        arcs
            .append("text")
            .attr("text-anchor", function (d, index) {
                var alpha = (d.endAngle - d.startAngle) / 2 + d.startAngle;
                var isLeft = alpha > 3.14159;
                return isLeft ? "start" : "end";
            })
            .attr("font-weight", "bold")
            .attr("fill", function (d, i) {
                return colors[i];
            })
            .attr("x", function (d, index) {
                var alpha = (d.endAngle - d.startAngle) / 2 + d.startAngle;
                var isLeft = alpha > 3.14159;
                return isLeft ?
                    positions[index].x + change :
                    positions[index].x - change;
            })
            .attr("y", function (d, index) {
                return positions[index].y - 10;
            })
            .text(function (d) {
                return Math.round(d.value) + "%";
            });

        //Category textes
        arcs
            .append("text")
            .attr("text-anchor", function (d, index) {
                var alpha = (d.endAngle - d.startAngle) / 2 + d.startAngle;
                var isLeft = alpha > 3.14159;
                return isLeft ? "start" : "end";
            })
            .attr("fill", "#787f85")
            .attr("x", function (d, index) {
                var alpha = (d.endAngle - d.startAngle) / 2 + d.startAngle;
                var isLeft = alpha > 3.14159;
                return isLeft ?
                    positions[index].x + change :
                    positions[index].x - change;
            })
            .attr("y", function (d, index) {
                return positions[index].y + 15;
            })
            .text(function (d) {
                return d.data.name;
            });
    }

    window.addEventListener("resize", draw);
    draw();

    return {
        draw: draw
    }
}
