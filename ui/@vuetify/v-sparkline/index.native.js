import React from 'react';
import { View } from 'react-native';
import { Svg } from 'expo';
import { genPoints } from './helpers/_core'
import { genPath } from './helpers/_path'

var id = 0;

class vSparkLine extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            lastLength: 0
        }
    }

    //Properties
    get autoDrawDuration() {
        return this.props.autoDrawDuration || 2000
    }
    get value() {
        return this.props.value || [];
    }
    get color() {
        return '#000';
    }
    get gradientDirection() {
        return this.props.gradientDirection || 'top';
    }
    get height() {
        return this.props.height || 75;
    }
    get width() {
        return this.props.width || 300;
    }
    get labelSize() {
        return this.props.labelSize || 7
    }
    get lineWidth() {
        return this.props.lineWidth || 4;
    }
    get padding() {
        return this.props.padding || 8;
    }
    get type() {
        return this.props.type || 'trend';
    }
    get labels() {
        return this.props.labels || [];
    }

    //Computed fields
    get parsedPadding () {
      return Number(this.padding)
    }
    get parsedWidth() {
      return Number(this.width)
    }
    get totalBars () {
      return Array.isArray(this.props.value) ? this.props.value.length : 0
    }
    get _lineWidth() {
      if (this.props.autoLineWidth && this.type !== 'trend') {
        const totalPadding = this.parsedPadding * (this.totalBars + 1)
        return (this.parsedWidth - totalPadding) / this.totalBars
      } else {
        return Number(this.lineWidth) || 4
      }
    }
    get boundary () {
      const height = Number(this.height)

      return {
        minX: this.parsedPadding,
        minY: this.parsedPadding,
        maxX: this.parsedWidth - this.parsedPadding,
        maxY: height - this.parsedPadding
      }
    }
    get hasLabels () {
      return Boolean(
        this.props.showLabels ||
        this.labels.length > 0 //||
        //this.$scopedSlots.label //ToDo
      )
    }
    get parsedLabels() {
      const labels = []
      const points = this.points
      const len = points.length

      for (let i = 0; labels.length < len; i++) {
        const item = points[i]
        let value = this.labels[i]

        if (!value) {
          value = item === Object(item)
            ? item.value
            : item
        }

        labels.push({
          ...item,
          value: String(value)
        })
      }

      return labels
    }
    get points() {
      return genPoints(this.value.slice(), this.boundary, this.type)
    }
    get textY() {
      return this.boundary.maxY + 6
    }
    get smooth() {
        return this.props.smooth || false;
    }
    get _uid() {
        return "xz"
    }
    //End of computed properties
    //------

    genGradient() {
      var gradientDirection = this.gradientDirection
      const gradient = this.props.gradient.slice()

      // Pushes empty string to force
      // a fallback to currentColor
      if (!gradient.length) gradient.push('')

      const len = Math.max(gradient.length - 1, 1)
      const stops = gradient.reverse().map((color, index) => (<Svg.Stop offset={index/len} stopColor={ color || this.color || 'currentColor'} key={'sp' + index} />))

      return (<Svg.Defs>
            <Svg.LinearGradient id="linear-gradient"
                x1={(gradientDirection === 'right') ? '0' : '100%'}
                y1={(gradientDirection === 'bottom') ? '0' : '100%'}
                x2={(gradientDirection === 'left') ? '0' : '100%'}
                y2={(gradientDirection === 'top') ? '0' : '100%'}>
                    {stops}
              </Svg.LinearGradient>
          </Svg.Defs>)
    }
    genG(children) {
        return (<Svg.G style={{
                      fontSize: '8',
                      textAnchor: 'middle',
                      dominantBaseline: 'mathematical',
                      fill: this.color || 'currentColor' }}>{children}</Svg.G>)
    }
    genLabels() {
      if (!this.hasLabels)
          return null

      return this.genG(this.parsedLabels.map(this.genText.bind(this)))
    }
    genPath() {
      const radius = this.smooth === true ? 8 : (Number(this.smooth) || 0)

      return (<Svg.Path id={this._uid}
                  d={genPath(this.points.slice(), radius, this.props.fill, Number(this.height))}
                  fill={this.props.fill ? `url(#linear-gradient)` : 'none'}
                  stroke={this.props.fill ? 'none' : `url(#linear-gradient)`}
                  ref={(p) => this.path = p} />);
    }
    genText(item, index) {
      /*const children = this.$scopedSlots.label
        ? this.$scopedSlots.label({ index, value: item.value })
        : item.value

      return this.$createElement('text', {
        attrs: {
          x: item.x,
          y: this.textY
        }
      }, [children])*/
        return null; //ToDo
    }
    genClipPath (offsetX, lineWidth, id) {
      const { maxY } = this.boundary
      const rounding = typeof this.smooth === 'number'
        ? this.smooth
        : this.smooth ? 2 : 0

      return (<Svg.ClipPath id={`${id}-clip`}>
          {
            this.points.map(item => (
                <Svg.Rect x={item.x + offsetX} y={0} width={lineWidth} height={Math.max(maxY - item.y, 0)} rx={rounding} ry={rounding}>
                  { this.autoDraw ? (<Svg.Animate attributeName='height' from={0} to={maxY - item.y} dur={`${this.autoDrawDuration || 2000}ms`} fill="freeze" />) : null }
                </Svg.Rect>
            ))
          }
        </Svg.ClipPath>)
    }
    genBarLabels(props) {
      /*const offsetX = props.offsetX || 0

      const children = props.points.map(item => (
        this.$createElement('text', {
          attrs: {
            x: item.x + offsetX + this._lineWidth / 2,
            y: props.boundary.maxY + (Number(this.labelSize) || 7),
            'font-size': Number(this.labelSize) || 7
          }
        }, item.value.toString())
      ))

      return this.genG(children)*/
    }

    genTrend() {
        return (<Svg strokeWidth={this._lineWidth || 1} width="100%" height={this.height} viewBox={`0 0 ${this.width} ${this.height}`} style={this.props.style}>
            { this.genGradient() }
            { this.genLabels() }
            { this.genPath() }
        </Svg>)
    }

    genBars() {
        if (!this.value || this.totalBars < 2)
            return null;

          const { width, height, parsedPadding, _lineWidth } = this
          const viewWidth = width || this.totalBars * parsedPadding * 2
          const viewHeight = height || 75
          const boundary = {
            minX: parsedPadding,
            minY: parsedPadding,
            maxX: Number(viewWidth) - parsedPadding,
            maxY: Number(viewHeight) - parsedPadding
          }
          const props = {
            ...this.props
          }

          props.points = genPoints(this.value, boundary, this.type)
          const totalWidth = boundary.maxX / (props.points.length - 1)

          props.boundary = boundary
          lineWidth = _lineWidth || (totalWidth - Number(parsedPadding || 5))
          props.offsetX = 0
          if (!this.autoLineWidth) {
            props.offsetX = ((boundary.maxX / this.totalBars) / 2) - boundary.minX
          }

          return (
            <Svg width="100%" height="25%" viewBox={`0 0 ${viewWidth} ${viewHeight}`}>
                {this.genGradient()}
                { this.genClipPath(props.offsetX, lineWidth, 'sparkline-bar-' + this._uid) }
                { this.hasLabels ? this.genBarLabels(props) : null }
                <Svg.G transform={`scale(1,-1) translate(0,-${boundary.maxY})`} clipPath={`url(#sparkline-bar-${this._uid}-clip)`} fill="url(#linear-gradient)">
                  <Svg.Rect x={0} y={0} width={viewWidth} height={viewHeight} />
                </Svg.G>
            </Svg>
          )
    }

    render() {
        let { value } = this.props;

        if(this.totalBars < 2)
            return null;

        return (this.type === 'trend') ? this.genTrend() : this.genBars();
    }
}

module.exports = vSparkLine;