import React from "react";
import { View, TouchableWithoutFeedback } from "react-native";

class Div extends React.Component {

  render() {
    if (!!this.props.onClick) {
      return (
        <TouchableWithoutFeedback onPress={this.props.onClick}>
          <View {...this.props}  />
        </TouchableWithoutFeedback>
      );
    }
    return (
      <View {...this.props}  />
    );
  }
}

module.exports = Div;
