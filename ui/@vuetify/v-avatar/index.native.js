import React from 'react';
import { View, StyleSheet } from 'react-native';
import Colors from '@config/colors';

import VImg from '@vuetify/v-img';

const VAvatar = props => {
  let { children, style, size, color, tile, ...rest } = props;

  if(typeof(size) === "string")
      size = parseFloat(size.replace('px', ''));

  return (
    <View style={[
            styles.wrapper,
            { width: size, height: size, borderRadius: tile ? 0 : (size / 2) },
            color && { backgroundColor: Colors[color] || color },
            style
          ]} {...rest}>
        <VImg.SizeProvider value={ size }>
            { children }
        </VImg.SizeProvider>
    </View>
  );
};

const styles = StyleSheet.create({
    wrapper: {
        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden',
        margin: 0,
    },
})

VAvatar.Styles = styles;

module.exports = VAvatar;