import React from 'react';
import { View } from 'react-native';
import { withNavigation } from 'react-navigation';

export default withNavigation(class VNavigationDrawer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            key: 'nav'
        }
    }

    getChildren(props) {
        if(this.props.children.render)
            return this.props.children.render(props);

        return this.props.children.map((o) => {
            if(typeof(o.render) === 'function')
                return o.render(props);
        })
    }

    render() {
        const { Navigator, navigation, children, shadowKey } = this.props;
        //ToDo auto generate routes & create Router from props

        /*if(!this.nav) {
            this.nav = createDrawerNavigator(routes, {
                initialRouteName: initialRouteName,
                contentComponent: (props) => (children)
            })
        }

        const Navigator = this.nav*/
        Navigator.getDrawer = (props) => (
          <View key={shadowKey} style={{ flex: 1 }}>
            {this.getChildren(props)}
          </View>
        );

        Navigator.setKey = (key) => {
            this.setState({ key })
        }

        return (<Navigator navigation={navigation} key={this.state.key} />);
    }
})