import React from 'react';
import { View, StyleSheet } from 'react-native';
import VText from '@vuetify/v-text';

const VSubHeader = props => {
  let { children, ...rest } = props;

  return (
    <View style={styles.container}>
        <VText style={[ styles.text, { color: 'rgba(0, 0, 0, 0.54)' } ]} {...rest} medium>
          { children }
        </VText>
    </View>
  );
};

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        height: 48,
    },
    text: {
        fontSize: 14,
        paddingLeft: 16,
        paddingRight: 16,
    }
})

module.exports = VSubHeader;