import React from 'react';
import { StyleSheet } from 'react-native';
import { Image } from 'react-native';
import { align } from "@vuetify/helpers";

const SizeProvider = React.createContext(null);

export default class VImg extends React.Component {
  render() {
      let {
        src, round, rounded, style, children,
        width, height, minHeight, minWidth,
        contain, cover, stretch, center, repeat
      } = this.props;

      width = parseInt(width || SizeProvider.Consumer._currentValue || 100, 10);
      height = parseInt(height || SizeProvider.Consumer._currentValue || 100, 10);
      minWidth = parseInt(minWidth || 1, 10);
      minHeight = parseInt(minHeight || 1, 10);

      let imageResizeMode = undefined;
      if (contain) imageResizeMode = 'contain';
      else if (cover) imageResizeMode = 'cover';
      else if (stretch) imageResizeMode = 'stretch';
      else if (center) imageResizeMode = 'center';
      else if (repeat) imageResizeMode = 'repeat';

      style = StyleSheet.flatten(style, align(this.props));

      return (
        <Image
          ImageResizeMode={imageResizeMode}
          source={{ uri: src }}
          style={[
            (round || rounded) && { borderRadius: Math.round(parseInt(width, 10) / 2) },
            { width, height, minHeight, minWidth },
            style,
          ]} />
      )
  }
}

VImg.SizeProvider = SizeProvider.Provider;