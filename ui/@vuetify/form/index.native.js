import React from "react";
import { View } from "react-native";
import $emit from '@vuetify/helpers/emit';
import $api from '@addons/apiManager';

const Context = React.createContext(null);

class Form extends React.Component {

    constructor(props) {
        super(props);
        this.data = {};
    }

    attachValue(key, val) {
        this.data[key] = val;
    }

    submit() {
        const { action, method } = this.props;
        if(!action)
            return;

        $emit(this.props, 'apiPrepare', action);
        var res = (action === 'get') ? $api.get(action, this.data) : $api.post(action, this.data);

        res.then((r) => $emit(this.props, 'apiSuccess', r))
           .catch((err) => $emit(this.props, 'apiError', err))
           .then((r) => $emit(this.props, 'apiDone', r));
    }

    render() {
        return <Context.Provider {...this.props} value={this} />;
    }
}

export default Form
export const Consumer = Context.Consumer;
