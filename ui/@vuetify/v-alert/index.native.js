import PropTypes from 'prop-types';
import React from 'react';
import {
  TouchableNativeFeedback,
  TouchableHighlight,
  StyleSheet,
  View,
  Platform,
  Text as NativeText,
} from 'react-native';

import Text from '@vuetify/v-text';
import Icon from '@vuetify/v-icon';
import $colors from '@config/colors';

import Color from 'color';
function parseColor(color) {
    const res = {
        background: null,
        text: null,
        border: null,
    }

    if(!color)
        return res;

    const colors = color.split(' ').filter((o) => o != '');
    for(var c of colors) {
        if(c.length > 6 && c.substr(c.length - 6) === '--text') {
            res.text = $colors[c.substr(0, c.length - 6)] || c.substr(0, c.length - 6);
        } else {
            res.background = $colors[c] || c;
            if(c === 'primary' || c === 'error')
                res.text = '#fff';
        }
    }

    if (res.background)
      res.border = Color(res.background).darken(0.2);

    return res;
}

const VAlert = props =>
{
  let {
    value,
    color,
    icon,
    style,
    children,
  } = props;
  
  if (icon !== null) {
    icon = icon || 'alert';
  }

  if (!value)
    return null; // Vuetify toggle attribute.

  color = parseColor(color);
  return (
    <View style={[
      styles.alert,
      style,
      color.border && { borderColor: color.border },
      color.background && { backgroundColor: color.background },
    ]}>
      { !!icon && (<Icon color={ color.text || styles.icon.color } style={styles.icon}>{`icon-${icon}`}</Icon>) }
      { typeof children === 'string' 
        ? (
          <Text style={[styles.alertText, color.text && { color: color.text }]}>
            {children}
          </Text>
        )
        : (children)
      }
    </View>
  );
};

const stylesObject = {
  alert: {
    borderTopWidth: 3,
    paddingTop: 15,
    paddingBottom: 12,
    paddingHorizontal: 10,
    flexDirection: 'row',
    width: '100%',
    backgroundColor: 'red',
  },
  alertText: {
    paddingTop: 2,
    fontWeight: 'normal',
    color: '#fff'
  },
  icon: {
    color: '#fff',
    fontSize: 18,
    marginRight: 10,
  },
};

const styles = StyleSheet.create(stylesObject);

export default VAlert;