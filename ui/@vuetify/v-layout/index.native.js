import React from 'react';
import { View } from 'react-native';
import { align } from "@vuetify/helpers";

export default class VLayout extends React.Component {
    render() {
        const { style, children } = this.props;
        const alignments = align(this.props);

        return (
          <View style={[alignments, {
            flexWrap: 'wrap',
            flexBasis: 12,
            flexGrow: 1,
            flexShrink: 1,
          }, style]}>{children}</View>
        );
    }
}
