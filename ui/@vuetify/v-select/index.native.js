import React from 'react';
import { Picker } from 'react-native';
import $emit from '@vuetify/helpers/emit';

export default class VSelect extends React.Component {
  emit(selected) {
      $emit.model(this.props, 'change', selected);
  }

  render() {
    const selected = this.props.value || (this.props.items.length ? this.props.items[0].value : null);
    return (
      <Picker
        label={this.props.label || undefined}
        selectedValue={selected}
        onValueChange={(selected) => this.emit(selected)}
      >
        {(this.props.items || []).map((item) => (
          <Picker.Item
            key={String(item.id || item.key || JSON.stringify(item))}
            label={item.label || item.name}
            value={item.value || item.id}
          />
        ))}
      </Picker>
    );
  }
}
