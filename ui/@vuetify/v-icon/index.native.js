import PropTypes from 'prop-types';
import React from 'react';
import { Text as NativeText, StyleSheet } from 'react-native';
import { fontSize } from '@vuetify/helpers/responsive';
import colors from '@config/colors';
import bit4you from '@config/icons';

import { Color } from '../helpers/providers';

const SizeProvider = React.createContext(null);
    
const VIcon = props => {
  var {
    children,
    icon,
    style,
    onPress,
    color,
    size,
      
    large,
    medium,
    small,
    xLarge,

    ...attributes
  } = props;

  var myIcon       = icon ? icon : {};
  var iconName     = String(myIcon.name || children);
  if (iconName.substr(0, 5) === 'icon-') {
    iconName = iconName.substr(5);
  }
  var fontFamily   = myIcon.family || myIcon.fontFamily;

  if(!color && Color.Consumer._currentValue)
      color = Color.Consumer._currentValue

  if(color && colors[color])
      color = colors[color];

  if(!fontFamily) {
      if(bit4you[iconName]) {
          fontFamily = 'b4yIcons';
          iconName = bit4you[iconName];
      }
  }

  style = StyleSheet.flatten(style || {})
  size = (size && Number(size)) || (xLarge && 40) || (large && 36) || (medium && 28) || (small && 16) || SizeProvider.Consumer._currentValue || style.fontSize || 24;
  return (
    <NativeText onPress={onPress} style={[
      {
        backgroundColor: 'transparent',
        fontFamily: fontFamily
      },
      style,
      (size && !Number.isNaN(size) && { fontSize: fontSize(size) }),
      (color && { color })
    ]} {...attributes}>{ iconName }</NativeText>
  );
};

VIcon.propTypes = {
  icon: PropTypes.object,
  style: PropTypes.any,
  onPress: PropTypes.any
};

VIcon.SizeProvider = SizeProvider.Provider;
export default VIcon;
