import React from "react";
import { View, Dimensions } from "react-native";
import { align, getBreakpoint } from "@vuetify/helpers";

export default class VFlex extends React.Component {
  getSizePercentage(prefix = "", getIndex = false) {
    let type = getBreakpoint();
    if (prefix !== "") type = type.substr(0, 1).toUpperCase() + type.substr(1);
    for (var i = 0; i <= 12; i++) {
      if (this.props[prefix + type + i]) {
        if (getIndex) return i;
        return parseFloat(100 / (12 / parseInt(i, 10)));
      }
    }
    return false;
  }

  render() {
    const { children, grow, shrink, style } = this.props;

    let size = this.getSizePercentage();
    let offset = this.getSizePercentage("offset");
    let order = this.getSizePercentage("order", true);

    const alignments = align(this.props);

    return (
      <View
        style={[
          {
            flexBasis: !size ? "100%" : size.toFixed(2) + "%",
            flexGrow: grow ? 1 : 0,
            flexShrink: shrink ? 1 : 0,
            flexDirection: 'row',
          },
          offset && { marginLeft: offset.toFixed(2) + "%" },
          order && { order: order },
          alignments,
          style,
        ]}
      >
        {children}
      </View>
    );
  }
}
