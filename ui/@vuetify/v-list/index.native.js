import React from 'react';
import { View, StyleSheet } from 'react-native';
import VIcon from '@vuetify/v-icon';

export default class VList extends React.Component {
    render() {
        const { style, dense, children } = this.props;
        var subStyle = {}
        if(dense) {
            subStyle.paddingTop = 4;
            subStyle.paddingBottom = 4;
        }

        return (
          <View style={[styles.container, subStyle, style]}>
                { dense ? (<VIcon.SizeProvider value={18}>{children}</VIcon.SizeProvider>) : children }
          </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        paddingTop: 8,
        paddingBottom: 8,
    }
})