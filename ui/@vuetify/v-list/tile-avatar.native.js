import React from 'react';
import { View, StyleSheet } from 'react-native';

const VListTileAvatar = props => {
  let { children, style, ...rest } = props;

  return (
    <View style={[styles.container, style]} {...rest}>
      { children }
    </View>
  );
};

module.exports = VListTileAvatar;

const styles = StyleSheet.create({
    container: {
        minWidth: 56,
        justifyContent: 'flex-start',
    }
})