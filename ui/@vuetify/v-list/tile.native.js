import React from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import $emit from '@vuetify/helpers/emit';
import { withNavigation } from 'react-navigation';
import { nav } from '../router-link';
import { createEvent } from '@addons/apiManager/event';

const VListTile = props => {
  let { children, style, to, navigation, ...rest } = props;

  function onPress() {
    const $e = createEvent('click');
    $emit(props, 'click', $e)

    if(!$e.defaultPrevented && to)
        nav(navigation, to)
  }

  return (
    <TouchableOpacity style={[ styles.container, style ]} {...rest} onPress={onPress}>
      { children }
    </TouchableOpacity>
  );
};

module.exports = VListTile;

const styles = StyleSheet.create({
    container: {
        height: 48,
        paddingLeft: 16,
        paddingRight: 16,
        flexDirection: 'row',
        alignItems: 'center',
    }
})