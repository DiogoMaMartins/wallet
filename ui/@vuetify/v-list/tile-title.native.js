import React from 'react';
import { StyleSheet } from 'react-native';
import VText from '@vuetify/v-text';
import { createEvent } from '@addons/apiManager/event';
import $emit from '@vuetify/helpers/emit';
const VListTileTitle = props => {
   
    let { children, style, to, navigation, ...rest } = props;

    function onPress(e) {
      const $e = createEvent('click');
    $emit(props, 'click', $e)

    if(!$e.defaultPrevented && to)
        nav(navigation, to)
  }

    return (
        <VText style={[styles.text, style]} {...rest} onPress={onPress}>
     { children } 
    </VText>
    );
};

module.exports = VListTileTitle;

const styles = StyleSheet.create({
    text: {
        height: 24,
        lineHeight: 24,
        fontSize: 16,
    }
})