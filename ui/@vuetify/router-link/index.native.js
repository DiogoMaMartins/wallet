import React from "react";
import { TouchableOpacity } from "react-native";
import $emit from '@vuetify/helpers/emit';
import { withNavigation } from 'react-navigation';
import { createEvent } from '@addons/apiManager/event';

function cammel(str, del) {
    return str.split(del).map((o) => o.substr(0,1).toUpperCase() + o.substr(1)).join('')
}

function nav(navigation, link) {
    const routeName = cammel(cammel(String(link), '/'), '-')
    navigation.navigate(routeName)
}

class RouterLink extends React.Component {

    onPress() {
        const $e = createEvent('click');
        $emit(this.props, 'click', $e)

        if(!$e.defaultPrevented)
            nav(this.props.navigation, this.props.to);
    }

    render() {
        const { children } = this.props;
        return (
          <TouchableOpacity onPress={this.onPress.bind(this)} {...this.props}>
            {children}
          </TouchableOpacity>
        );
    }
}

module.exports = withNavigation(RouterLink);
module.exports.nav = nav;