import React from 'react';
import { ActivityIndicator } from 'react-native';
// import { align } from "@vuetify/helpers";
import VText from "@vuetify/v-text";
import Colors from "@config/colors";
import Color from 'color';

export default class VProgressBar extends React.Component {
    render() {
        let { indeterminate, color, rotate, size, value, width } = this.props;
        size = (!size || size < 50) ? 'small' : 'large';

        return (
            <ActivityIndicator color={color ? (Colors[color] || color) : '#000'} size={size} animating={indeterminate ? true : false} />
        )
        
        /*value = Math.round(parseInt(value, 10));

        const colorDark = Colors[color] || Colors.primary;
        const colorLight = new Color(colorDark).alpha(0.2).rgb();

        return (
          <View style={[styles.bar, { backgroundColor: colorLight, width: '100%', maxWidth: '100%', flex: 1 }, style]}>
            <View style={[styles.bar, { backgroundColor: colorDark, width: `${value}%`, flex: 1 }]} />
          </View>
        );*/
    }
}