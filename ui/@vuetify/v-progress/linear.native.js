import React from 'react';
import { View, StyleSheet } from 'react-native';
// import { align } from "@vuetify/helpers";
import Colors from "@config/colors";
import Color from 'color';

export default class VProgressBar extends React.Component {
    render() {
        let { value, color, style } = this.props;
        value = Math.round(parseInt(value, 10));

        const colorDark = Colors[color] || Colors.primary;
        const colorLight = new Color(colorDark).alpha(0.2).rgb();
        style = StyleSheet.flatten(style || {});

        return (
          <View style={[styles.bar, { backgroundColor: colorLight, width: '100%', maxWidth: '100%' }, style]}>
            <View style={[styles.bar, { backgroundColor: colorDark, width: `${value}%`, flex: 1 }, style.height && { height: style.height } ]} />
          </View>
        );
    }
}

const styles = {
  bar: {
    overflow: 'hidden',
    borderRadius: 10,
    height: 20,
  },
};
