import React from 'react';
import { Switch } from 'react-native';
import $emit from '@vuetify/helpers/emit';
import colors from '@config/colors';

export default class VSwitch extends React.Component {
  emit(checked) {
      $emit.model(this.props, 'change', checked);
  }

  render() {
    const color = colors.primary;
    return (
      <Switch
        value={!!this.props.value}
        onValueChange={((checked) => this.emit(checked))}
        disabled={this.props.disabled || false}
        trackColor={color}
      />
    );
  }
}
