import React from "react";
import { TextInput } from "react-native";
import { Consumer } from '../form';

class Input extends React.Component {
    render() {
        const { name, value, type } = this.props;
        if(name && Consumer._currentValue)
            Consumer._currentValue.attachValue(name, value);

        if(type === 'hidden')
            return null

        return (<TextInput />);
    }
}

module.exports = Input;
