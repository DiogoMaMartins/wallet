import React from 'react';
import { View, StyleSheet } from 'react-native';

const VListAction = props => {
  let { style, ...rest } = props;

  return (
    <View style={[styles.container, style]} {...rest} />
  );
};

module.exports = VListAction;

const styles = StyleSheet.create({
    container: {
        height: 1,
        backgroundColor: 'rgba(0,0,0,0.12)'
    }
})