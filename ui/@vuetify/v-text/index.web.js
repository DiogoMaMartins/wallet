import Colors from '@config/colors';
import Translator from '@addons/translator';

export default {
    functional: true,
    props: {
        h1: Boolean,
        h2: Boolean,
        h3: Boolean,
        h4: Boolean,
        center: Boolean,
        bold: Boolean,
        fontFamily: Number,
        i18n: [Object, Boolean],
        medium: Boolean,
        color: String
    },
    render(h, context) {
        const props = context.props;
        
        context.data.staticStyle = context.data.staticStyle || {};

        const fontSize = (props.h1 && '25px') || (props.h2 && '22px') || (props.h3 && '18px') || (props.h4 && '15px');
        if(fontSize && !context.data.staticStyle['font-size'])
            context.data.staticStyle['font-size'] = fontSize;

        const fontWeight = ((props.bold || props.h1 || props.h2 || props.h3 || props.h4) && 'bold') || (props.medium && 'medium');
        if(fontWeight && !context.data.staticStyle['font-weight'])
            context.data.staticStyle['font-weight'] = fontWeight;

        if(props.color && !context.data.staticStyle['color'])
            context.data.staticStyle['color'] = Colors[props.color] || props.color;

        if(props.fontFamily && !context.data.staticStyle['font-family'])
            context.data.staticStyle['font-family'] = props.fontFamily;

        if(props.center && !context.data.staticStyle['text-align'])
            context.data.staticStyle['text-align'] = 'center';

        return h(
            'div',
            {
                ...context.data,
            },
            props.i18n ? context.children.map((o) => {
                if(o.text)
                    return Translator.$t(o.text, props.i18n);

                return o;
            }) : context.children
        )
    },
}