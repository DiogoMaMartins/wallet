import PropTypes from 'prop-types';
import React from 'react';
import { Text, StyleSheet, Platform } from 'react-native';
import fonts from '@config/fonts';
import colors from '@config/colors';
import Translator from '@addons/translator';
import { fontSize } from '../helpers/responsive';

const styles = StyleSheet.create({
  text: {
      backgroundColor: 'transparent',
    ...Platform.select({
      android: {
        ...fonts.android.regular,
      }
    }),
  },
  bold: {
    ...Platform.select({
      android: {
        ...fonts.android.bold,
      },
      ios: {
        ...fonts.ios.bold,
      }
    }),
  },
});

const TextElement = props => {
  let { children, h1, h2, h3, h4, center, bold, fontFamily, i18n, medium, style, color, ...rest } = props;

  style = StyleSheet.flatten(style);
  if (style && style.fontSize && (h1 || h2 || h3 || h4)) {
    style = Object.assign({}, style);
    delete style.fontSize
  }

  if(!color && style && style.color)
      color = style.color;

  if(color && colors[color])
      color = colors[color];

  return (
    <Text
      style={[
        styles.text,
        //fontSize && fontSize,
        h1 && { fontSize: fontSize(25) },
        h2 && { fontSize: fontSize(22) },
        h3 && { fontSize: fontSize(18) },
        h4 && { fontSize: fontSize(15) },
        center && { textAlign: 'center' },
        medium && { fontFamily: 'Exo2-Medium' },
        (h1 || h2 || h3 || h4 || bold) && styles.bold,
        fontFamily && { fontFamily },
        style,
        (style
          && style.fontSize
          && { fontSize: fontSize(parseFloat(String(style.fontSize).replace('px', ''))) }
        ),
        color && { color: color },
      ]}
      {...rest}
    >
      { i18n ? Translator.$(children, typeof(i18n) === "object" ? i18n : {}) : children }
    </Text>
  );
};

TextElement.propTypes = {
  style: PropTypes.any,
  i18n: PropTypes.any,
  h1: PropTypes.bool,
  h2: PropTypes.bool,
  h3: PropTypes.bool,
  h4: PropTypes.bool,
  fontFamily: PropTypes.string,
  children: PropTypes.any,
};

export default TextElement;