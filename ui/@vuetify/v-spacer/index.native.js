import React from 'react';
import { View } from 'react-native';

const VSpacer = props => {
  let { style, children } = props;

  return (
    <View style={[{ flexGrow: 1 }, style]}>{ children }</View>
  );
};

module.exports = VSpacer;