import React from 'react';
import { Image } from 'react-native';
import { server } from '@config/server';

export default class Img extends React.Component {
  render() {
    const props = Object.assign({}, this.props);
    if (props.cover) {
      props.resizeMode = 'cover';
      delete props.cover;
    }
    if (props.src) {
      props.source = props.src;
      delete props.src;
    }
    if (typeof props.source === 'string') {
      props.source = { uri: String(props.source) };
    }

    if(props.source && props.source.uri && props.source.uri.indexOf('://') === -1) {
        props.source.uri = server + (props.source.uri.substr(0, 1) === '/' ? props.source.uri.substr(1) : props.source.uri);
    }

    return (
      <Image {...props} />
    );
  }
} 

