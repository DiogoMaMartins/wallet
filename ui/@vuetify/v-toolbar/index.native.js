import React from "react";
import { View, StyleSheet, Dimensions, StatusBar, Platform } from "react-native";
import { LinearGradient, Constants } from 'expo';
import Colors from '@config/colors'

class VToolbar extends React.Component {

    get defaultSlot() {
        const { children } = this.props;
        return children.filter((o) => {
            return !o.dataSlot || o.dataSlot === 'default';
        })
    }

    get extensionSlot() {
        const { children } = this.props;
        const slot = children.find((o) => o.dataSlot === "extension")
        if(!slot)
            return null;

        return (<View style={styles.ext}>{ slot.render(this.props.navigation) }</View>);
    }

    render() {
        var { style, light, dark, children, color, dense, height, ...rest } = this.props;
        style = StyleSheet.flatten([ styles.container, style ])

        if(height)
            style.height = parseFloat(String(height).replace('px', ''))

        if(!dense) {
            var statusbarHeight = Constants.statusBarHeight || 0;
            style.paddingTop = (style.paddingTop || 0) + Math.max(0, statusbarHeight - 10);
            //style.height += statusbarHeight - 10;
        }

        if(color)
            style.backgroundColor = Colors[color] || color;

        const extension = this.extensionSlot;
        return (
            <View style={style} { ...rest }>
                <StatusBar barStyle={dark || !light ? 'light-content' : 'dark-content'} />
                <View style={[styles.bar, extension && { paddingBottom: 0 }]}>
                    { this.defaultSlot }
                </View>
                { extension }
            </View>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        backgroundColor: '#095ce5',
        paddingTop: 5,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column'
    },

    bar: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 5,
    },

    ext: {
        width: '100%',
        paddingBottom: 1
    }

});

module.exports = VToolbar;