import React from 'react';
import { View, StyleSheet } from 'react-native';
import VBtn from '@vuetify/v-btn';
import VIcon from '@vuetify/v-icon';
import { DrawerActions, NavigationActions, withNavigation } from 'react-navigation';

const VToolbarSideIcon = props => {
  let { children, style, back, onClick, navigation, ...rest } = props;

  onClick = onClick || function() {
      if(back)
          navigation.dispatch(NavigationActions.back());
      else
        navigation.dispatch(DrawerActions.openDrawer());
  }

  return (
    <VBtn style={[styles.container, style]} {...rest} onClick={onClick} flat round icon dark>
        <VIcon>{ back ? 'icon-angle-left' : 'icon-menu'}</VIcon>
    </VBtn>
  );
};

module.exports = withNavigation(VToolbarSideIcon);

const styles = StyleSheet.create({
    container: {
        marginLeft: 0,
        marginRight: 0,
        marginTop: 0,
        marginBottom: 0,
    }
})