import React from 'react';
import { View, StyleSheet } from 'react-native';
import VText from '@vuetify/v-text';

const VSubHeader = props => {
  let { children, style, ...rest } = props;

  return (
    <VText style={[ styles.text, style ]} {...rest} medium>
      { children }
    </VText>
  );
};

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        height: 48,
    },
    text: {
        fontSize: 14,
        paddingLeft: 16,
        paddingRight: 16,
        color: '#fff'
    }
})

module.exports = VSubHeader;