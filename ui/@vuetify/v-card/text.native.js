import React from 'react';
import { View, StyleSheet } from 'react-native';

const VCardText = props => {
  let { style, children } = props;

  return (
      <View style={[styles.wrapper, style]}>{ children }</View>
  );
};

const styles = StyleSheet.create({
    wrapper: {
        padding: 16,
        flexWrap: 'wrap',
    },
})

module.exports = VCardText;