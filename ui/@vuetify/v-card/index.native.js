import React from 'react';
import { View, Image, StyleSheet } from 'react-native';
import { align } from "@vuetify/helpers";

export default class VCard extends React.Component {

  render() {
      let {children, contain, style} = this.props;
      contain = contain ? {flex: 1} : {width: '100%'};
      const alignments = align(this.props);
      return (
        <View style={[styles.wrapper, alignments, contain, style]}>{children}</View>
      )
  }

}

const styles = StyleSheet.create({
    wrapper: {
        backgroundColor: '#fff',
        borderRadius: 8,

        //marginVertical: 10,

        elevation: 2,
        borderColor: 'rgba(176,190,197,0.05)',
        borderWidth: 1,
        shadowOpacity: 0.75,
        shadowRadius: 5,
        shadowColor: 'rgba(176,190,197,0.90)',
        shadowOffset: { height: 0, width: 0 },
    },
})

VCard.Styles = styles;