import React from 'react';
import { View, StyleSheet } from 'react-native';

const VCardTitle = props => {
  let { style, children } = props;

  return (
      <View style={[styles.wrapper, style]}>{ children }</View>
  );
};

const styles = StyleSheet.create({
    wrapper: {
        padding: 16,
        flexWrap: 'wrap',
        flexDirection: 'row'
    },
})

module.exports = VCardTitle;