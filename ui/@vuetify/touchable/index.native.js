import React from "react";
import { TouchableOpacity } from "react-native";
import $emit from '@vuetify/helpers/emit';

class Touchable extends React.Component {
  render() {
    const { onClick, ...props } = this.props;

    return <TouchableOpacity onPress={(e) => $emit(this.props, 'click', e)} {...props}  />;
  }
}

module.exports = Touchable;
