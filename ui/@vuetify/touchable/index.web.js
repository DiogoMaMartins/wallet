export default {
    functional: true,
    render(h, context) {
        context.data.staticStyle = context.data.staticStyle || {};
        context.data.staticStyle.cursor = 'pointer';

        return h(
            'div',
            {
                ...context.data,
                on: {
                  click: context.listeners.click || (() => {})
                }
            },
            context.children
        )
    },
}