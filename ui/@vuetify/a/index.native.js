import React from 'react';
import { TouchableOpacity } from 'react-native';
import { WebBrowser } from 'expo';

export default class A extends React.Component {
  showArticle() {
    const { href } = this.props;
    WebBrowser.openBrowserAsync(href).catch((err) => {
        console.error(err)
    });
  }

  render() {
    const { children, href } = this.props;
    if (!href) return null;
    return (
      <TouchableOpacity onPress={this.showArticle.bind(this)}>
        { children }
      </TouchableOpacity>
    );
  }
} 

