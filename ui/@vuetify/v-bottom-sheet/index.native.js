import React from 'react';
import { View, StyleSheet } from 'react-native';
import NativeModal from 'react-native-modal';
import { ifIphoneX } from '@vuetify/helpers/responsive';
import $emit from '@vuetify/helpers/emit';

class VBottomSheet extends React.Component {
  state = {
    height: 600,
    layout: false,
  }

  constructor(props) {
    super(props);
  }

  onLayout(e) {
    if (!this.state.layout && e && e.nativeEvent && e.nativeEvent.layout && e.nativeEvent.layout.height) {
      this.setState({
        height: e.nativeEvent.layout.height,
        layout: true,
      });
    }
  }

  onHide() {
      $emit.model(this.props, 'change', false);
  }

  render() {
    let { children, value } = this.props;
    const onHide = this.onHide.bind(this);

    return (
      <NativeModal isVisible={value} style={styles.wrapper} onBackdropPress={onHide} onBackButtonPress={onHide}>
        <View
          onLayout={this.onLayout.bind(this)}
          style={[
            styles.content,
            (this.state.layout && { height: this.state.height }),
            { paddingTop: 20, paddingBottom: ifIphoneX(30, 20) }
          ]}>
            <View>
              { children }
            </View>
        </View>
      </NativeModal>
    );
  }
};

const styles = StyleSheet.create({
    wrapper: {
        alignItems: 'center',
        justifyContent: 'flex-end',
        margin: 0,
      },
      content: {
        zIndex: 2,
        paddingHorizontal: 30,
        paddingTop: 10,
        backgroundColor: '#fff',
        width: '100%',
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
    },
})

VBottomSheet.Styles = styles;

module.exports = VBottomSheet;