import React from 'react';
import { CheckBox } from 'react-native-elements';
import $emit from '@vuetify/helpers/emit';
import colors from '@config/colors';

export default class VCheckbox extends React.Component {
  emit() {
    //$emit(this.props, 'value', !this.props.value);
    //$emit(this.props, 'input', !this.props.value);
    //$emit(this.props, 'change', !this.props.value);
    //$emit(this.props, 'update:value', !this.props.value);

      $emit.model(this.props, 'change', !this.props.value);
  }

  render() {
    return (
      <CheckBox
        containerStyle={[{ backgroundColor: 'transparent', borderWidth: 0, marginLeft: 0, padding: 5 }, this.props.style]}
        textStyle={{ paddingLeft: !!this.props.value ? 1 : 5 }}
        title={this.props.label || this.props.title || undefined}
        checked={!!this.props.value}
        onPress={this.emit.bind(this)}
        disabled={this.props.disabled || false}
      />
    );
  }
}
