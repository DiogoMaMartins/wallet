import Normalize from './normalizeText';
import { Dimensions, Platform } from 'react-native';

export const deviceHeight = Dimensions.get('window').height;
export const deviceWidth = Dimensions.get('window').width;

export const ifIphoneX = (iphoneXStyle, regularStyle) => {
  let dimen = Dimensions.get('window');

  if (Platform.OS === 'ios' &&
      !Platform.isPad &&
      !Platform.isTVOS &&
      (dimen.height >= 812 || dimen.width >= 812)) {
      return iphoneXStyle;
  } else {
      return regularStyle
  }
}

export const ifHeightGreater = (condition, rule, _else = null) => {
  if (deviceHeight > condition) {
    return rule;
  }
  return _else;
};

export const ifHeightLesser = (condition, rule, _else = null) => {
  if (deviceHeight < condition) {
    return rule;
  }
  return _else;
};

export const ifHeightGreaterOrEqual = (condition, rule, _else = null) => {
  if (deviceHeight >= condition) {
    return rule;
  }
  return _else;
};

export const ifHeightLesserOrEqual = (condition, rule, _else = null) => {
  if (deviceHeight <= condition) {
    return rule;
  }
  return _else;
};

export const ifWidthGreater = (condition, rule, _else = null) => {
  if (deviceWidth > condition) {
    return rule;
  }
  return _else;
};

export const ifWidthLesser = (condition, rule, _else = null) => {
  if (deviceWidth < condition) {
    return rule;
  }
  return _else;
};

export const ifWidthGreaterOrEqual = (condition, rule, _else = null) => {
  if (deviceWidth >= condition) {
    return rule;
  }
  return _else;
};

export const ifWidthLesserOrEqual = (condition, rule, _else = null) => {
  if (deviceWidth <= condition) {
    return rule;
  }
  return _else;
};

export const fontSize = (val => Normalize(val));