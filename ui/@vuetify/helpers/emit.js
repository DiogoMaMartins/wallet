
export default function $emit(props, event, arg) {
    const cbName = 'on' + event.substr(0, 1).toUpperCase() + event.substr(1)

    if(props && props[cbName]) {
        var res = props[cbName](arg);
        if(typeof(res) === 'function')// && res.name === 'boundFn')
            res = res(arg);

        return res;
    }

    return null;
}

$emit.model = function(props, event, arg) {
    //const $e = /*typeof(arg) === 'string' ? arg :*/ {};
    //$e.nativeEvent = { text: arg };
    return $emit(props, event, arg)
}