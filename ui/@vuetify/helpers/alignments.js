const MARGIN_PADDING_STEPS = 4;

export default (props) => {
  const obj = {};
  if (props['alignBaseline'] || props['alignItemsBaseline']) obj.alignItems = 'baseline';
  else if (props['alignCenter'] || props['alignItemsCenter']) obj.alignItems = 'center';
  else if (props['alignStart'] || props['alignItemsStart']) obj.alignItems = 'flex-start';
  else if (props['alignEnd'] || props['alignItemsEnd']) obj.alignItems = 'flex-end';

  if (props['alignContentCenter']) obj.alignContent = 'center';
  else if (props['alignContentStart']) obj.alignContent = 'flex-start';
  else if (props['alignContentEnd']) obj.alignContent = 'flex-end';
  else if (props['alignContentSpaceAround']) obj.alignContent = 'space-around';
  else if (props['alignContentSpaceBetween']) obj.alignContent = 'space-between';

  if (props['justifyBaseline'] || props['justifyItemsBaseline']) obj.justifyItems = 'baseline';
  else if (props['justifyCenter'] || props['justifyItemsCenter']) obj.justifyItems = 'center';
  else if (props['justifyStart'] || props['justifyItemsStart']) obj.justifyItems = 'flex-start';
  else if (props['justifyEnd'] || props['justifyItemsEnd']) obj.justifyItems = 'flex-end';

  if (props['justifyContentCenter']) obj.justifyContent = 'center';
  else if (props['justifyContentStart']) obj.justifyContent = 'flex-start';
  else if (props['justifyContentEnd']) obj.justifyContent = 'flex-end';
  else if (props['justifyContentSpaceAround']) obj.justifyContent = 'space-around';
  else if (props['justifyContentSpaceBetween']) obj.justifyContent = 'space-between';
  else if (props['justifySpaceAround']) obj.justifyContent = 'space-around';
  else if (props['justifySpaceBetween']) obj.justifyContent = 'space-between';

  if (props.row) obj.flexDirection = 'row';
  else if (props.column) obj.flexDirection = 'column';

  if (props.reverse) {
    obj.flexDirection = `${obj.flexDirection || 'row'}-reverse`;
  }
  if (props.wrap) {
    obj.flexWrap = 'wrap';
  }

  if (props.fluid) {
    obj.width = '100%';
  }

  for (var i = 0; i <= 10; i += 1) {
    if (props[`ma${i}`]) obj.margin = i * MARGIN_PADDING_STEPS;
    if (props[`my${i}`]) obj.marginVertical = i * MARGIN_PADDING_STEPS;
    if (props[`mx${i}`]) obj.marginHorizontal = i * MARGIN_PADDING_STEPS;
    if (props[`ml${i}`]) obj.marginLeft = i * MARGIN_PADDING_STEPS;
    if (props[`mt${i}`]) obj.marginTop = i * MARGIN_PADDING_STEPS;
    if (props[`mr${i}`]) obj.marginRight = i * MARGIN_PADDING_STEPS;
    if (props[`mb${i}`]) obj.marginBottom = i * MARGIN_PADDING_STEPS;
    if (props[`pa${i}`]) obj.padding = i * MARGIN_PADDING_STEPS;
    if (props[`py${i}`]) obj.paddingVertical = i * MARGIN_PADDING_STEPS;
    if (props[`px${i}`]) obj.paddingHorizontal = i * MARGIN_PADDING_STEPS;
    if (props[`pl${i}`]) obj.paddingLeft = i * MARGIN_PADDING_STEPS;
    if (props[`pt${i}`]) obj.paddingTop = i * MARGIN_PADDING_STEPS;
    if (props[`pr${i}`]) obj.paddingRight = i * MARGIN_PADDING_STEPS;
    if (props[`pb${i}`]) obj.paddingBottom = i * MARGIN_PADDING_STEPS;
  }

  if (
    obj.alignItems ||
    obj.alignContent ||
    obj.justifyItems ||
    obj.justifyContent ||
    obj.flexDirection ||
    obj.flexWrap
  ) {
    obj.display = 'flex';
  } else {
    for (var d of [ 'flex', 'block', 'table', 'inline', 'inline-flex', 'inline-block', 'inline-table', 'table-row', 'table-column', 'list-item' ]) {
      if (props[`d-${d}`]) obj.display = d;
    }
  }

  /*
  if (props.style && typeof props.style.unshift === 'function') {
    let merge = Object.assign({}, obj);
    props.style.forEach((style) => {
      if (typeof style === 'object')
        merge = Object.assign({}, merge, style);
    });
    props.style = merge;
  } else {
    props.style = Object.assign({}, obj, (props.style || {}));
  }
  */

  // fill-height -> props.children same height?

  return obj;
}