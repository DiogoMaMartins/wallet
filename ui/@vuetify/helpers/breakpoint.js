import React from 'react';
import { Dimensions } from 'react-native';

const {width} = Dimensions.get('window');
const breakpoints = {
  xl: [1905, 99999],
  lg: [1265, 1904],
  md: [960, 1264],
  sm: [600, 959],
  xs: [0, 599],
};

export default () => {
  for(var type of Object.keys(breakpoints)) {
    if(breakpoints[type][0] <= width) {
      return type;
    }
  }
};