import React from 'react';
import { TouchableOpacity, TouchableNativeFeedback, StyleSheet, View, Platform } from 'react-native';
import VText from '@vuetify/v-text';
import $colors from '@config/colors';
import RippleFeedbackIOS from './RippleFeedbackIOS';
import $emit from '@vuetify/helpers/emit';
import { Color } from '../helpers/providers';
import { Consumer as FormConsumer } from '../form';
import { createEvent } from '@addons/apiManager/event';

const log = () => {
  console.log('please attach a method to this component'); //eslint-disable-line no-console
};

function parseColor(color, light, dark, flat, disabled, icon, outline) {
    const res = {
        background: (flat || icon) ? 'transparent' : $colors.grey,
        textColor: light ? 'black' : (dark ? 'white' : null),
        ripple: disabled ? 'transparent' : $colors.grey,
    }

    if(disabled && (flat || icon)) {
        res.textColor = $colors.grey
    } else if(disabled) {
        res.textColor = 'white';
        res.background = $colors.grey
    }

    if(!color || disabled)
        return res;

    const colors = color.split(' ').filter((o) => o != '');
    for(var c of colors) {
        if(c.length > 6 && c.substr(c.length - 6) === '--text') {
            res.textColor = $colors[c.substr(0, c.length - 6)] || c.substr(0, c.length - 6);
        } else if(flat) {
            res.textColor = $colors[c] || c;
            if(flat)
                res.ripple = res.textColor;
        } else {
            res.background = $colors[c] || c;
            if(!disabled)
                res.ripple = res.background;
            if((c === 'primary' || c === 'error') && !outline)
                res.textColor = '#fff';
        }
    }

    return res;
}

function getBtnComponent(ripple, tag) {
    if(tag)
        return { Container: tag, attr: {} };

    if(ripple === false)
        return { Container: TouchableOpacity, attr: {} };

    if (Platform.OS === 'ios')
        return { Container: RippleFeedbackIOS, attr: {} };

    if (Platform.OS === 'android')
        return {
            Container: TouchableNativeFeedback, 
            attr: {
                background: TouchableNativeFeedback.Ripple(
                  'ThemeAttrAndroid',
                  true
                )
            }
        }

    return { Container: RippleFeedbackIOS, attr: {} };
}

function putButtonSize(style, small, large, fab, icon, round) {
    let width = null;
    let height = null;

    if(small) {
        height = 40
    }  else if(large) {
        height = 50
    } else if(fab) {
        height = 50;
    }

    if(fab || icon)
        width = height

    if(width)
        style.width = width
    if(height)
        style.height = height
    if((fab || icon || round) && height)
        style.radius = height / 2
}

const VBtn = props => {
  var { absolute, activeClass, append, block, bottom, color, dark, depressed, disabled, exact, exactActiveClass, fab, fixed, flat, href, icon, inputValue, large,
           left, light, loading, outline, replace, right, ripple, round, small, tag, target, to, top, type, value, children, style, ...rest } = props;

  style = style || {}
  let { Container, attr } = getBtnComponent(ripple, tag);
  const textStyle = {};
  const containerStyle = { marginTop: 8, marginBottom: 8, marginLeft: 8, marginRight: 8 };

  color = parseColor(color || '', light, dark, flat, disabled, icon, outline)
  textStyle.color = color.textColor;
  if (style.fontSize > 0) textStyle.fontSize = Number(style.fontSize);
  let background = color.background || background;
  let rippleBackground = color.ripple;

  const form = FormConsumer._currentValue;
  let radius = round || icon || fab ? 28 : 0;

  if(outline) {
      if(!textStyle.color)
          textStyle.color = background;

      containerStyle.borderWidth = 1;
      containerStyle.borderColor = background;
      background = 'transparent';
  }

  putButtonSize(containerStyle, small, large, fab, icon, round)
  if(style.borderRadius) {
    containerStyle.radius = style.borderRadius;
  }
  if(containerStyle.radius)
      radius = containerStyle.radius;

  if(block) {
      containerStyle.marginTop = containerStyle.marginBottom = 6;
      containerStyle.marginLeft = containerStyle.marginRight = 0;
  }

  return (
      <View style={[{ overflow: 'hidden', borderRadius: radius }, containerStyle, style]}>
          <Container
            underlayColor={rippleBackground || 'transparent'}
            onPress={() => {
                const event = createEvent('click');
                $emit(props, 'click', event)

                if(!event.defaultPrevented && type === 'submit' && form)
                    form.submit();
            }}
            disabled={disabled || false}
            {...attr}
            dark={dark}
            color={rippleBackground}
            style={{ borderRadius: radius }}>
            <View style={[
                      styles.button, 
                      { backgroundColor: background, borderRadius: radius, width: containerStyle.width, height: containerStyle.height },
            ]} >
                { 
                    (icon || fab) ?
                        (
                            <Color.Provider value={ textStyle.color }>{children}</Color.Provider>
                        ) :
                        (
                            <VText i18n style={[ textStyle ]}>
                                 {children}
                            </VText>
                        )
                }
            </View>
          </Container>
    </View>
  );
};

const styles = StyleSheet.create({
    button: {
        padding: 8,
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        borderColor: 'transparent',
        borderWidth: 2
    },
});

export default VBtn;