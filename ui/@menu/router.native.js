import { DrawerItems, createDrawerNavigator } from 'react-navigation';
import $translator from '@addons/translator'

import Wallets from '@screens/wallets';


export const Navigator = createDrawerNavigator({

    Wallets,

    //Camera
}, {
    initialRouteName: 'Wallets',
    contentComponent: (props) => Navigator.getDrawer ? Navigator.getDrawer(props) : DrawerItems(props)
})

$translator.onLangChange((lang) => {
    if(Navigator.setKey)
        Navigator.setKey(lang);
})

export const Router = Navigator.router