function getTabs($route) {
    if(!$route || !$route.matched[0] || !$route.matched[0].components.default)
        return {};

    if ($route.matched.length > 0 && $route.matched[0].components.default.tabs) {
        const options = $route.matched[0].components.default;
        var tabs = options.tabs;

        if(typeof(tabs) === 'function')
            tabs = Object.assign({}, tabs.call($route.params));

        if(Object.values(tabs).length > 0)
            return tabs || {};
    }

    return {};
}

export default {

    data() {
        return {
            selectedTab: this.$findSelectedTab()
        }
    },

    updated() {
      this.$nextTick(() => {
        const found = this.$findSelectedTab();
        if (found && found !== '0' && parseInt(this.selectedTab) !== parseInt(found))
            this.$set(this, 'selectedTab', String(found));
      });
    },

    computed: {
        tabs() {
            const tabs = getTabs(this.$route);
            if(Object.keys(tabs).length === 0)
                this.selectedTab = '0';

            if (this.$route.query && this.$route.query.search && this.$route.query.search.length) {
              return [];
            }
            return tabs;
        },

        path() {
            return this.$route.path;
        }
    },

    watch: {
        path(val, old) {
            var keys = Object.keys(this.tabs);
            if (val.split('/')[1] !== old.split('/')[1]) {
                const found = this.$findSelectedTab();
                if (found > -1 && found < keys.length && parseInt(this.selectedTab) !== parseInt(found))
                    this.$set(this, 'selectedTab', String(found));
            } else if(val.split('/')[2] !== old.split('/')[2]) {
                var sub = val.split('/')[2];
                if(!sub)
                    sub = keys[0];
                const index = keys.indexOf(sub);
                if(index > -1 && index < keys.length && index !== parseInt(this.selectedTab)) {
                    this.$set(this, 'selectedTab', String(index));
                }
            }
        },

        selectedTab(val, oldVal) {
            if (val === oldVal)
                return;

            var instanceTab = this.$route.matched[0].instances.default ? this.$route.matched[0].instances.default.urlTab : undefined;

            if (instanceTab !== undefined) {
                const tabs = this.tabs;
                const index = parseInt(val, 10);
                const name = String((Array.isArray(tabs) ? tabs : Object.keys(tabs))[index] || '')
                  .toLowerCase()
                  .split(' ')
                  .join('-');

                if (name === instanceTab || (index === 0 && instanceTab === null))
                    return;

                const params = this.$route.params;

                if (!params.urlTab && this.$route.matched[0].instances.default.urlTab !== undefined) {
                    const route = this.$router.options.routes.find((obj) => obj.path === this.$route.path + '/:urlTab');

                    if (route) {
                        const path = route.path.split('/');
                        params.urlTab = name;

                        for (var key in path) {
                            if (path[key].substr(0, 1) === ':' && params[path[key].substr(1)]) {
                                path[key] = params[path[key].substr(1)];
                            }
                        }

                        this.$router.replace({
                            path: path.join('/')
                        });
                    }
                } else {
                    params.urlTab = name;

                    this.$router.replace({
                        name: this.$route.name,
                        params: params
                    });
                }
            }
        }
    },

    methods: {
        $findSelectedTab() {
            if (this.$route.matched.length === 0)
                return '0';

            const options = this.$route.matched[0].components.default;
            const tabs    = getTabs(this.$route);
            if (Object.keys(tabs || {}).length == 0)
                return '0';

            const prop = (options.props || {}).selectedTab || {};
            const instanceTab = this.$route.params.urlTab;

            if (!instanceTab) {
                if (options.initialTab !== undefined)
                    return options.initialTab;
                else if (prop.default !== undefined)
                    return (typeof (prop.default) === 'function') ? prop.default() : prop.default;
                else
                    return '0';
            }

            var count = 0;
            for (var key in tabs) {
                if (String(key).toLowerCase().split(' ').join('-') === instanceTab) {
                    return String(count);
                }
                count += 1;
            }

            return '0';
        }
    }
}

function createMixin(def) {
    return {
        props: {
            urlTab: {
                default: null,
                type: String,
                required: false
            },
            selectedTab: {
                default: String(def),
                required: true,
                urlProp: false
            }
        }
    };
}

Vue.selectTabsMixin = createMixin(0);
Vue.selectTabsMixin.default = (n) => createMixin(n);
