//import { Platform } from 'react-native';
import React from 'react';
import { createSwitchNavigator } from 'react-navigation';

import Main from './index';
import { Router } from './router';
import Offline from '@screens/offline/login-flow';

console.disableYellowBox = true;

export default function (route) {
    Main.router = Router; //ToDo check how to automaticly attach Router from v-navigation-drawer to Main (Maybe creating a router bridge ?)

    const nav = createSwitchNavigator({
        Offline: Offline,
        Main: Main,
    }, {
        initialRouteName: route,
        headerMode: 'none',
    });

    return nav;
};
