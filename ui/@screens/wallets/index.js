import React from 'react';
import { createStackNavigator } from 'react-navigation';

import Wallet from './index.vue';


export default createStackNavigator(
  {
    Wallet: { screen: Wallet },

  },
  {
    initialRouteName: 'Wallet',
    headerMode: 'none',

    navigationOptions: {
      gesturesEnabled: true,
    },
  }
);
