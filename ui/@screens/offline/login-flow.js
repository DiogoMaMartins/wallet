import { Platform } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import CreateWalletRestore from './firstTime.vue';
import SmallIntroductionCreateWallet from './createWalletInfo.vue';
import CreateKeysForAdress from './create_keys_for_adress.vue';
import VerifyWords from './verifyWords.vue';
import CreatePassword from './createPassword.vue';
import LoginPage from './loginPage.vue';
/*Private routes*/
import Wallet from '../wallets';
import CreateWallet from '../wallets/createWallet.vue';
import Coupler from '../wallets/coupler.vue';
import Transaction from '../wallets/transaction.vue';
import Send from '../wallets/send.vue';
import CopyFunction from '../wallets/copyFunction.js';

const offlineNavigation = createStackNavigator(
  {
    CreateWalletRestore,
    SmallIntroductionCreateWallet,
    LoginPage,
    CreateKeysForAdress,
    VerifyWords,
    Wallet,
    CreatePassword,
    CreateWallet,
    Coupler,
    Transaction,
    Send,
    CopyFunction

  },
  {
    initialRouteName: 'CreatePassword',
    headerMode: 'none',
    cardStyle: { backgroundColor: '#fff' },
    navigationOptions: {
      gesturesEnabled: true,
    },
    mode: Platform.OS === 'ios' ? 'modal' : 'card',
  }
);

export default offlineNavigation;
