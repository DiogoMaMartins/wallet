import React from 'react';
import { StyleSheet, View, ScrollView } from 'react-native';
import { VText, VBtn } from '@vuetify';
import Colors from '@config/colors'
import Translator from '@addons/translator'

export default class NoWifi extends React.Component {
  render() {
      //        {/*<SvgUri width={107} height={99} style={{ alignItems: 'center' }} source={require('../../assets/images/svg/no-wifi.svg')} />*/}
    return (
        <ScrollView keyboardShouldPersistTaps="always" style={{ flex: 1 }}>
            <View style={styles.container}>
              <VText style={styles.title}>
                {Translator.$t('wifi.error1')}
              </VText>
              <VText style={styles.message}>
                {Translator.$t('wifi.error2')}
              </VText>

              <VBtn style={styles.button} color="primary" round>
                {Translator.$t('global.btn9')}
              </VBtn>
            </View>
        </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 150,
    flex: 1,
  },

  button: {
    marginLeft: 20,
    marginRight: 20,
  },

  title: {
    marginTop: 50,
    fontSize: 20,
    textAlign: 'center',
    color: Colors.black,
    fontFamily: 'Exo2-Medium',
  },

  message: {
    textAlign: 'center',
    color: Colors.black,
    fontFamily: 'Exo2-Regular',
    marginLeft: 40,
    marginRight: 40,
    marginTop: 20,
    marginBottom: 50,
    fontSize: 14,
    lineHeight: 24,
  },
});