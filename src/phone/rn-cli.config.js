const { getDefaultConfig } = require("metro-config");
const path      = require('path');
const resolver  = require('./plugins/resolver.js');

require('./plugins/watch-components.js');

const resolve = resolver({
    node_modules: [
        path.resolve(__dirname, 'node_modules'),
        path.resolve(__dirname, '../../ui', 'node_modules'),
    ],
    alias: require('../../ui/@config/alias.js'),
});

module.exports = (async () => {
  const {
    resolver: { sourceExts }
  } = await getDefaultConfig();

  return {
      //resetCache: true,
      projectRoot: path.resolve(__dirname),
      watchFolders: [
          path.resolve(__dirname, "../../ui/"),
          path.resolve(__dirname, "../../assets/"),
          path.resolve(__dirname, "../../insight/"),
          path.resolve(__dirname, "../addons/"),
          path.resolve(__dirname, "../dev_modules/vueglobalregistration-webpack-plugin/dynamic-components/runtime/"),
      ],
      transformer: {
          babelTransformerPath: require.resolve("./plugins/vueTransformerPlugin.js"),
      },
      getBlackListRE() {
        //return metro.createBlacklist([/dist\/mac\/.*/, /dist\/win-unpacked\/.*/])
      },
      resolver: {
          sourceExts: [...sourceExts, "vue"],
          resolveRequest: resolve,
      }
  };
})();

module.exports.$resolve = resolve;