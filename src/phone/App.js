import './shim.js'
import React from 'react';
import { NetInfo, View } from 'react-native';
import { NavigationActions, createAppContainer } from 'react-navigation';
import * as Font from 'expo-font'
import {
  ActionSheetProvider,
} from '@expo/react-native-action-sheet';
import '@addons';
import '@addons/phone';
import $db from '@addons/database';
import $api from '@addons/apiManager';
import NoWifi from '@screens/offline/no-wifi';
import Menu from '@menu/main-navigation';
import Router from '@addons/phone/router';
import Translator from '@addons/translator';

import { Sentry } from 'react-native-sentry';

Sentry.config('https://eda2fdab1bff40378f9c293353fb5dc2@sentry.io/274729').install();

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fontLoaded: false,
            netConnected: true,
            language: null,
            currency: 'USD',
            virtual: $api.isSimulation,
        };

        this.databaseRefresh();

        $api.on('logout', () => {
            if(this.$el) {
                this.$el.dispatch(NavigationActions.navigate({
                    routeName: 'Offline',
                    params: {}
                }));
            }
        })

        Translator.onLangChange((lang) => {
            if(lang !== this.state.language) {
                this.setState({
                    language: lang,
                });
            }
        })
    }

    componentWillReceiveProps() {
        this.databaseRefresh();
    }

    databaseRefresh() {
        let obj = {};

        const curr = $db.getCurrency().name;
        if (curr !== this.state.currency) obj.currency = curr;

        if ($api.isSimulation !== this.state.virtual)
            obj.virtual = $api.isSimulation;

        if (Object.keys(obj).length > 0) {
            this.setState(obj);
            // this.forceUpdate();
        }
    }

    async componentDidMount() {
        try {
            await Font.loadAsync({
                b4yIcons: require('@assets/fonts/icons/fonts/bit4you.ttf'),
                'Exo2-Medium': require('@assets/fonts/exo2/Exo2-Medium.ttf'),
                'Exo2-Regular': require('@assets/fonts/exo2/Exo2-Regular.ttf'),
                'Exo2-Bold': require('@assets/fonts/exo2/Exo2-Bold.ttf'),
            });
        } catch (e) {
            console.error(e);
        }

        //const netInfo = await NetInfo.isConnected.fetch(); // wrong on IOS ?

        if (!this.state.fontLoaded || !this.state.netConnected) {
            this.setState({
                fontLoaded: true,
                netConnected: true,
            });
        }

        NetInfo.isConnected.addEventListener('connectionChange', (connected) => {
            if (connected !== this.state.netConnected)
                this.setState({
                    netConnected: connected,
                });
        });

        $db.getLanguage().then((lang) => {
            if (lang !== this.state.language)
                this.setState({ language: lang });

            this.databaseRefresh();
        })
    }

    bindNav(el) {
        this.$el = el;
        Router._nav = el;
    }

    render() {
        if (!this.state.fontLoaded) return null;
        if (!this.state.netConnected) return ( <NoWifi /> );

        const auth = $api.$storage.getCookie('auth') || {};
        if (auth.expires && auth.connected) {
            if (String(parseInt(auth.expires, 10)) === String(auth.expires) && String(auth.expires).length === 10) {
                auth.expires = Number(auth.expires) * 1000;
            }

            auth.connected = new Date(auth.expires).getTime() > new Date().getTime();

            if (!auth.connected) {
                $api.setCookies({
                    auth: {
                        connected: false
                    }
                });
            }
        }

        const Main = this.MainPage = this.MainPage || createAppContainer(Menu(auth.connected ? 'Main' : 'Offline'))
        $db.setProfile(auth);
        $db.key_token = ((auth.connected && auth.key_token) || null);

        return (
          <ActionSheetProvider>
            <Main screenProps={{ lang: this.state.language }} ref={(el) => this.bindNav(el)} />
          </ActionSheetProvider>
        );
    }
}
