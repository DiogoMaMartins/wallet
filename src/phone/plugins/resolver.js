const path      = require('path');
const fs        = require('fs');
const _resolve  = require('resolve');

fs.writeFileSync(__dirname + '/../node_modules/metro-resolver/src/resolve.js', "module.exports = require('../../../rn-cli.config.js').$resolve");

function getPackage(basedir) {
    while(basedir.length > 0) {
        const filepath = path.join(basedir, 'package.json')
        if(fs.existsSync(filepath) && fs.statSync(filepath).isFile()) {
            const r = JSON.parse(fs.readFileSync(filepath))
            r.$path = basedir;
            return r;
        }

        if(basedir === '/')
            break;

        basedir = path.resolve(basedir, '../');
    }

    return {}
}

function resolve(name, { basedir, platform, aliases, sourceExts, node_modules, mainFields }) {
    basedir = basedir || __dirname;
    aliases = aliases || {};

    for(var alias in aliases) {
        if(name === alias) {
            name = './index'
            basedir = path.resolve(basedir, aliases[alias]);
            break;
        } else if(name.length > alias.length && name.substr(0, alias.length + 1) === alias + '/') {
            basedir = path.resolve(basedir, aliases[alias]);
            name = './' + name.substr(alias.length + 1);
            break;
        }
    }

    if(name.substr(0, 1) === '!')
        name = name.substr(1);

    const tmpPath = path.resolve(basedir, name);
    if(fs.existsSync(tmpPath) && fs.statSync(tmpPath).isFile())
        return tmpPath;

    sourceExts = sourceExts || ['js', 'vue'];
    mainFields = mainFields || [ 'react-native', 'browser', 'main' ];

    const extensions = [];
    for(var ext of sourceExts) {
        extensions.push('.' + platform + '.' + ext);
        extensions.push('.native.' + ext);
        extensions.push('.' + ext);
    }

    extensions.push('.json');
    const package = getPackage(basedir);
    for(var i of mainFields) {
        if(!package[i])
            continue;

        if(typeof(package[i]) === "object") {
            if(package[i][name])
                return path.resolve(package.$path, package[i][name])
        }

        break;
    }

    if(name.substr(0, 1) === "." && fs.existsSync(path.resolve(package.$path, name)) && fs.statSync(path.resolve(package.$path, name)).isFile())
        return path.resolve(package.$path, name);

    return _resolve.sync(name, {
        basedir,
        extensions,
        moduleDirectory: node_modules || [
            path.resolve(__dirname, '..', 'node_modules'),
            path.resolve(basedir, 'node_modules'),
        ],
        packageFilter(pkg, dir) {
            for(var i of mainFields) {
                if(typeof(pkg[i]) === 'string') {
                    pkg.main = pkg[i];
                    break;
                }
            } 

            return pkg
        }
    });
}

//--------------------------------

module.exports = function(config) {
    config.cache = {};

    return function(context, moduleName, platform) {
        const realModuleName = context.redirectModulePath(moduleName);
        if(moduleName.substr(0, 1) !== '.' && config.cache[realModuleName]) {
            return {
                type: 'sourceFile',
                filePath: config.cache[realModuleName],
            }
        }

        // exclude
        if (realModuleName === false) {
            return {
                type: 'empty'
            };
        }

        const filepath = resolve(realModuleName, {
            mainFields:   context.mainFields,
            node_modules: config.node_modules,
            aliases:      config.alias,
            basedir:      path.dirname(context.originModulePath),
            platform
        });

        if(filepath.indexOf('.ttf') !== -1) {
            const assets = context.resolveAsset(path.dirname(filepath), path.basename(filepath), platform);
            return {
                type: 'assetFiles',
                filePaths: assets.map((o) => path.resolve(path.dirname(filepath), o))
            };
        }

        if(moduleName.substr(0, 1) !== '.')
            config.cache[realModuleName] = filepath;
        
        return {
            type:'sourceFile',
            filePath: filepath,
        }
    }
}

module.exports.resolve = resolve;
module.exports.getPackage = getPackage;