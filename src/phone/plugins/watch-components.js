const sane   = require('sane');
const path   = require('path');
const plugin = require('./vueTransformerPlugin.js');
const fs     = require('fs');

function watch(platform) {
    if(process.argv[2] === 'link')
        return;

    console.log('Starting global components watcher');

    const config = plugin.getConfig([ platform, 'native' ]);

    var timeout = null;
    const handler = function (filepath, root, stat) {
        if(timeout)
            clearTimeout(timeout);

        timeout = setTimeout(() => {
            timeout = null;
            const src = plugin.injectComponents(config);
            fs.writeFileSync(config.globalRegistrationFile, src);
        }, 50);
    };

    var watcher = sane(path.join(__dirname, '../../../'), {
        glob: ['**/*.js', '**/*.vue'],
        dot: false,
        watchman: true,
    });

    //watcher.on('change', handler);
    watcher.on('add', handler);
    watcher.on('delete', handler);
}

watch('ios');