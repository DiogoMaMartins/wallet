require('module-alias/register')
const semver = require('semver');
const vueNaiveScripts = require("vue-native-scripts");
const reactNativeVersionString = require("react-native/package.json").version;
const reactNativeMinorVersion = semver(reactNativeVersionString).minor;
const getComponents = require('../../dev_modules/vueglobalregistration-webpack-plugin/dynamic-components').getComponents;
const getConfig     = require('../../../ui/@config/global-components.js');
const templateBuilderPath = require.resolve('vue-native-helper/build.js');
const fs = require('fs');
const aliases = require('../../../ui/@config/alias.js');

if (reactNativeMinorVersion >= 56) {
  upstreamTransformer = require("metro/src/reactNativeTransformer");
} else if (reactNativeMinorVersion >= 52) {
  upstreamTransformer = require("metro/src/transformer");
} else if (reactNativeMinorVersion >= 47) {
  upstreamTransformer = require("metro-bundler/src/transformer");
} else if (reactNativeMinorVersion === 46) {
  upstreamTransformer = require("metro-bundler/build/transformer");
} else {
  // handle RN <= 0.45
  var oldUpstreamTransformer = require("react-native/packager/transformer");
  upstreamTransformer = {
    transform({ src, filename, options }) {
      return oldUpstreamTransformer.transform(src, filename, options);
    }
  };
}

var vueExtensions = ["vue"]; // <-- Add other extensions if needed.
var configs = {};

function injectComponents(config) {
    const components = getComponents(config).filter((o) => {
        return o.name.substr(0, 6) != 'helper';
    });

    var newContent = "import Vue from 'vue-native-core';\n";

    return newContent + components.map((obj) => {
        if(obj.name === '')
            return;

        obj.importName = obj.importName || obj.name.split('-').map((o) => o.substr(0,1).toUpperCase() + o.substr(1).toLowerCase()).join('');
        var path = obj.path;
        for(var alias in aliases) {
            if(path.substr(0, aliases[alias].length) === aliases[alias]) {
                path = alias + path.substr(aliases[alias].length)
                break;
            }
        }

        return `import ${obj.importName} from '${path}';`;
    }).join('\n') + "\n\n" + components.map((obj) => {
        if(obj.name === '')
            return;

        return `Vue.component('${obj.name}', ${obj.importName});`;
    }).join('\n');
}

module.exports.injectComponents = injectComponents;
module.exports.getConfig = getConfig;

module.exports.transform = function({ src, filename, options }) {
    if(!configs[options.platform]) {
        configs[options.platform] = getConfig([ options.platform, 'native' ]);
    }

    const config = configs[options.platform];
    if(filename === config.globalRegistrationFile) {
        const nsrc = injectComponents(config);
        if(src !== nsrc) {
            src = nsrc;
            fs.writeFileSync(filename, nsrc);
        }
    } else if(config.reactIgnoreCss && filename === templateBuilderPath) {
        src = src + "\n" + inject + `\n\nexports.buildNativeComponent = ignoreCss(${JSON.stringify(config.reactIgnoreCss)});`;
    }

    if (vueExtensions.some(ext => filename.endsWith("." + ext))) {
        return vueNaiveScripts.transform({ src, filename, options });
    }

    return upstreamTransformer.transform({ src, filename, options });
};

const inject = `function ignoreCss(props) {
    return function(render, options, config) {
        const css = config.css;
        for(var i in css) {
              const selector = css[i];
              for(var field of props)
                  if(selector[field])
                      delete selector[field];
        }

        config.css = css;
        return buildNativeComponent(render, options, config);
    }
}`