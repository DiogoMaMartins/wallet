const fs        = require('fs');
const path      = require('path');
const crypto    = require('crypto');
const minify    = require('html-minifier').minify;
const sm        = require('sitemap');
const request   = require('request-promise-native');
const { createBundleRenderer } = require('vue-server-renderer');

class SsrPrerenderPlugin {
    constructor(options = {}) {
        this.options = options;
    }

    buildAll(distPath) {
        return this._buildAll({
            compilation: {
                compiler: {
                    outputPath: distPath
                }
            }
        })
    }

    async build(distPath, route) {
        this._setupRenderer(path.join(distPath, '.ssr'));
        return await this._build({ path: route }, distPath);
    }

    getTemplate(path) {
        var template = fs.readFileSync(path, 'utf-8');
        template = template.replace('<div id=app></div>', '<!--vue-ssr-outlet-->').replace('</head>', `{{{ meta.inject().title.text() }}}
                    {{{ meta.inject().meta.text() }}}
                    {{{ meta.inject().link.text() }}}
                    {{{ meta.inject().base.text() }}}
                    {{{ meta.inject().style.text() }}}
                    {{{ meta.inject().noscript.text() }}}
                    </head>`)
                    .replace('</body>', `{{{ meta.inject().script.text() }}}</body>`)
                    .replace('<html', '<html {{{ meta.inject().htmlAttrs.text() }}}')
                    .replace('<head', '<head {{{ meta.inject().headAttrs.text() }}}')
                    .replace('<body', '<body {{{ meta.inject().bodyAttrs.text() }}}')

        return template;
    }

    getRoutes() {
        const context = { url: '/', title: '', getOnly: true }
        return this.renderer.renderToString(context).then((res) => {
            return context.$router.options.routes.filter(el => !el.redirect);
        });
    }

    getTemplatePath(ssrFolder = '') {
        return this.options.templatePath || path.join(ssrFolder, 'template.html');
    }

    getBundlePath(ssrFolder = '') {
        return this.options.ssrBundlePath || path.join(ssrFolder, 'vue-ssr-server-bundle.json');
    }

    _setupRenderer(ssrFolder) {
        var template = this.getTemplate(this.getTemplatePath(ssrFolder));
        const serverBundle = require(this.getBundlePath(ssrFolder));
        const clientManifest = require(this.options.ssrManifestPath || path.join(ssrFolder, 'vue-ssr-client-manifest.json'));

        this.redirectUris = [];
        this.buildFolders = [];
        this._builds = {};
        this.renderer = createBundleRenderer(serverBundle, {
            //runInNewContext: false,
            template,
            //clientManifest,
        });

        return this.renderer;
    }

    async _build(route, distFolder) {
        if(route.path === '*')
            route.path = '/error';

        if(route.path !== '/')
            this.mkdir(route.path + '/', distFolder);

        const context = { url: route.path, title: '' }
        const start = Date.now()
        var html = await this.renderer.renderToString(context);
        console.log('build time:', Date.now() - start, 'ms')

        const length = html.length
        if(this.options.minify !== false) {
            html = minify(html, Object.assign({
                collapseBooleanAttributes: true,
                collapseInlineTagWhitespace: true,
                caseSensitive: true,
                minifyCSS: true,
                removeAttributeQuotes: true,
                removeOptionalTags: true,
                removeRedundantAttributes: true,
                collapseWhitespace: true,
                conservativeCollapse: true,
                //removeComments: true,
            }, this.options.minify || {}));
        }

        fs.writeFileSync(`${distFolder}${route.path === '/' ? '' : route.path}/index.html`, html);

        const startIndex = html.indexOf('>', html.indexOf('<body'));
        const endIndex = html.indexOf('<script src=/js/chunk', startIndex);
        const body = html.substr(startIndex === -1 ? 0 : startIndex, endIndex === -1 ? html.length : endIndex - startIndex);

        const shasum = crypto.createHash('sha1');
        shasum.update(body);
        this._builds[route.path] = shasum.digest('hex').substr(0, 10);
        console.log('build', route.path, length, '=>', html.length)
    }

    async _buildAll({compilation}) {
        const ssrFolder = path.join(compilation.compiler.outputPath, '.ssr');
        if (fs.existsSync(this.getTemplatePath(ssrFolder)) && fs.existsSync(this.getBundlePath(ssrFolder))) {
            this._setupRenderer(ssrFolder);
            this.routes = await this.getRoutes();

            for(const route of this.routes) {
                await this._build(route, compilation.compiler.outputPath);

                if(route.alias && !Array.isArray(route.alias))
                    route.alias = [ route.alias ];

                if(route.alias) {
                    for(var alias of route.alias) {
                        const r = Object.assign({}, route, { path: alias })
                        await this._build(r, compilation.compiler.outputPath);
                    }
                }
            }

            var sitemap = null;
            if(this.options.sitemap)
                sitemap = await this.generateSitemap(compilation.compiler.outputPath);

            if(this.options.langRedirects)
                await this.generateRedirectUris(sitemap)

            return true;
        }

        return false;
    }

    apply(compiler) {
        compiler.hooks.done.tapPromise('VueGlobalPrerenderPlugin', this._buildAll.bind(this));
    }

    mkdir(path, distFolder) {
        const folders = path.substr(1).split('/');
        folders.length--;
        if(folders.length > 0 && this.buildFolders.indexOf(folders.join('/')) === -1) {
            this.buildFolders.push(folders.join('/'));
            SsrPrerenderPlugin.mkDirByPathSync(`${distFolder}/${folders.join('/')}`)
        }
    }

    static mkDirByPathSync(targetDir, { isRelativeToScript = false } = {}) {
        const sep = path.sep;
        const initDir = path.isAbsolute(targetDir) ? sep : '';
        const baseDir = isRelativeToScript ? __dirname : '.';

        return targetDir.split(sep).reduce((parentDir, childDir) => {
            const curDir = path.resolve(baseDir, parentDir, childDir);
            try {
                fs.mkdirSync(curDir);
            } catch (err) {
                if (err.code === 'EEXIST') { // curDir already exists!
                    return curDir;
                }

                // To avoid `EISDIR` error on Mac and `EACCES`-->`ENOENT` and `EPERM` on Windows.
                if (err.code === 'ENOENT') { // Throw the original parentDir error on curDir `ENOENT` failure.
                    throw new Error(`EACCES: permission denied, mkdir '${parentDir}'`);
                }

                const caughtErr = ['EACCES', 'EPERM', 'EISDIR'].indexOf(err.code) > -1;
                if (!caughtErr || caughtErr && curDir === path.resolve(targetDir)) {
                    throw err; // Throw if it's just the last created dir.
                }
            }

            return curDir;
        }, initDir);
    }

    //----------------------------------

    async generateRedirectUris(sitemap = null) {
        if(!sitemap)
            sitemap = await this.generateSitemapUris();

        //ToDo save in nginx config

        const uris = []
        for(const url of sitemap) {
            if(!url.links)
                continue;

            for(const link of url.links) {
                var path = link.url.substr(this.options.domain.length)
                if(path.substr(0, link.lang.length + 2) === '/' + link.lang + '/')
                    path = path.substr(link.lang.length + 2);
                else if(path.substr(0, link.lang.length + 3) === '/' + link.lang)
                    path = path.substr(link.lang.length + 3);
                else
                    path = path.substr(1);

                if(path === '')
                    continue;

                for(const link2 of url.links) {
                    if(link2.lang === link.lang)
                        continue;

                    const npath = `/${link2.lang}/${path}`;
                    const dest  = link2.url.substr(this.options.domain.length)

                    if(npath == dest || uris.find((o) => o.src == npath))
                        continue;

                    uris.push({
                        src: npath,
                        dest: dest,
                    })
                }
            }
        }

        const config = uris.map((r) => {
            return `rewrite ${r.src} ${r.dest} permanent;`;
        }).join('\n');

        console.log(config)
    }

    //----------------------------------

    generateSitemapUris() {
        const uris = [];
        const handledUris = [];
        var priority = 1;

        const locales = this.routes.map((o) => (o.meta && o.meta.lang) || 'en').filter((v, i, a) => a.indexOf(v) === i);

        for(var route of this.routes) {
            if(route.path === '*' || handledUris.indexOf(route.path) !== -1 || handledUris.indexOf(route.path + '/') !== -1)
                continue;

            handledUris.push(route.path);
            if(!route.component.uri) {
                priority = Math.max(priority - 0.1, 0);

                uris.push({
                    url: route.path,
                    changefreq: 'daily',
                    lastmodISO: (new Date()).toISOString(),
                    priority,
                    hash: this._builds[route.path],
                });

                continue;
            }

            const enVersion = route.component.uri['en'];
            if(enVersion === undefined)
                throw('English uri version not found for path' + route.path);

            priority = Math.max(priority - 0.1, 0);
            const hashes = [];
            uris.push({
                url: '/' + enVersion,
                changefreq: 'daily',
                lastmodISO: (new Date()).toISOString(),
                priority,
                links: locales.map((lang) => {
                    var url = `${lang === 'en' ? '' : '/' + lang}/${route.component.uri[lang] || enVersion}`;
                    if(url.substr(url.length -1) === '/' && url.length > 1)
                        url = url.substr(0, url.length - 1);

                    handledUris.push(url)
                    hashes.push(this._builds[url]);
                    return {
                        lang,
                        url: url,
                    }
                }),
                hash: hashes.join('')
            });
        }

        return uris;
    }

    async generateMergedSitemapUris() {
        var remotes = [];

        if(this.options.domain) {
            const res = await request({
                uri: this.options.domain + '/sitemap.config.json', 
                json: true 
            }).catch((err) => []);

            if(!Array.isArray(res)) {
                console.warn('No sitemap found on domain, generating new one');
            } else {
                remotes = res;
            }
        }

        var uris = this.generateSitemapUris();
        for(var uri of uris) {
            const remote = remotes.find((r) => r.url === uri.url);
            if(!remote)
                continue;

            uri.priority = Math.round(remote.priority * 1000) / 1000;
            if(remote.androidLink && !uri.androidLink)
                uri.androidLink = remote.androidLink;

            if(remote.hash === uri.hash)
                uri.lastmodISO = remote.lastmodISO || uri.lastmodISO;
        }

        const reorder = [ '/', '/services', '/currency-converter', '/contact', '/about-us', '/whois' ]
        for(var i in reorder) {
            const r = uris.find((r) => r.url === reorder[i])
            if(r) {
                r.priority = 1 - (parseInt(i) / 10)
            }
        }

        uris = uris.sort((a, b) => a.priority < b.priority ? 1 : -1);
        for(var i=reorder.length; i<uris.length; i++)
            uris[i].priority = Math.round(Math.max(0, 1 - (parseInt(i) / 10)) * 100) / 100;

        return uris.filter((v, i, a) => a.findIndex((x) => x.url === v.url) === i);
    }

    async generateSitemap(outputPath) {
        console.log('generating sitemap');
        const uris = await this.generateMergedSitemapUris();
        fs.writeFileSync(`${outputPath}/sitemap.config.json`, JSON.stringify(uris, null, 2));

        const sitemap = sm.createSitemap({
            hostname: this.options.domain,
            cacheTime: 600000, // 600 sec - cache purge period
            urls: uris
        })

        var xml = sitemap.toString();
        fs.writeFileSync(`${outputPath}/sitemap.xml`, xml);
        return uris;
    }
}

module.exports = SsrPrerenderPlugin;