const fs = require('fs');
const path = require('path');

var walkSync = function (dir, exts, base, rec, prepend) {
    if (!base)
        base = dir;
    var filelist = [],
        fs = fs || require('fs');
    if (!fs.existsSync(dir))
        return filelist;

    var files = fs.readdirSync(dir);
    var inFiles = {};

    files.forEach(function (file) {
        var fullpath = path.join(dir, file);
        var isdir = fs.statSync(fullpath).isDirectory();
        if (isdir && rec) {
            filelist = filelist.concat(walkSync(fullpath, exts, base, rec, prepend));
        } else if (!isdir && file.substr(0, 1) !== '_') {
            const index = file.indexOf('.');
            if(index >= 0 && file.substr(0, index) === 'index' && exts.indexOf(file.substr(index)) === -1)
                return

            const fileExt = file.substr(index)
            for(var i in exts) {
                const ext = exts[i];
                if(/*file.substr(file.length - ext.length)*/ fileExt === ext) {
                    const inName = file.substr(0, file.length - ext.length);

                    var data = {
                        ext,
                        name: (prepend ? prepend + '-' : '') + GetComponentPath((inName === 'index' ? dir : path.join(dir, inName)).replace(base, "")),
                        fullpath: fullpath.replace(/\\/g, "/"),
                        file: file
                    }

                    if(inFiles[inName] && inFiles[inName] <= i)
                        return;

                    if(inFiles[inName]) {
                        filelist = filelist.filter((o) => {
                            return o.name !== data.name
                        })
                    }

                    inFiles[inName] = i;
                    filelist.push(data);
                    break;
                }
            }
        }
    });
    return filelist;
};

function GetComponentPath(file, separator) {
    return file.substr(1).replace(/\\/g, separator || "-").replace(/\//g, separator || "-").replace(/@/g, "")
}
var r = 0;

module.exports = function getComponents(options) {
    var res = options.folder ? walkSync(path.resolve(options.folder.replace(/\\/g, "/")), options.extensions || [".vue"], null, !(options.recursive == false)) : [];
    if(options.folders) {
        for(var folder of options.folders) {
            if(folder === null)
                continue;

            if(typeof(folder) === "object") {
                const base = path.resolve(folder.path.replace(/\\/g, "/"));
                res = res.concat(walkSync(base, options.extensions || [".vue"], null, !(options.recursive == false), folder.pre || null))
            } else {
                res = res.concat(walkSync(path.resolve(folder.replace(/\\/g, "/")), options.extensions || [".vue"], null, !(options.recursive == false)))
            }
        }
    }

    return res.map((o) => {
        return {
            name: o.name,
            path: o.fullpath,
        }
    });
}