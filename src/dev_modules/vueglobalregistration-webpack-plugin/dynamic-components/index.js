const path = require('path')
const loaderUtils = require('loader-utils')
const compiler = require('vue-template-compiler')

const components     = require('./components');
const { camelize, capitalize, hyphenate, requirePeer } = require('./util')
const runtimePaths = {
  installGlobalComponents: require.resolve('./runtime/installComponents'),
}

function getMatches (type, items, matches, component) {
  const imports = []

  items.forEach(item => {
    for (const matcher of matches) {
      const match = matcher(item, {
        [`kebab${type}`]: hyphenate(item),
        [`camel${type}`]: capitalize(camelize(item)),
        path: this.resourcePath.substring(this.rootContext.length + 1),
        component
      })
      if (match) {
        imports.push(match)
        break
      }
    }
  })

  imports.sort((a, b) => a[0] < b[0] ? -1 : (a[0] > b[0] ? 1 : 0))
  return imports
}

function install (install, content, imports) {
  if (imports.length) {
    let newContent = '/* vueglobalregistration-plugin */\n'
    if(install && content.indexOf('import '+install+' from') === -1)
        newContent += `import ${install} from ${loaderUtils.stringifyRequest(this, '!' + runtimePaths[install])}\n`

    newContent += imports.map(i => i[1]).join('\n') + '\n'
    if(install)
        newContent += `${install}(component, {${imports.map(i => i[0]).join(',')}})\n`

    // Insert our modification before the HMR code
    const hotReload = content.indexOf('/* hot reload */')
    if (hotReload > -1) {
      content = content.slice(0, hotReload) + newContent + '\n\n' + content.slice(hotReload)
    } else if(content.indexOf('<script>') !== -1) {
      var script = content.indexOf('<script>') + 8;
      content = content.slice(0, script) + newContent + '\n\n' + content.slice(script)
    } else {
      content += '\n\n' + newContent
    }
  }

  return content
}

function transform(src, file, options, content = null) {
    const readFile = path => fs.readFileSync(path);

    const tags = new Set()
    const attrs = new Set()
    const component = compiler.parseComponent(src)
    if (component.template) {
      if (component.template.src) {
        const externalFile = path.resolve(path.dirname(file), component.template.src);
        const externalContent = readFile(externalFile).toString('utf8')
        component.template.content = externalContent
      }
      if (component.template.lang === 'pug') {
        const pug = requirePeer('pug')
        try {
          component.template.content = pug.render(component.template.content)
        } catch (err) {/* Ignore compilation errors, they'll be picked up by other loaders */}
      }
      compiler.compile(component.template.content, {
        modules: [{
          postTransformNode: node => {
            Object.keys(node.attrsMap).forEach(attr => attrs.add(attr))
            tags.add(node.tag)
          }
        }]
      })
    }

    const c = components(options)
    const inject = []
    for(var comp of c) {
        if(tags.has(comp.name)) {
            const impName = comp.name.split('-').map((o) => o.charAt(0).toUpperCase() + o.slice(1)).join('');
            inject.push([ impName, 'import '+impName+' from \''+comp.path+'\'' ])
        }
    }

    return install('installGlobalComponents', content || src, inject)
}

module.exports = async function (content, sourceMap, cb) {
  cb = cb || this.async()
  this.cacheable()

  const options = loaderUtils.getOptions(this);

  if (!this.resourceQuery) {
    const readFile = path => new Promise((resolve, reject) => {
      this.fs.readFile(path, function (err, data) {
        if (err) reject(err)
        else resolve(data)
      })
    })

    this.addDependency(this.resourcePath)
    const file = (await readFile(this.resourcePath)).toString('utf8')
    content = transform(file, this.resourcePath, options, content);
  }

  this.callback(null, content, sourceMap)
}

module.exports.transform = transform;
module.exports.getComponents = components;