'use strict';
 
var list = require('select-shell')(
  /* possible configs */
  {
    pointer: ' ◉  ',
    pointerColor: 'blue',
    msgCancel: 'No selected options!',
    msgCancelColor: 'orange',
    multiSelect: false,
    inverse: true,
    prepend: true,
    disableInput: true
  }
);

var stream = process.stdin;

console.log('Please select the type of UI server to sert')
list.option(' App    ', 'src/phone')
    .option(' Web    ', 'src/browser')
    .option(' App + Web  ', 'wm')
    .list();

var timeout = setTimeout(() => {
    list.keypress(null, {
        name: 'return'
    });
}, 3000)

//----------------------------------------

var preference = {};
try {
    preference = require('./preferences.json');
    if(preference.start > 0) {
        for(var i=0; i<preference.start; i++)
            list.next();
    }
} catch(e) {}

//----------------------------------------

function start(mode) {
    const child_process = require('child_process');
    const path          = require('path');

    const subprocess = child_process.spawn('npm', ['start'], {
        stdio: 'inherit',
        cwd: path.join(__dirname, '..', '..', mode)
    });
}

list.on('select', function(options){
    clearTimeout(timeout);

    preference.start = ['src/phone', 'src/browser', 'wm'].indexOf(options[0].value);
    require('fs').writeFileSync(__dirname + '/preferences.json', JSON.stringify(preference, null, 2));
    
    if(options[0].value === 'wm') {
        start('src/browser')
        start('src/phone')
    } else {
        start(options[0].value)
    }
});
 
/*list.on('cancel', function(options){
  console.log('Cancel list, '+ options.length +' options selected');
  process.exit(0);
});*/