const fs = require('fs');
const Path = require('path');
const AppJson = require('./../phone/app.json');

try {
  const versionPath = Path.resolve(__dirname + '/../../ui/@config/version.native.json');
  const versionObj = require(versionPath);
  if (AppJson.expo.version === versionObj.version) {
    versionObj.release = ((parseInt(versionObj.release || 0, 10) || 0) + 1);
  } else {
    versionObj.release = 1;
  }
  versionObj.version = AppJson.expo.version;
  fs.writeFileSync(versionPath, JSON.stringify(versionObj, null, 4));
} catch (err) {
  console.error(err);
}