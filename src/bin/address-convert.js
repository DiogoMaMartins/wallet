const bs58check = require('bs58check');
      
const BTC_VER = 0;
const BTC_SCRIPT_VER = 5;

//Bitcoin gold
//const BTG_VER = 38;
//const BTG_SCRIPT_VER = 23;

//Bitcoin cash
const BTG_VER = 5;
const BTG_SCRIPT_VER = 23;

const EXPLORER = {
    0: {"name": "blockchain.info", "url": "https://blockchain.info/address/"},
    38: {"name": "btgexp.com", "url": "https://btgexp.com/address/"},
    5: {"name": "blockchain.info", "url": "https://blockchain.info/address/"},
    23: {"name": "btgexp.com", "url": "https://btgexp.com/address/"}
};

function tryConvertAddr() {
    var lastAddr = null;
    var lastAddrType = null;

    var convAddr = '12PbkM8DNNrZuNREjdDx9ERe5JtBeiiFbg';
    var decoded;

    try {
        decoded = bs58check.decode(convAddr);
    } catch (ex) {
        throw("Invalid checksum");
    }

    if (decoded.length != 21) {
        throw("Not an address (this tool does NOT accept private keys!)");
    }

    switch (decoded[0]) {
        case BTC_VER:
            decoded[0] = 0xBD;
            decoded = Buffer.concat([Buffer.from([0x1C]), decoded])
            console.log(decoded)
            lastAddr = bs58check.encode(decoded);
            lastAddrType = decoded[0];
            console.log("Bitcoin to Bitcoin Gold");
            return lastAddr
            break;
        case BTG_VER:
            decoded[0] = BTC_VER;
            lastAddr = bs58check.encode(decoded);
            lastAddrType = decoded[0];
            console.log("Bitcoin Gold to Bitcoin");
            return lastAddr
            break;
        case BTC_SCRIPT_VER:
            decoded[0] = BTG_SCRIPT_VER;
            lastAddr = bs58check.encode(decoded);
            lastAddrType = decoded[0];
            console.log("Bitcoin to Bitcoin Gold (Script)");
            return lastAddr
            break;
        case BTG_SCRIPT_VER:
            decoded[0] = BTC_SCRIPT_VER;
            lastAddr = bs58check.encode(decoded);
            lastAddrType = decoded[0];
            console.log("Bitcoin Gold to Bitcoin (Script)");
            return lastAddr
            break;

        default:
            return '/'
    }

}


const convert = tryConvertAddr();
console.log(convert)