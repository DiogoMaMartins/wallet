const path = require('path');
const chalk = require('chalk');
const localesPath = path.resolve(__dirname, '../../ui/@locales');
const shared = {
    fr: require(path.resolve(localesPath, 'shared/fr.json')),
    nl: require(path.resolve(localesPath, 'shared/nl.json')),
    en: require(path.resolve(localesPath, 'shared/en.json')),
}

const backend = {
    fr: require(path.resolve(localesPath, 'backend/fr.json')),
    nl: require(path.resolve(localesPath, 'backend/nl.json')),
    en: require(path.resolve(localesPath, 'backend/en.json')),
}

for(var key in backend.en) {
    if(!shared.en[key])
        continue;

    if(shared.en[key] === backend.en[key] && shared.fr[key] === backend.fr[key] && shared.nl[key] === backend.nl[key]) {
        console.log('duplicate key', chalk.bold(key), ', same data detected', chalk.green('removing backend key'))
        delete backend.en[key];
        delete backend.fr[key];
        delete backend.nl[key];
    } else {
        const msg = [
            (shared.en[key] !== backend.en[key]) ? chalk.bold.red('EN not same ' + shared.en[key] + ' => ' + backend.en[key]) : null,
            (shared.fr[key] !== backend.fr[key]) ? chalk.bold.red('FR not same ' + shared.fr[key] + ' => ' + backend.fr[key]) : null,
            (shared.nl[key] !== backend.nl[key]) ? chalk.bold.red('NL not same ' + shared.nl[key] + ' => ' + backend.nl[key]) : null,
        ]

        console.log('duplicate key', chalk.bold(key), msg.filter((o) => o).join(', '))
    }
}

const fs = require('fs');
fs.writeFileSync(path.resolve(localesPath, 'backend/fr.json'), JSON.stringify(backend.fr, null, 2));
fs.writeFileSync(path.resolve(localesPath, 'backend/nl.json'), JSON.stringify(backend.nl, null, 2));
fs.writeFileSync(path.resolve(localesPath, 'backend/en.json'), JSON.stringify(backend.en, null, 2));

console.log('---')
console.log('done')