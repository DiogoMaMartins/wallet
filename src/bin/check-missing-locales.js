const en = require('../../ui/@locales/en.json');
const nl = require('../../ui/@locales/nl.json');
const fr = require('../../ui/@locales/fr.json');

const missingNL = {};
const missingFR = {};
Object.keys(en).forEach((key) => {
  console.log(key, nl[key], fr[key]);
  if (typeof nl[key] === 'undefined' || !nl[key] || !nl[key].length) missingNL[key] = en[key];
  if (typeof fr[key] === 'undefined' || !fr[key] || !fr[key].length) missingFR[key] = en[key];
});

console.log('');
console.log('Missing NL:', missingNL);
console.log('Missing FR:', missingFR);
console.log('');