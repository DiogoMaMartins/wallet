const fs = require('fs');
const Path = require('path');

function getAddress() {
    // process.env['my-ip'] = '0.0.0.0';
    // process.env['platform-port'] = 8081;
    if (process.env['my-ip']) {
        const port = process.env['platform-port'] || 8080;
        return `http://${process.env['my-ip']}:${port}/`;
    }

    return 'https://www.bit4you.io/';
}

function getOath2Address() {
    // process.env['oauth2-ip'] = '0.0.0.0';
    if (process.env['oauth2-ip']) {
        const port = process.env['oauth2-port'] || 8085;
        return `http://${process.env['oauth2-ip']}:${port}/`;
    }

    return 'https://auth.bit4you.io/';
}

try {
    const configPath = Path.resolve(__dirname + '/../../ui/@config/server.json');
    const config = require(configPath);
    config.server = getAddress();
    config.oauth2.url = getOath2Address();

    //if (process.env['chartServer'])
    //    config.chartServer = process.env['chartServer'];

    fs.writeFileSync(configPath, JSON.stringify(config, null, 4));
} catch (err) {
    console.error(err);
}
