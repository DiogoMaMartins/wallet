/*const translate = require('node-google-translate-skidz');

if(process.argv.length < 3)
    throw('Language required');

const lang = process.argv[2];
console.log('Translating to:', lang);

var dictionary = {};
const fr = require(__dirname + '/../locales/fr.json');
try {
    dictionary = require(__dirname + '/../locales/' + lang + '.json');
} catch(e) {}

var ToDo = 0;

//----------------------------------------------------

function onDone() {
    const content = JSON.stringify(dictionary, null, 4);
    const fs = require('fs');
    // console.log(content);
    fs.writeFileSync(__dirname + '/../locales/' + lang + '.json', content);
}

function Translate(text) {
    ToDo++;
    
    const translationConfig = {
        text:   text,
        source: 'en',
        target: lang
    }

    translate(translationConfig, (result) => {
        var nText = '';

        result.sentences.forEach((obj) => {
            if(obj.orig && obj.trans)
                nText += obj.trans
        });

        dictionary[text] = {
            text: nText,
            auto: true,
            pages: []
        };

        ToDo--;
        if(ToDo <= 0)
            onDone();
    });
}

//----------------------------------------------------

for(var sentence in fr) {
    if(!dictionary[sentence]) 
        Translate(sentence);
}

onDone();*/