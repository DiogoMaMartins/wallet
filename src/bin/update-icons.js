const fs = require('fs');
const path = require('path');
const basePath = path.join(__dirname, '..', '..', 'assets', 'fonts', 'icons');
let css = fs.readFileSync(path.join(basePath, 'styles.css')).toString();

let index = 0;
const icons = {};

while((index = css.indexOf('.icon-', index)) !== -1) {
    const nameEndIndex = css.indexOf(':before {', index);
    const name = css.substr(index + 6, nameEndIndex - index - 6);
    
    const contentStart = css.indexOf('content: "', nameEndIndex) + 10;
    const contentEnd   = css.indexOf('"', contentStart);
    const content      = css.substr(contentStart, contentEnd - contentStart)
    
    index = contentEnd;
    icons[name] = content;
}

let json = JSON.stringify(icons, null, 4).split("\\\\").join("\\x");
fs.writeFileSync(path.join(__dirname, '..', '..', 'ui', '@config', 'icons.js'), 'export default Icons = ' + json);