const args = process.argv.slice(2);
if (!args || args.length < 3) {
  console.error('Please run this script with arguments: <USER_ID> <CURRENT_TX_COUNT> <AMOUNT_TO_GENERATE>');
  return;
}

const USER_ID = parseInt(args[0], 10);
const CURRENT_TX_COUNT = parseInt(args[1], 10);
const AMOUNT_TO_GENERATE = parseInt(args[2], 10);

const result = [];
for (txId = CURRENT_TX_COUNT; txId < Number(CURRENT_TX_COUNT + AMOUNT_TO_GENERATE); txId += 1) {
  var ref  = USER_ID.toString().padStart(7, '0').substr(-7) + txId.toString().padStart(3, '0').substr(-3);
  ref = ref + (parseInt(ref) % 97).toString().padStart(2, '0');
  ref = '+++ ' + ref.substr(0, 3) + ' / ' + ref.substr(3, 4) + ' / ' + ref.substr(7) + ' +++';
  result.push(ref);
}

console.log(result.join('\n'));

return result;