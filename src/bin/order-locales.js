const en = require('../../ui/@locales/en.json');
const nl = require('../../ui/@locales/nl.json');
const fr = require('../../ui/@locales/fr.json');

const fs = require('fs');
const path = require('path');
function sortObject(o) {
  return Object.keys(o).sort().reduce((r, k) => (r[k] = o[k], r), {});
}
fs.writeFileSync(path.resolve(__dirname, '../../ui/@locales/en.json'), JSON.stringify(sortObject(en), null, 4), { encoding: 'UTF-8' });
fs.writeFileSync(path.resolve(__dirname, '../../ui/@locales/nl.json'), JSON.stringify(sortObject(nl), null, 4), { encoding: 'UTF-8' });
fs.writeFileSync(path.resolve(__dirname, '../../ui/@locales/fr.json'), JSON.stringify(sortObject(fr), null, 4), { encoding: 'UTF-8' });
