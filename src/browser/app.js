import Vue from 'vue';
import '../addons';
import '../addons/browser';

import Analytics from '@addons/browser/analytics';
//import BrowserVersion from './plugins/browser-version';
//import './plugins/polyfills';
import router from '@addons/browser/router';
//import './registerServiceWorker';
import Meta from 'vue-meta';
import DefaultMenu from '@menu/backend';

const app = new Vue(Object.assign(DefaultMenu, {
    router,
    /*metaInfo() {
        const uris = this.$route.matched[0] ? (this.$route.matched[0].components.default.uri || {}) : {};
        const canonicalPath = (this.$route.matched[0] || {}).path === "*" ? '/error/404' : `${this.$route.meta.lang == 'en' ? '' : '/' + this.$route.meta.lang}/${uris[this.$route.meta.lang] || uris.en}`;

        const links = locales.map((lang) => {
            return {
                rel: 'alternate',
                href: `https://www.bit4you.io${lang === 'en' ? '' : '/' + lang}/${uris[lang] || uris.en}`,
                hreflang: lang,
            };
        });

        return {
            title: 'bit4you',
            htmlAttrs: {
                lang: this.$route.meta.lang,
            },
            link: links.concat([
                {
                    rel: 'canonical',
                    href: `https://www.bit4you.io${canonicalPath}`
                },
                {
                    href: '/opensearch.xml',
                    rel: 'search',
                    title: 'bit4you',
                    type: 'application/opensearchdescription+xml',
                }
            ]),
            meta: [{
                    property: 'og:type',
                    content: 'website'
                    },
                {
                    property: 'og:url',
                    name: 'og:url',
                    content: this.$route.fullPath,
                    },
                {
                    property: 'apple-itunes-app',
                    name: 'apple-itunes-app',
                    content: 'app-id=1444871913'
                    },
                {
                    name: 'google-play-app',
                    content: 'app-id=com.bit4you.app'
                    },
                {
                    property: 'twitter:url',
                    name: 'twitter:url',
                    content: this.$route.fullPath,
                    },
                {
                    property: 'twitter:site',
                    name: 'twitter:site',
                    content: 'bit4you.io'
                    },
                {
                    property: 'og:image',
                    name: 'og:image',
                    content: 'https://www.bit4you.io/img/manifest/logo-256.png'
                    },
                {
                    property: 'twitter:image',
                    name: 'twitter:image',
                    content: 'https://www.bit4you.io/img/manifest/logo-256.png'
                    },
                {
                    property: 'twitter:creator',
                    name: 'twitter:creator',
                    content: 'bit4you.io'
                    },
                {
                    property: 'twitter:card',
                    name: 'twitter:card',
                    content: 'summary'
                    },
                {
                    property: 'twitter:app:id:googleplay',
                    name: 'twitter:app:id:googleplay',
                    content: 'com.bit4you.app'
                    },
                {
                    property: 'twitter:app:id:ipad',
                    name: 'twitter:app:id:ipad',
                    content: '1444871913'
                    },
                {
                    property: 'twitter:app:id:iphone',
                    name: 'twitter:app:id:iphone',
                    content: '1444871913'
                    },
                {
                    property: 'twitter:app:name:googleplay',
                    name: 'twitter:app:name:googleplay',
                    content: 'bit4you crypto exchange'
                    },
                {
                    property: 'twitter:app:name:iphone',
                    name: 'twitter:app:name:iphone',
                    content: 'bit4you crypto exchange'
                    },
                {
                    property: 'twitter:app:name:ipad',
                    name: 'twitter:app:name:ipad',
                    content: 'bit4you crypto exchange'
                    }
                ]
        };
    },*/
}));

export default (context) => {
  app.$router.push(context.url)
  context.$router = app.$router
  context.meta = app.$meta() // and here
  return app
}

if(typeof(window) !== "undefined") {
  app.$mount('#app');
  Analytics(app.$router);
}

//BrowserVersion(Vue, app);