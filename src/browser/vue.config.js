const VueGlobalRegister  = require('vueglobalregistration-webpack-plugin');
//const VueSSRServerPlugin = require('vue-server-renderer/server-plugin');
//const VueSSRClientPlugin = require('vue-server-renderer/client-plugin');

const webpack         = require('webpack');
const path            = require('path');
const serversConfig   = require('../../ui/@config/server.json');
//const PurgecssPlugin  = require('purgecss-webpack-plugin')
//const glob            = require('glob-all')

const TARGET_NODE = process.env.WEBPACK_TARGET === 'node'
const target = TARGET_NODE ? 'server' : 'client';
const components = require('../../ui/@config/global-components')(['web']);

// vue.config.js
module.exports = {
  configureWebpack: {
    entry: __dirname + '/app.js',
    //target: TARGET_NODE ? 'node' : 'web',
    //node: TARGET_NODE ? false : undefined,
    //devtool: 'source-map',
    resolve: {
        extensions: ['.web.js'],
        alias: require('../../ui/@config/alias.js'),
    },
    plugins: [
      //TARGET_NODE ?
      //  new VueSSRServerPlugin({ filename: '.ssr/vue-ssr-server-bundle.json' }) :
      //  new VueSSRClientPlugin({ filename: '.ssr/vue-ssr-client-manifest.json' }),
        new VueGlobalRegister.DynamicPlugin(components),
        new webpack.ProvidePlugin({
            'Vue': 'vue'
        }),
        /*new webpack.EnvironmentPlugin({
            oauth2: {
                url: serversConfig.auth.url,
                client_id: serversConfig.auth.client_id
            }
        })*/
    ],
    module: {
      rules: [
        {
            resourceQuery: /blockType=route/,
            loader:  require.resolve('vueglobalregistration-webpack-plugin/dynamic-components/route-meta-script.js'),
        },
        {
          enforce: "pre",
          test: /router.js/,
          loader: VueGlobalRegister.Register({
            type: "routing",
            array: "routes",
            replace: "// <ROUTING>",
            lazyLoad: false,
            folder: components.folders.find((o) => o && o.routes).path,
            test: /index.vue/,
            rules: [
              {
                test: /@connected/,
                meta: {
                  connected: true
                }
			  }
		    ]
          })
	    },
      ]
    },
    /*output: {
      //libraryTarget: TARGET_NODE ? 'commonjs2' : undefined,
        chunkFilename: '[name].bundle.js',
    },
    optimization: {
      splitChunks: {
          cacheGroups: {
                vendors: false,
                default: {
                    minChunks: 2,
                    priority: -20,
                    reuseExistingChunk: true,
                    minSize: 15000,
                }
            },
            minSize: 15000,
            chunks: 'all'
      }
    },*/
  },
  devServer: {
    contentBase: path.resolve(__dirname, '../../assets/'),
    // disableHostCheck: true, // localtunnel / NGROK
    proxy: {
      '^/api': {
        // target: 'http://localhost:8082/',
        // target: 'http://localhost:8085/',
        // target: 'http://localhost:8080/',
        target: serversConfig.server,
        ws: false,
        changeOrigin: true
      },
      '^/socketapi': {
        // target: 'http://localhost:8082/',
        // target: 'http://localhost:8085/',
        // target: 'http://localhost:8080/',
        target: serversConfig.server,
        ws: true,
        changeOrigin: true
      },
    }
  },
  //transpileDependencies: [/[/\\]vuetify[/\\]/],
  chainWebpack: config => {
    //config.optimization.delete('splitChunks')
    //config.optimization.splitChunks(false)

    if (TARGET_NODE) {
      config.plugins.delete('hmr');
      config.optimization.splitChunks(false)

      config.plugin('define').tap(definitions => {
        definitions[0]['process.env'].VUE_ENV = '"server"';
        return definitions
      })
    }

    /*config.module
        .rule('vue')
        .use('vue-loader')
        .tap(options =>
            Object.assign(options, {
                optimizeSSR: false
            })
        )*/
  },
  indexPath: '.ssr/template.html',
  pwa: {
      name: 'bit4you',
      themeColor: '#095ce5',
      msTileColor: '#212121',
  }
}
