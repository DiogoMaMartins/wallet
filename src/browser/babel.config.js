// babel.config.js
module.exports = {
    presets: [
        [
            "@vue/app", {
                modules: "commonjs",
                "targets": {
                    "edge": "17",
                    "firefox": "58",
                    "chrome": "40",
                    "safari": "10.0",
                    "opera": "47"
                },
                "useBuiltIns": "entry"
            }
        ],
    ]
};
