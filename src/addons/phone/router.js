import { NavigationActions } from 'react-navigation';

class Router {

    install(Vue, options) {
        Vue.prototype.$router = this;
    }

    generateAction(route) {
      if (!route) return undefined;
      return NavigationActions.navigate({
          routeName: route.name,
          params: route.params || {},
          action: this.generateAction(route.subroute),
      })
    }

    push(route) {
        if(typeof(route) === 'string') {
            route = {
                name: route,
                params: {}
            }
        }

        this.navigation.dispatch(this.generateAction(route));
    }

    replace(route) {
      this.go(-1);
      this.push(route);
    }

    get navigation() {
        return this._nav;
    }

    go(n) {
        if(n !== -1)
            throw('Unsupported router go count: ' + n);

        this._nav.dispatch( NavigationActions.back() );
    }

}

module.exports = new Router();