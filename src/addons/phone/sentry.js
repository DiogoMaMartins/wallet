import { Linking, WebBrowser, deviceName, platform, sessionId } from 'expo';
import Sentry from 'sentry-expo';

if (process.env.NODE_ENV !== "development") {
  Sentry.config(
    'https://d1521a1c1e64499b91caa97bad39f9e0@sentry.io/1321153', {
      // ignoreModulesInclude: ["RNSentry"], // default: [] | Include modules that should be ignored too
      // ignoreModulesExclude: ["I18nManager"], // default: [] | Exclude is always stronger than include
    }
  ).install();

  /*Sentry.setExtraContext({
    react: true,
    ExpoSessionId: sessionId, 
    deviceName,
    platform,
  });*/
}

const getCircularReplacer = () => {
  const seen = new WeakSet();
  return (key, value) => {
    if (typeof value === "object" && value !== null) {
      if (seen.has(value)) {
        return;
      }
      seen.add(value);
    }
    return value;
  };
};

function stringify(circularReference) {
    return JSON.stringify(circularReference, getCircularReplacer());
}

const consoleError = console.error;
console.error = function () {
    const response = consoleError.apply(console, arguments);
    //if (process.env.NODE_ENV !== "development") {
        try {
            for(var arg of arguments) {
                if(arg && typeof(arg) === "object" && arg.stack && arg.message) {
                    Sentry.captureException(arg, {
                        level: 'error'
                    });
                } else {
                    Sentry.captureException(new Error(stringify(arg)), {
                        level: 'error'
                    });
                }
            }
        } catch(e) {
            consoleError(e)
            Sentry.captureException(e, {
                level: 'error'
            });
        }
    //}
    return response;
};