import './sentry';
import './components';
import './css';
import Vue from 'vue-native-core';
import { View } from 'react-native';
import { Svg } from 'expo';

Vue.prototype.$phone = true;

Vue.component('svg', Svg)
Object.keys(Svg).forEach(key => {
  Vue.component(String(key).toLowerCase(), Svg[key]);
});
