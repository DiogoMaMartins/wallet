import { StyleSheet } from 'react-native';

const props = [ 'width', 'height', 'padding', 'paddingTop', 'paddingBottom', 'paddingLeft', 'paddingRight', 'margin', 'marginTop', 'marginLeft', 'marginRight', 'marginBottom', 'height', 'fontSize', 'borderBottomWidth', 'borderRadius' ]

for(var prop of props) {
    StyleSheet.setStyleAttributePreprocessor(prop, (value) => {
        if(typeof(value) === 'string' && value.indexOf('px') !== -1) {
            const v2 = parseFloat(String(value).replace('px', ''));
            return isNaN(v2) ? value : v2;
        }

        return value;
    });
}

/*StyleSheet.setStyleAttributePreprocessor("shadowOffset", (value) => {
    if(typeof(value) === 'string') {
        const [ width, height ] = value.split(' ').map((o) => parseInt(o));
        return { width, height };
    }

    return value;
});*/