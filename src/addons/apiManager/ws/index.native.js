const WS = global.WebSocket;
const navigator = { userAgent: 'bit4you-mobile' };
global.WebSocket = class WebSocket extends global.WebSocket {
    constructor(uri, protocol, opts) {
        super(uri, protocol || '', opts || {
            headers: {
                'User-Agent': navigator.userAgent
            }
        })
    }
}

export default {
    join() {
        return fetch((require('@config/server.json')).server + 'api/ws_join', {
            method: 'POST',
            credentials: 'include',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'User-Agent': navigator.userAgent,
            },
            body: JSON.stringify({}),
            // json: true,
            cache: false,
        })
        .then(async (e) => {
            e = await (e.json && e.json()) || {};
            if (e.error) {
                console.error(e.error);
                throw 'Internal Server Error';
            }

            return e;
        }).catch((e) => {
            if(e.error)
                throw(e);

            throw({ error: e.message || e })
        })
    },
    get native() {
        return true
    }
}