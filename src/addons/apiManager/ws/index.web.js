import axios from 'axios';

function $parseAxiosError(err) {
    if(err && err.response && err.response.data) {
        var data = err.response.data;
        try {
            data = typeof(err.response.data) !== "object" ? JSON.parse(err.response.data) : err.response.data;
        } catch(e) {
            console.error('error with data', err.response.data, '=>', e);
        }

        throw({
            error: data ? (data.error || data.message || data) : data,
            status: err.response.status || err.status,
        })
    }

    throw({
        error: err.error || err.message || err,
        status: (err.response && err.response.status) || err.status,
    })
}

export function post(url, params) {
    return axios.post(url, params).catch($parseAxiosError); 
}

export default {
    join() {
        return post('/api/ws_join', {}).then(async (e) => {
            return e.data || e;
        });
    },
    get native() {
        return false
    }
}