export function createEvent(name) {
    var event;
    if (document.createEvent) {
        event = document.createEvent("HTMLEvents");
        event.initEvent(name, true, true);
    } else {
        event = document.createEventObject();
        event.eventType = name;
    }

    event.eventName = name;

    event.fireOn = function(elm) {
        if (document.createEvent)
            elm.dispatchEvent(event);
        else
            elm.fireEvent("on" + name, event);

        return event;
    }

    return event;
}