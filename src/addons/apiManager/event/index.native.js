class event {

    fireOn(elm) {
        
    }

    preventDefault() {
        this.__prevented = true;
    }

    get defaultPrevented() {
        return this.__prevented || false;
    }

}

export function createEvent(name) {
    return new event(name);
}