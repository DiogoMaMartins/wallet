import axios from 'axios';
import ws from "./ws";
import SockJS from "sockjs-client";
import ServerConfig from "@config/server.json";
import $storage from './storage';
import { createEvent } from './event';
import InitMixin from './mixin';
import PermissionsManager from './permissionsManager';
//import NavigatorService from "./../navigator-service";

class ApiManager {
    constructor() {
        this.isDev           = process.env.NODE_ENV === "development";
        this.idIncrementer   = 0;
        this.fetchingApis    = [];
        this.onOpenCallbacks = [];
        this.onApiCallbacks  = [];
        this.__cookies       = {};
        this.userConnection  = null;
        this.saltAsyncs      = {};
        this.permissionsManager = new PermissionsManager(this);

        //this.formPoster         = formPoster;
        this.socketConnected = false;
        this.socketLastMessage = 0;
        this.saltApiForcers  = {};
        this.__onDone        = [];

        //--------------------------------------

        if(!this.isClient)
            return

        //ToDo convert to sync model
        Promise.all([
            this.$storage.get('$lst'),
            $storage.setup()
        ]).then(([token]) => this.setup(token)).catch((err) => {
            console.error(err);
        });

        //this.debug();
    }

    get token() {
        return this.__token;
    }

    get isServer() {
        return (process.env.VUE_ENV === 'server');
    }

    get isClient() {
        return !this.isServer;
    }

    get isTrial() {
        if(!this.credentials)
            return false;

        return String(this.credentials.phone).indexOf('TRIAL-') === 0;
    }

    set token(val) {
        this.__token = val;
        if(this._onToken) {
            for(var cb of this._onToken) {
                cb(val);
            }
        }
    }

    onToken(cb) {
        this._onToken = this._onToken || [];
        this._onToken.push(cb);
    }

    setup(token) {
        if(token && typeof(token) === 'object' && token.exp > Date.now())
            this.token = token.token;

        const auth = $storage.get('auth') || {};
        this.credentials = {
            phone: auth.phone,
        };


        this.deleteOldCache();
        this.socketConnect();

        for (var key in this.__onDone)
            this.__onDone[key](this);

        this.__onDone = null;
    }

    install(Vue, options) {
        Vue.prototype.$api = this;
        this.permissionsManager.install(Vue, options);
        $storage.install(Vue, options);
        InitMixin(Vue, this);

        if(Vue.observable) {
            this.state = Vue.observable({
                isSimulation: false
            })
        } else {
            this.state = {
                isSimulation: false
            }
        }
    }

    debug() {
        this.__debug = {
            networkRPS: {
                apis: 0,
                bytes_in: 0,
                bytes_out: 0
            }
        };

        setInterval(() => {
            console.log(this.__debug.networkRPS);

            this.__debug.networkRPS = {
                apis: 0,
                bytes_in: 0,
                bytes_out: 0
            };
        }, 1000);
    }

    //--------------------------------------

    onLoaded(cb) {
        if (!cb) {
            return new Promise((resolve, reject) => {
                this.__onDone.push(resolve);
            });
        }

        if (this.__onDone === null) return cb(this);

        this.__onDone.push(cb);
    }

    socketConnect() {
        if (this.socket) {
            try {
                console.warn('Closing old socket');
                this.socket.close();
            } catch (e) {
                console.error(e);
            }
        }

        if(!this.token || !this._wsJoinToken) {
            return ws.join().then((res) => {
                this.token = res.session;
                this._wsJoinToken = res.token;
                this.socketConnect();
                this.deleteOldCache();
                this.$storage.put('$lst', { token: res.session, exp: res.exp });

                if(this.permissionsManager)
                    this.permissionsManager.deleteOldCache();
            }).catch((err) => {
                console.error(err);
                setTimeout(() => this.socketConnect(), 1000);
            });
        }

        var _this = this;
        const socket = new SockJS(this.base() + "socketapi?token=" + this._wsJoinToken, [ 'websocket' ]);
        this.socket = socket
        this.socket.onopen = function () {
            _this.socket = socket
            _this.socketConnected = true;
            _this.socketLastMessage = 0;
            _this.emitSocketOpen();
        };
        this.socket.onmessage = function (e) {
            _this.socketConnected = true;
            _this.socketLastMessage = Date.now();
            _this.emitSocketMessage(e);
        };
        this.socket.onclose = function () {
            _this.socketLastMessage = 0;
            _this.socketConnected = false;
            _this.emitSocketClose();
        };
    }

    base() {
        if(ws.native)
            return ServerConfig.server;
        if (this.base_url)
            return this.base_url;

        var base = window.api_url || location.href;
        var index = base.indexOf('//');

        if (index != -1) {
            index = base.indexOf('/', index + 3);
            if (index != -1) {
                this.base_url = base.substr(0, index + 1);
                return this.base_url;
            }
        }

        return '/';
    }

    //---------------------------------------------------------

    emitSocketOpen() {
        const callbacks = this.onOpenCallbacks;
        this.onOpenCallbacks = [];

        for (var key in callbacks) {
            try {
                if (callbacks[key].name.indexOf("updateSubscriptions") > -1)
                    callbacks[key]();
                else callbacks[key].call(this, this);
            } catch (e) {
                console.error(e);
            }
        }

        var count = 0;
        for (var key in this.saltApiForcers) {
            count++;
            this.saltApiForcers[key]();
        }
    }

    emitSocketClose() {
        this.socketLastMessage = 0;
        this.socketConnected = false;
        this.socket = false;
        this.fetchingApis = [];

        this.socketTimeout = setTimeout(() => {
            console.log("Socket closed, reconnecting");
            this.socketConnect();
        }, 500);
    }

    emitSocketMessage(msg) {
        try {
            if (msg.type === "message") {
                var data = msg.data;

                if (this.__debug) this.__debug.networkRPS.bytes_in += data.length;

                try {
                    data = JSON.parse(data);

                    if (data.cookies)
                        $storage.setCookies(data.cookies, data.expiration);

                    if (this.__debug && (data.api || data.apiError))
                        this.__debug.networkRPS.apis++;

                    if (data.api) {
                        this.emitApi(data.api, data.data, data.salt);
                    }  else if (data.error) {
                        if(data.reset || (data.error.reconnect && data.error.httpCode === 401) || data.error === 'Wrong credentials provided') {
                            delete this.token;
                            delete this._wsJoinToken;
                            delete this.__cookies.token;
                            delete this.__cookies.auth;
                            $storage.setCookies({});
                            data.httpCode = 401;
                            this.handleApiError(null, data, data.salt);
                        } else if(data.resetSession) {
                            delete this.token;
                            delete this._wsJoinToken;
                        } else if(data.apiError) {
                            this.handleApiError(data.apiError, data.error, data.salt);
                        } else {
                            console.error(data.error);
                        } 
                    } else if (data.apiError) {
                      this.handleApiError(data.apiError, data.error, data.salt);
                    } 
                } catch (e) {
                    console.error(e);
                }

                for (var key in this.socketHooks) {
                    try {
                        this.socketHooks[key](data);
                    } catch (e) {
                        console.error(e);
                    }
                }
            }
        } catch (e) {
            console.error(e);
        }
    }

    emitApi(api, data, salt) {
        this.saltAsyncs[salt] = true;

        setImmediate(() => {
            if (!this.saltAsyncs[salt])
                return;

            delete this.saltAsyncs[salt];

            //------

            $storage.putCache(salt, data);
            var found = false;

            if (salt === undefined) {
                salt = this.getSalt(api, {});
            }

            if (this.saltApiForcers[salt])
                delete this.saltApiForcers[salt];

            for (var key in this.onApiCallbacks) {
                try {
                    const post = JSON.parse(
                        JSON.stringify(this.onApiCallbacks[key].post || {})
                    );
                    const cbSalt =
                        this.onApiCallbacks[key].salt ||
                        this.getSalt(this.onApiCallbacks[key].api, post) ||
                        this.getSalt(this.onApiCallbacks[key].api, this.unmergePost(post));

                    if (cbSalt === salt) {
                        found = true;
                        if(!this.onApiCallbacks[key].callback.call) {
                            console.error(this.onApiCallbacks[key])
                            continue
                        }

                        this.onApiCallbacks[key].callback.call(
                            this,
                            data,
                            api,
                            this.onApiCallbacks[key].id
                        );
                    }
                } catch (e) {
                    console.error(e);
                }
            }

            if (!found && this.isDev)
                console.warn("Api handler not found for", api, salt);
        });
    }

    emit(api, data, salt) {
        setImmediate(() => {
            if (salt === undefined)
                salt = this.getSalt(api, {});

            for (var key in this.onApiCallbacks) {
                try {
                    const post = JSON.parse(
                        JSON.stringify(this.onApiCallbacks[key].post || {})
                    );
                    const cbSalt =
                        this.onApiCallbacks[key].salt ||
                        this.getSalt(this.onApiCallbacks[key].api, post) ||
                        this.getSalt(this.onApiCallbacks[key].api, this.unmergePost(post));

                    if (cbSalt === salt) {
                        if(!this.onApiCallbacks[key].callback.call) {
                            console.error(this.onApiCallbacks[key])
                            continue
                        }

                        this.onApiCallbacks[key].callback.call(
                            this,
                            data,
                            api,
                            this.onApiCallbacks[key].id
                        );
                    }
                } catch (e) {
                    console.error(e);
                }
            }
        });
    }

    sendSocketMessage(msg) {
        if (!this.isClient)
            return null;

        if (typeof msg === "object") {
            try {
                msg = JSON.stringify(msg);
            } catch (e) {
                console.error(e);
            }
        }

        function send() {
            if (this.__debug) this.__debug.networkRPS.bytes_out += msg.length;
            if(!this.socket)
                console.error("Sending message while no socket is available");

            this.socket && this.socket.send(msg);
        }

        if (this.socketConnected === false)
            return this.onSocketOpen(send.bind(this));

        send.apply(this);
        return this;
    }

    onSocketOpen(cb) {
        if (this.socketConnected) {
            try {
                cb.call(this);
            } catch (e) {
                console.error(e);
            }
        } else {
            this.onOpenCallbacks.push(cb);
        }

        return this;
    }

    //---------------------------------------------------------

    sendApiAndForceResponse(api, data, salt) {
        const _this = this;

        this.saltApiForcers[salt] = function () {
            _this.sendSocketMessage({
                api: api,
                data: _this.mergePost(data),
                salt: salt
            });
        };

        if (!this.socketConnected)
            return;

        this.saltApiForcers[salt]();
    }

    //---------------------------------------------------------

    get isSimulation() {
        const val = (() => {
            if(this.isTrial)
                return true;

            const cookie = $storage.get("virtual");
            return cookie === null || cookie === true || Boolean(parseInt(cookie, 10));
        })();

        if(val !== this.state.isSimulation) {
            this.state.isSimulation = val;
            this.emit('mode-switch', {
                isSimulation: this.state.isSimulation,
            });
        }

        return this.state.isSimulation;
    }

    set isSimulation(val) {
        this.state.isSimulation = val;
        this.$storage.put('virtual', val ? 1 : 0);

        this.emit('mode-switch', {
            isSimulation: val
        });
    }

    deleteOldCache() {
        $storage.removeNonTokenCache(this.token);
        $storage.removeNonTokenStorage(this.token).catch(console.error);
    }

    removeCache(api, data) {
        const salt = this.getSalt(api, data || {});
        $storage.removeCache(salt);
    }

    get $storage() {
        return $storage;
    }

    setCookies(cookies, exp, cb) {
        return $storage.setCookies(cookies, exp, cb);
    }

    //---------------------------------------------------------

    onAccessDenied(error, api) {
        var event = createEvent('access-denied');
        event.error = error;

        event.logout = () => {
            try {
                this.$storage.deleteAllCache();

                if(this.permissionsManager)
                    this.permissionsManager.setPermissions([]);

                this.logout();
            } catch(e) {
                console.error(e);
            }
        }

        for (var cb of this.onApiCallbacks) {
            try {
                if(cb.api !== 'access-denied')
                    continue;

                cb.callback.call(this, event, api);
            } catch (e)
            {
                console.error(e);
            }
        }

        if(!event.defaultPrevented && error.reconnect)
            event.logout();
    }

    handleApiError(api, error, salt) {
        if (error.httpCode === 401)
            this.onAccessDenied(error.error, api);

        for (var i = this.fetchingApis.length - 1; i >= 0; i--) {
            if (this.fetchingApis[i].salt === salt) {
                this.fetchingApis.splice(i, 1);
            }
        }

        var found = false;
        for (var key in this.onApiCallbacks) {
            try {
                const post = this.mergePost(JSON.parse(JSON.stringify(this.onApiCallbacks[key].post)), api);
                const cbSalt =
                    this.onApiCallbacks[key].salt ||
                    this.getSalt(this.onApiCallbacks[key].api, post);

                if (this.onApiCallbacks[key].errorCallback && cbSalt === salt) {
                    found = true;
                    this.onApiCallbacks[key].errorCallback.call(this, error, api, this.onApiCallbacks[key].id);
                }
            } catch (e) {
                console.error(e);
            }
        }

        if (!found && this.isDev && error.httpCode !== 401)
            console.error("Api Error", api, error);
    }

    getSalt(api, data = {}) {
        data = this.unmergePost(data);
        var dataJ = JSON.stringify(data || {});
        if (dataJ.length == 0) return api;

        var char,
            hash = 0;

        for (var i = 0; i < dataJ.length; i++) {
            char = dataJ.charCodeAt(i);
            hash = (hash << 5) - hash + char;
            hash = hash & hash;
        }

        return api + "_" + Math.abs(hash);
    }

    isFetching(salt) {
        var timeout = Date.now() - 15 * 1000; //15 Secondes

        for (var i = this.fetchingApis.length - 1; i >= 0; i--) {
            if (this.fetchingApis[i].time <= timeout) {
                this.fetchingApis.splice(i, 1);
            } else if (this.fetchingApis[i].salt === salt) {
                return true;
            }
        }

        return false;
    }

    setFetching(salt) {
        this.fetchingApis.push({
            salt: salt,
            time: Date.now()
        });
    }

    //---------------------------------------------------------

    logout() {
        this.userConnection = null;
        this.fetchingApis = [];

        this.permissionsManager.setPermissions([]);
        this.emitApi("logout", {});

        this.setCookies({
            auth: {
                connected: false
            }
        });

        //Delete all local caches of this token
        if (!this.isClient) return null;

        try {
            //ToDo delete all cache from $storage ..
            /*if (typeof localStorage == "object") {
                for (var i = localStorage.length - 1; i >= 0; i--) {
                    var key = localStorage.key(i);
                    var index = key.indexOf("_");

                    if (key.substr(0, index) === this.token) {
                        localStorage.removeItem(key);
                    }
                }
            }*/
        } catch (e) {
            console.error(e);
        }

        this.sendSocketMessage({
            event: "logout"
        });
    }

    setCredentials(credentials) {
        return this.permissionsManager.setPermissions(credentials);
    }

    isConnected() {
        return this.permissionsManager.connected();
    }

    //---------------------------------------------------------

    mergePost(data) {
        data = data || {};
        data.simulation = this.isSimulation;
        return data;
    }

    unmergePost(data) {
        data = data || {};
        delete data.simulation;
        return data;
    }

    on(api, data, cb, salt) {
        if (typeof data === "function" || data === undefined || data === null) {
            cb = data;
            data = {};
        }

        if (!salt) {
            salt = this.getSalt(api, data);
        }

        this.idIncrementer++;
        var id = this.idIncrementer;

        this.onApiCallbacks.push({
            id: id,
            api: api,
            post: data,
            salt: salt,
            callback: cb
        });

        if (this.idIncrementer > 1000000) this.idIncrementer = 0;

        const cache = $storage.getCache(salt);
        if (cache !== null) {
            setImmediate(() => {
                try {
                    cb.call(this, cache, api, id);
                } catch (e) {
                    console.error(e);
                }
            });
        }

        return id;
    }

    require(api, data, cb) {
        if (typeof data === "function" || data === undefined || data === null) {
            cb = data;
            data = {};
        }

        var id = 0;
        const salt = this.getSalt(api, data);

        if (cb)
            id = this.on(api, data, cb, salt);

        if (this.isFetching(salt)) {
            return; //ToDo call function to check if it is a fake (live) api
        }

        this.setFetching(salt);
        this.sendApiAndForceResponse(api, data, salt);

        return id;
    }

    refresh(api, data) {
        if (typeof data === "function" || data === undefined || data === null) {
            data = {};
        }

        this.sendApiAndForceResponse(api, data, this.getSalt(api, data));
    }

    //----------------

    $parseAxiosError(err) {
        if(err.status === 401 && (err.response && err.response.status === 401))
            this.onAccessDenied({ error: err }, api);

        if(err && err.response && err.response.data) {
            var data = err.response.data;
            try {
                data = typeof(err.response.data) !== "object" ? JSON.parse(err.response.data) : err.response.data;
            } catch(e) {
                console.error('error with data', err.response.data, '=>', e);
            }

            throw({
                error: data ? (data.error || data.message || data) : data,
                status: err.response.status || err.status,
            })
        }

        throw({
            error: err.error || err.message || err,
            status: (err.response && err.response.status) || err.status,
        })
    }

    static $parseAxiosRes(res) {
        return res.data;
    }

    parseApiUrl(path) {
        if(path.indexOf('://') !== -1)
            return path;
        
        if(path.substr(0, 1) === '/')
            return this.base() + path.substr(1);

        return this.base() + 'api/' + path;
    }
    
    post(api, params) {
        if(api === 'subscribe')
            return this.wsPost(api, params);

        const uri = this.parseApiUrl(api);
        return axios.post(uri, this.mergePost(params)).then(ApiManager.$parseAxiosRes).catch(this.$parseAxiosError.bind(this));
    }

    get(api, params) {
        const uri = this.parseApiUrl(api);
        return axios.get(uri, this.mergePost(params)).then(ApiManager.$parseAxiosRes).catch(this.$parseAxiosError.bind(this));
    }

    wsPost(api, data) {
        this.idIncrementer++;
        data = data || {};

        const _this = this;
        const id = this.idIncrementer;
        const salt = this.getSalt(api, data);

        return new Promise(function (resolve, reject) {
            _this.onApiCallbacks.push({
                id: id,
                api: api,
                post: data,
                salt: salt,
                callback: function (res) {
                    _this.unbindListener(id);
                    resolve(res);
                },
                errorCallback: function (res) {
                    _this.unbindListener(id);
                    reject(res);
                }
            });

            _this.sendSocketMessage({
                api: api,
                data: _this.mergePost(data),
                salt
            });
        });
    }

    //----------------

    unbindListener(id) {
        if (!this.onApiCallbacks) return;

        for (var i = this.onApiCallbacks.length - 1; i >= 0; i--) {
            if (this.onApiCallbacks[i].id === id) {
                this.onApiCallbacks.splice(i, 1);
            }
        }
    }
}

if(typeof(setImmediate) === "undefined") {
    global.setImmediate = function(cb) {
        return setTimeout(cb, 1);
    }
}

export default new ApiManager();