function vueMerge(Vue, vueObject, nData) {
    try
    {
        if(nData instanceof Array)
            nData = { items: nData };
        if(nData.push) //SSR
            nData = { items: nData };

        //Analyse simple properties
        for(var key in nData)
        {
            if(vueObject[key] != nData[key] && vueObject[key] !== undefined)
            {
                Vue.set(vueObject, key, nData[key]);
            }
        }

        if(nData.items instanceof Array)
        {
            if(nData.items.length === 1 && !Array.isArray(vueObject.items))
            {
                nData = nData.items[0];

                for(var key in nData)
                {
                    if(vueObject[key] != nData[key] && vueObject[key] !== undefined)
                    {
                        Vue.set(vueObject, key, nData[key]);
                    }
                }

                if(vueObject.info instanceof Object)
                {
                    vueObject.info = nData;
                }
            }
        }

        vueObject.$forceUpdate();
    }
    catch(e)
    {
        console.error(e);
    }

    return vueObject;
}

export default function init(Vue, $api) {
    Vue.mixin({

        created() {
            this.$intervals = [];
            this.$boundApis = [];

            if(this.$options.api) {
                if(Array.isArray(this.$options.api)) {
                    for(var api of this.$options.api) {
                        if(typeof(api) === 'string')
                            this.$mainApiListenerId = this.$bindApi(api);
                        else if(typeof(api) === 'object')
                            this.$mainApiListenerId = this.$bindApi(api.name, api.data);
                    }
                } else if(typeof(this.$options.api) === "object") {
                    this.$mainApiListenerId = this.$bindApi(this.$options.api.name, this.$options.api.data);
                } else if(typeof(this.$options.api) === 'string') {
                    this.$mainApiListenerId = this.$bindApi(this.$options.api);
                }
            }

            if(this.$options.internalApi) {
                if(Array.isArray(this.$options.internalApi)) {
                    for(var api of this.$options.internalApi) {
                        if(typeof(api) === 'string')
                            this.$mainApiListenerId = this.$bindApi(api, {}, false);
                        else if(typeof(api) === 'object')
                            this.$mainApiListenerId = this.$bindApi(api.name, api.data, false);
                    }
                } else if(typeof(this.$options.internalApi) === "object") {
                    this.$mainApiListenerId = this.$bindApi(this.$options.internalApi.name, this.$options.internalApi.data, false);
                } else if(typeof(this.$options.internalApi) === 'string') {
                    this.$mainApiListenerId = this.$bindApi(this.$options.internalApi, {}, false);
                }
            }

            if(this.api)
                this.$mainApiListenerId = this.$bindApi(this.api, this.api_data);
        },

        destroyed() {
            if(this.$boundApis) {
                this.$boundApis.forEach((id) => {
                    this.$api.unbindListener(id);
                });
            }

            if(this.$intervals) {
                this.$intervals.forEach(function(id) {
                  clearInterval(id);
                  clearTimeout(id);
                });
            }
        },

        methods: {
            $bindApi(api, data, require = true) {
                const id = this.$api[ require ? 'require' : 'on'](api, data || {}, (data) => {
                    vueMerge(Vue, this, data);
                });

                this.$boundApis.push(id);
                return id;
            },

            $replaceApi(api, data) {
                if(this.$mainApiListenerId) {
                    this.$boundApis = this.$boundApis.filter((o) => o !== this.$mainApiListenerId);
                    this.$api.unbindListener(this.$mainApiListenerId);
                }

                this.$mainApiListenerId = this.$bindApi(api, data);
            },

            $onApi(api, data, cb) {
                if (typeof (data) === 'function' || data === undefined || data === null) {
                    cb = data;
                    data = {};
                }

                const id = this.$api.on(api, data, cb);
                this.$boundApis.push(id);
                return id;
            },

            $requireApi(api, data, cb) {
                if (typeof (data) === 'function' || data === undefined || data === null) {
                    cb = data;
                    data = {};
                }

                const id = this.$api.require(api, data, cb);
                this.$boundApis.push(id);
                return id;
            },

            $refreshApi(api, post) {
                var postIn = post;

                if(!api) 
                    postIn || this.api_data;

                if(!api && !this.api)
                    return;

                this.$api.refresh(api || this.api, postIn || {});
            },

            $setInterval(fn, time) {
                const id = setInterval(fn, time);
                this.$intervals.push(id);
                return id;
            },

            $setTimeout(fn, time) {
                const id = setTimeout(fn, time);
                this.$intervals.push(id);
                return id;
            }
        },

        watch: {
            api(val, oldVal) {
                if(val === oldVal)
                    return;

                this.$nextTick(() => {
                    this.$replaceApi(this.api, this.api_data);
                });
            },

            api_data(val, oldVal) {
                if(val === oldVal)
                    return;

                this.$nextTick(() => {
                    this.$replaceApi(this.api, this.api_data);
                });
            }
        }
    });
    
}