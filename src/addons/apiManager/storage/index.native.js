import { AsyncStorage } from "react-native";

class StorageManager {

    constructor() {
        this.dbStore = {};
        this.dbCache = {};
        this.__cookies = {};
    }

    async setup() {
        const res = await AsyncStorage.getItem("@BIT4YOU:cookies")
        this.__cookies = res || this.__cookies;
        this.dbStore = await AsyncStorage.getItem("@BIT4YOU:storage") || {}

        try {
            if (typeof this.__cookies === "string")
                this.__cookies = JSON.parse(this.__cookies);
        } catch (e) {
            console.error(e);
        };

        try {
            if (typeof this.dbStore === "string")
                this.dbStore = JSON.parse(this.dbStore);
        } catch (e) {
            console.error(e);
        };

        this.onReady();
    }

    onReady(cb) {
        if(cb) {
            if(this.ready)
                return cb();

            this._onReady = this._onReady || [];
            this._onReady.push(cb);
        } else {
            this.ready = true;
            const cbs = this._onReady || [];
            delete this._onReady;
            for(var cb of cbs)
                cb();
        }
    }

    install(Vue, options) {
        Vue.prototype.$storage = this;
    }

    get isClient() {
        return true;
    }

    //-----

    hexEncode(str) {
        var hex, i;
    
        var result = "";
        for (i=0; i<str.length; i++) {
            hex = str.charCodeAt(i).toString(16);
            result += ("000"+hex).slice(-4);
        }
        return result
    }

    hexDecode(str) {
        var j;
        var hexes = str.match(/.{1,4}/g) || [];
        var back = "";
        for(j = 0; j<hexes.length; j++) {
            back += String.fromCharCode(parseInt(hexes[j], 16));
        }
        return back;
    }

    getCookie(name) {
        return this.__cookies[name];
    }

    setCookies(cookies, expiration, cb) {
        for (var key in cookies) {
            //ToDo check for expiration time
            this.__cookies[key] = cookies[key];
        }

        setImmediate(() => {
            AsyncStorage.setItem(
                "@BIT4YOU:cookies",
                JSON.stringify(this.__cookies || {})
            ).then(() => {
              if (typeof cb === 'function') cb(this.__cookies);
            }).catch(function (err) {
                console.error(err);
            });
        });
    }

    //-----

    put(key, val) {
        try {
            if(val === this.dbStore[key])
                return

            this.dbStore[key] = val;

            setImmediate(() => {
                AsyncStorage.setItem(
                    "@BIT4YOU:storage",
                    JSON.stringify(this.dbStore || {})
                ).catch(function (err) {
                    console.error(err);
                });
            });
        } catch (e) {
            console.error(e);
        }
    }

    get(key) {
        return this.dbStore[key] === undefined ? null : this.dbStore[key];
    }

    async removeNonTokenStorage(token) {
        try {
            for(var key in this.dbStore) {
                var index = key.indexOf("_");

                if (index !== -1 && key.substr(0, index) !== token)
                    delete this.dbStore[key];
            }
        } catch (e) {
            console.error(e);
        }
    }

    //-----

    deleteAllCache() {
        try {
            this.dbCache = {};
        } catch (e) {
            console.error(e);
        }
    }

    putCache(key, val) {
        this.dbCache[key] = val;
    }

    getCache(key) {
        return this.dbCache[key] || null;
    }

    removeCache(key) {
        if(this.dbCache[key])
            delete this.dbCache[key]
    }

    removeNonTokenCache(token) {
        try {
            for(var key in this.dbCache) {
                var index = key.indexOf("_");

                if (key.substr(0, index) !== token)
                    delete this.dbCache[key];
            }
        } catch (e) {
            console.error(e);
        }
    }
}

export default new StorageManager()