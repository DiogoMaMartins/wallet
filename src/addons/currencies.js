var $database, $root;
var currencies = {};
var mainCurrency = 'USD';

function getDefaultPrecision(symbol) {
    return currencies[symbol || mainCurrency] ? currencies[symbol || mainCurrency].decimal_digits : 2;
}

function formatCurrency(val, precission, symbol, convertValue = true) {
    symbol = (symbol === false || !currencies[symbol || mainCurrency] ? '' : currencies[symbol || mainCurrency].symbol + ' ');
    
    if(precission === undefined)
        precission = getDefaultPrecision(symbol);
    else if(typeof(precission) !== typeof(1))
        precission = parseInt(($database.getMarket(precission) || { precision: 2 }).precision, 10);
    
    //---------------------------------------

    if(convertValue && currencies[mainCurrency])
        val = val * (currencies[mainCurrency].rate || 0);
    else if(convertValue && mainCurrency !== 'USD')
        val = 0

    //---------------------------------------
        
    var c = isNaN(c = Math.abs(precission)) ? 2 : precission,
        d = ".",
        t = " ",
        s = val < 0 ? "-" : "",
        i = String(parseInt(val = Math.abs(Number(val) || 0).toFixed(c))),
        j = (j = i.length) > 3 ? j % 3 : 0;
    return symbol + ' ' + s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(val - i).toFixed(c).slice(2) : "");
}

export default {
    filters: {
        formatPercentage: function (n, precission = 2) {
            return formatCurrency(n, precission, false, false);
        },

        formatCurrency: function (n, precission = null, symbol = false) {
            if(precission === undefined || precission === null)
                precission = getDefaultPrecision(symbol);
            return formatCurrency(n, precission, symbol);
        },

        currencyWithSymbol: function(n, p, c) {
            return formatCurrency.call(this, n, p === undefined ? getDefaultPrecision(c) : p, c);
        },

        round: function(n, p) {
            const pow = Math.pow(10, p || 8);
            return Math.round(n * pow) / pow;
        },
    },
    
    computed: {
        $currencies() {
            return currencies;
        }
    },
    
    methods: {
        $formatCurrency(n, precission = null, symbol = false) {
            if(precission === undefined || precission === null)
                precission = getDefaultPrecision(symbol);
            return formatCurrency(n, precission, symbol);
        },

        $getMarketPrecision(market) {
            return parseInt(($database.getMarket(market) || { precision: 2 }).precision, 10);
        },

        $setCurrenciesRoot(root) {
            $root = root;
        },

        $convertRate(val, iso = null) {
            if(currencies[iso || mainCurrency])
                return val * (currencies[iso || mainCurrency].rate || 0);

            return 0;
        },

        $getCurrency(name = null) {
            if(!name)
                name = mainCurrency;

            return {
                name: name,
                symbol: (currencies[name] || {}).symbol,
                info: currencies[name] || {}
            }
        },

        $getCurrencies() {
            return currencies;
        },

        $setCurrency(currency) {
            mainCurrency = currency;
            this.$root.$forceUpdate();
        }
    },

    install(Vue, options) {
        Vue.mixin(this);

        $database = options.$db;

        options.$api.require('platform/currencies', {}, (res) => {
            mainCurrency = res.main;
            currencies   = res.currencies;

            if($root)
                $root.$forceUpdate();
        });
    }
}

//-------------------------