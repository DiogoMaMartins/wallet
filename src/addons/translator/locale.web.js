export function getDeviceLang(locales) {
    var userLocales = locales;
    if (typeof (navigator) !== "undefined") {
        userLocales = (navigator.language || navigator.userLanguage ?
            [navigator.language || navigator.userLanguage] :
            navigator.languages || locales
        );
    }

    for(var [lang, country] of userLocales) {
        if(locales.indexOf(lang) !== -1)
            return lang;
    }

    return locales[0];
}
