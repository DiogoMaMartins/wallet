import { Localization } from 'expo-localization';

export function getDeviceLang() {
    return Localization.locale;
}