import $storage from '../apiManager/storage';
import { getDeviceLang } from './locale';

const $locales = {
    fr: require('@locales/shared/fr.json'),
    nl: require('@locales/shared/nl.json'),
    en: require('@locales/shared/en.json'),
    // de: require('./locales/de.json'),
    // es: require('./locales/es.json'),
    // it: require('./locales/it.json'),
}

class Translator {

    constructor(lang) {
        this.dictionary = {};
        this.__lang = {};

        this.list = {
            //En as first key to force it as the primary lang
            en: {
                name: 'English',
                flag: 'GB'
            },
            fr: {
                name: 'Francais',
                flag: 'FR'
            },
            nl: {
                name: 'Nederlands',
                flag: 'NL'
            },
        }

        $storage.onReady(() => {
            if (!!lang) {
                this.lang = lang;
            } else {
                this.lang = this.loadLang();
            }
        });
    }

    install(Vue, options) {
        Vue.prototype.$t          = this.$t.bind(this);
        Vue.prototype.$translate  = this.$t.bind(this);
        Vue.prototype.$translator = this;
        if(Vue.observable) {
          this.__lang = Vue.observable(Object.assign({}, this.__lang));
        }
    }

    get locales() {
        return Object.keys(this.list);
    }

    get lang() {
        return this.__lang.lang;
    }

    set lang(lang) {
        lang = lang.toLowerCase();
        this.__lang.lang = lang;

        $storage.put('lang', lang);
        this.dictionary = {};

        try {
            this.dictionary = $locales[lang] || {};
        } catch (e) {
            console.error(e)
        }

        if(this._onLang) {
            for(var cb of this._onLang) {
                try {
                    cb(lang);
                } catch(e) {
                    console.error(e);
                }
            }
        }
    }

    onLangChange(cb) {
        this._onLang = this._onLang || [];
        this._onLang.push(cb);
    }

    removeLangChangeListener(cb) {
        this._onLang = (this._onLang || []).filter((o) => o !== cb);
    }

    $t(text, params) {
        return this.$(text, params);
    }

    $(text, params, tried = 0) {
        if (!this.lang && (tried < 100)) {
            return this.$(text, params, (tried + 1));
        }
        if (this.lang && Object.keys(this.dictionary).length === 0) {
            this.lang = this.lang || this.locales[0];
        }
        let result = (this.dictionary[text] || text);
        if (params && typeof params === 'object') {
            Object.entries(params).forEach(([key, val]) => {
                result = result.split(`%%${String(key).toUpperCase()}%%`).join(val).split(`{@${String(key)}}`).join(val);
            });
        }
        return result;
    }

    //---------

    get preferredLang() {
        return getDeviceLang(this.locales);
    }

    loadLang() {
      if(this.__lang && this.__lang.lang)
          return this.__lang.lang;

      const locale = (getDeviceLang(this.locales) || this.locales[0]).toLowerCase().split('-');
      let lang = locale.find((o) => this.locales.indexOf(o) !== -1) || this.locales[0];
      this.__lang.lang = lang;

      const l = $storage.get("lang");
      if(l)
          this.__lang.lang = l;

      return this.__lang.lang;
  }

    extends(langs) {
        for(var lang in langs) {
            const dictionary = langs[lang];
            $locales[lang] = $locales[lang] || {};
            for(var key in dictionary) {
                if($locales[lang][key])
                    console.warn('Duplicate `' + key + '` locale found in extended', lang, 'locales file');
                else
                    $locales[lang][key] = dictionary[key];
            }
        }
    }
}

const translator = new Translator();
export default translator;
export const locales = Object.keys($locales);