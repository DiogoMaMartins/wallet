function serializeArray(form) {  
  var objects = [];  
  if (typeof form == 'object' && String(form.nodeName).toLowerCase() == "form") {  
    var fields = form.getElementsByTagName("input");  
    for (var i=0;i<fields.length;i++) {
      let value = fields[i].getAttribute("value");
      if (!value && typeof fields[i].value !== 'undefined') {
        value = fields[i].value;
      }
      objects[objects.length] = { name: fields[i].getAttribute("name"), value };  
    }
    var textfields = form.getElementsByTagName("textarea");
    for (var t=0;t<textfields.length;t++) {
      let value = textfields[t].getAttribute("value");
      if (!value && typeof textfields[t].value !== 'undefined') {
        value = textfields[t].value;
      }
      objects[objects.length] = { name: textfields[t].getAttribute("name"), value };  
    }
  }
  return objects;
};

class FormPoster {
    constructor() {
        const _this = this;
        this.events = {
            'api-progress': this.handleApiProgress,
            'api-prepare':  this.handleApiPrepare,
            'api-error':    this.handleApiError,
            'api-success':  this.handleApiSuccess,
            'api-done':     this.handleApiDone
        };

        if(typeof(document) === 'undefined')
            return;
        
        document.addEventListener('submit', (e) => {
            const elm = e.target;
            if(!elm || (elm.tagName && String(elm.tagName).toLowerCase() !== 'form') || !elm.getAttribute('action') || e.defaultPrevented)
                return;

            e.preventDefault()
            this.handleFormSubmit(elm);
        })
    }
    
    notify(type, message) {
      this.$api.emitApi('new-notification', {
        type,
        message,
      });
    }

    handleApiError(form, data) {
        var msg = "An error occured while processing your request";
        const err = data.error;

        if(typeof(err) == 'object' || typeof(err) == "array")
        {
            if(typeof(err.error) == 'string')
                msg = err.error;
        }
        else if(typeof(err) == "string")
            msg = err;

        this.notify('danger', msg);
    }

    handleApiSuccess(form, data) {
        const msg = data.result;

        if(form.hasAttribute('SuccessMessage'))
           return this.notify('success', form.getAttribute('SuccessMessage'));

        if(typeof(msg) == 'string')
            return this.notify('success', msg);

        if(typeof(msg) == 'object')
        {
            if(typeof(msg.result) == 'string')
                return this.notify('success', msg.result);
        }

        return this.notify('success', (msg && msg.message) || 'Data successfully updated!');
    }

    handleApiPrepare(form, data) {
        if(typeof($) !== 'undefined')
            $(form).find('input, textarea, select, button').not(':disabled').attr('disabled', true).attr('predisabled', true);

        //ToDo: set loader icon
    }

    handleApiProgress(form, data) {
        //ToDo set progress in button
    }

    handleApiDone(form) {
        if(typeof($) !== 'undefined')
            $(form).find('[predisabled]').removeAttr('disabled').removeAttr('predisabled');
    }

    trigger(elm, eventName, data) {
        try
        {
            const secondName = eventName.replace(/-\S*/g, function(txt){ return txt.charAt(1).toUpperCase() + txt.substr(2).toLowerCase();});

            if(secondName !== eventName)
            {
                if(!this.trigger(elm, secondName, data))
                    return;
            }

            //------------------------------------------------------------------------

            var event; // The custom event that will be created

            if (document.createEvent) {
                event = document.createEvent("HTMLEvents");
                event.initEvent(eventName, true, true);
            } else {
                event = document.createEventObject();
                event.eventType = eventName;
            }

            event.eventName = eventName;

            for(var key in data)
                event[key] = data[key];

            if (document.createEvent)
                elm.dispatchEvent(event);
            else
                elm.fireEvent("on" + event.eventType, event);

            if(event.defaultPrevented)
                return false;
        }
        catch(e) {
            console.error(e);
        }

        try
        {
            if(this.events[eventName])
                this.events[eventName].call(this, elm, data);
        }
        catch(e)
        {
            console.error(e);
        }

        return true;
    }

    handleFormSubmit(__elm)
    {
        var elm = __elm;
        if (Array.isArray(elm)) elm = __elm[0];
        const _this = this;
        const api   = elm.getAttribute('action');
        var data = {};

        this.$api.emitApi('form-submit', true);
        this.trigger(elm, 'api-before-submit', { api: api });
        
        if(elm.querySelector('input[type="file"]:not([disabled])')) {
            elm.setAttribute && elm.setAttribute('enctype', 'multipart/form-data');
            elm.attr && elm.attr('enctype', 'multipart/form-data');
            data = new FormData(elm);
        }
        else {
          elm.setAttribute && elm.setAttribute('enctype', 'application/json');
          elm.attr && elm.attr('enctype', 'application/json');
          data = serializeArray(elm);
          var o = {};
          for (var k in data) {
            const d = data[k];
            if (o[d.name] !== undefined) {
                if (!o[d.name].push) {
                    o[d.name] = [o[d.name]];
                }
                o[d.name].push(d.value || '');
            } else {
                o[d.name] = d.value || '';
            }
          }
          data = JSON.parse(JSON.stringify(o));
        }

        this.trigger(elm, 'api-prepare', { api: api });
        const method = (elm.getAttribute('method') || 'POST').toUpperCase();
        const res = method === 'GET' ? this.$api.get(api, data) : this.$api.post(api, data);
        ('form', method, api, data);
        
        return res.then((res) => {
            var result = res.data || res;
            if(elm.getAttribute('novalidate') === undefined)
                result = JSON.parse(result);

            this.trigger(elm, 'api-success', { api: api, result: result });
            return res;
        }).catch((err) => {
            this.trigger(elm, 'api-error', { api: api, result: err, error: err.error || err.message || err });
            return err;
        }).then((res) => {
            this.trigger(elm, 'api-done', res);
            this.$api.emitApi('form-submit-done', true);
        }).catch(console.error);
    }

    //-----------------------------------------------------
    
    install(Vue, options) {
        const $forms = this;
        this.$api = options.$api;

        Vue.prototype.$submitForm = function(form) {
            if(typeof(form) === 'string')
                form = this.$refs[form];

            $forms.handleFormSubmit(form);
        };

    }
}

export default new FormPoster();