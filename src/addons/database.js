import ApiManager from './apiManager';
import { server as ServerUrl } from '@config/server.json';
import $storage from './apiManager/storage';
import $translator from './translator';
var Format; /// = component/Formatter auto assigned

class DataBase {
    constructor() {
        this.summaries  = [];
        this.markets    = [];
        this.openOrders = [];
        this.portfolio  = [];
        this.subscriptions = {
            summaries: '*'
        };
        this.profile  = {
            first_name: '',
            last_name: '',
            sex: 'male',
        };
        this.notificationToken = false;
        this.__wallet   = 0;
        this.txCache = {};
        this.listen();

        this.verified = false;
        this.mainCurrency = 'USD';
        this.currencies = [];
        this.__summaryTime = 0;
    }

    toJSON() {
      const result = {};
      Object.entries(this).forEach(([key, value]) => {
        if (typeof value !== 'function')
          result[key] = value;
      });
      return result;
    }
    
    listen() {
        ApiManager.require('market/list', this.setMarkets.bind(this));

        ApiManager.on('market/summaries', (summaries) => {
            const d = new Date().getTime();
            if (this.__summaryTime < (d - 10000)) {
              this.__summaryTime = d;
              this.summaries = summaries;
            }
        });

        ApiManager.on('portfolio/list', (res) => {
            this.setPortfolio(res);
            if (this.wallet !== res.wallet) {
              this.wallet = res.wallet;
              //navigatorService.refreshApp();
            }
        });

        ApiManager.on('members/profile', (res) => {
            if (this.wallet !== res.wallet)
                this.wallet = res.wallet;

            this.verified = Boolean((res.profile || res).verified);
        });

        const currencyCb = (res) => {
          if (res.main) this.mainCurrency = res.main;
          if (res.currencies) this.currencies = res.currencies;
        };

        ApiManager.require('platform/currencies', { listOnly: true }, currencyCb);
        ApiManager.on('platform/currencies', currencyCb);

        const bindCurrency = (res) => {
            if (res.currency)
                this.mainCurrency = res.currency;
        }

        ApiManager.on('settings.verification/status', bindCurrency);
        ApiManager.on('members/connect', bindCurrency);

        ApiManager.onSocketOpen(() => this.updateSubscriptions(true));
    }

    //---------------
    
    get wallet() {
        return this.convertRate(this.__wallet);
    }
      
    get usdtWallet() {
        return (this.__wallet);
    }

    get walletFormatted() {
      return Format.price(this.usdtWallet, 2)
    }

    set wallet(wallet) {
        this.__wallet = Number(isNaN(wallet) ? 0 : wallet);
        ApiManager.emitApi("wallet", { wallet });
    }

    //---------------

    isPortrait() {
      const dim = Dimensions.get('screen');
      return dim.height >= dim.width;
    }

    get lang() {
        return $translator.lang;
    }

    async getLanguage() {
        await new Promise((resolve) => $storage.onReady(resolve));
        return await $translator.loadLang(true);
    }

    setLanguage(lang) {
        $translator.lang = lang;
    }

    //---------------

    getNotificationToken() {
      return this.notificationToken;
    }

    setNotificationToken(token) {
      this.notificationToken = token;
    }

    getFullName() {
      const profile = this.getProfile();
      return `${profile.first_name}${profile.first_name && ' '}${profile.last_name && profile.last_name[0]}${profile.last_name && '.'}`;
    }

    getProfile() {
      return this.profile;
    }

    setProfile(ApiResult) {
      if (ApiResult.first_name) this.profile.first_name = ApiResult.first_name;
      if (ApiResult.last_name) this.profile.last_name = ApiResult.last_name;
      if (ApiResult.sex) this.profile.sex = ApiResult.sex;
      if (ApiResult.connected !== undefined) this.profile.connected = ApiResult.connected;
      if (ApiResult.currency !== undefined) this.mainCurrency = ApiResult.currency;
    }

    //---------------

    getOpenOrders(market) {
        if(market)
            return this.openOrders.filter((obj) => obj.market === market);

        return this.openOrders;
    }

    setOpenOrders(list) {
        if (typeof list === 'object' && list !== null) {
          list = list.items || list;
        }
        if(!Array.isArray(list))
            return console.warn('Wrong open orders received ('+typeof(list)+');');

        this.openOrders = list;
    }

    //---------------
    
    convertRate(val, reverse) {
        if(this.currencies[this.mainCurrency]) {
          const rate = (this.currencies[this.mainCurrency].rate || 0);
          if (reverse) return Number(val / rate)
          return Number(val * rate);
        }

        return val;
    }
    
    getCurrencies() {
        return this.currencies;
    }

    setCurrency(currency) {
      if (this.mainCurrency !== currency) {
        this.mainCurrency = currency;
        ApiManager.post('settings.preferences/update-currency', {
          currency,
        });
      }
    }
    
    getCurrency() {
        const currencies = this.getCurrencies();
        return {
            name: this.mainCurrency,
            symbol: (currencies[this.mainCurrency] || { symbol: this.mainCurrency }).symbol,
            info: currencies[this.mainCurrency] || {}
        }
    }

    isVerified() {
      return this.verified;
    }

    //---------------

    refreshPortfolio() {
      ApiManager.post('portfolio/list', {})
      .catch((err) => {
          console.error(err);
      });
    }

    refreshMarkets(force = false) {
      if (force || this.markets.length === 0) {
        return ApiManager.post('market/list', {}).then((list) => {
            this.setMarkets(list);
            return list;
        }).catch((err) => {
            console.error('error loading market list', err);
        });
      }
      return new Promise((resolve) => {
        resolve(this.markets);
      });
    }

    forceRefreshMarkets() {
      return this.refreshMarkets(true);
    }

    setMarkets(list) {
        if(!Array.isArray(list))
            return console.warn('Wrong portfolio received ('+typeof(list)+');');

        const time = new Date().getTime();
        this.markets = list.map((item) => {
          item.lastUpdate = time;
          item.logo = ServerUrl + 'img/markets/' + (item.market || item.iso) + '.png';
          return item;
        });
    }

    getPrecision(market) {
        return (this.getMarket(market) || { precision: 2 }).precision;
    }

    getMarket(market, def = null) {
        return this.markets.find((obj) => obj.iso === market) || def;
    }

    getAsk(market, formatted, precision) {
      let summary;
      let n = Number(['USDT', 'USDT-USDT'].indexOf(market) > -1 ? 1 : market);
      if (isNaN(n)) n = (this.getSummary(market)).ask;
      if(formatted)
          return this.getRate(n, formatted, precision);

      return n;
    }

    getBid(market, formatted, precision) {
      let n = Number(['USDT', 'USDT-USDT'].indexOf(market) > -1 ? 1 : market);
      if (isNaN(n)) n = (this.getSummary(market)).bid;
      if(formatted)
          return this.getRate(n, formatted, precision);

      return n;
    }

    getUsdtRate(iso) {
        if(iso === 'USDT')
            return 1;

        var rate = this.getSummary('USDT-' + iso)
        if(rate.last > 0)
            return rate.last;

        var rate0 = this.getSummary('USDT-BTC');
        rate = this.getSummary('BTC-' + iso)
        if(rate.last > 0 && rate0.last > 0)
            return Math.round(rate.last * rate0.last * 100000000) / 100000000;

        rate0 = this.getSummary('USDT-ETH');
        rate = this.getSummary('ETH-' + iso)
        return Math.round(rate.last * rate0.last * 100000000) / 100000000;
    }

    getRate(marketParam, formatted, precision) {
        let market = String(marketParam);
        let last;
        const n = Number(['USDT', 'USDT-USDT'].indexOf(market) > -1 ? 1 : market);
        if (isNaN(n))
            last = (this.getSummary(market)).last;
        else
            last = n

        if (typeof formatted === 'function')
          last = formatted(last);

        if(!formatted || !precision)
            return this.convertRate(last);

        if (typeof precision === 'boolean') {
          precision = this.getCurrency().info.decimal_digits || 2;
        } else if (typeof precision == 'number') {
          precision = this.getPrecision(market);
        }

        return Format.price(last, precision);
    }

    getRateWithSymbol(market, formatted) {
      const rate = this.getRate(market, formatted);
      return `${this.getCurrency().symbol} ${rate}`;
    }

    getSummary(market) {
        if(market === 'USDT') {
            return {
                market:     market,
                ask:        1,
                bid:        1,
                high:       1,
                last:       1,
                low:        1,
                prevDay:    1,
                volume:     0
            }
        }

        if (String(market).indexOf('-') === -1) {
            throw(new Error('Wrong market provided ' + market))
        }

        const summary = this.summaries.find((obj) => obj.market === market) || {
            market:     market,
            ask:        0,
            bid:        0,
            high:       0,
            last:       0,
            low:        0,
            prevDay:    0,
            volume:     0
        };
        summary.iso = String(summary.market);
        // Changes made will not conflict with original summary. (Wallet bug)
        return Object.assign({}, summary);
    }

    //---------------

    deleteTxCache(txid) {
      delete this.txCache[txid];
    }

    setTxCache(txid, tx) {
      this.txCache[txid] = tx;
      return tx;
    }

    getTxCache(txid) {
      return this.txCache[txid] || null;
    }
    
    //---------------
    
    setPortfolio(portfolio) {
        if (typeof portfolio === 'object' && portfolio !== null) {
          portfolio = portfolio.items || portfolio;
        }
        if(!Array.isArray(portfolio))
            return console.warn('Wrong portfolio received ('+typeof(portfolio)+');');

        this.portfolio = portfolio.slice(0);
    }
    
    getPortfolio(market) {
        if(market)
            return this.portfolio.filter((obj) => obj.market === market);

        return this.portfolio;
    }
    
    //---------------


    subscribe(market, push = false) {
        this.subscriptions.history = this.subscriptions.history || [];
        this.subscriptions.history.push(market);
        this.updateSubscriptions();
    }
    
    unsubscribe(market) {
        this.subscriptions.history = (this.subscriptions.history || []).filter((obj) => obj !== market);
        this.updateSubscriptions();
    }

    updateSubscriptions(removeSubscriptions = true) {
        ApiManager.wsPost('subscribe', Object.assign({}, this.subscriptions, { removeSubscriptions })).then(() => {
            console.log('Subscribtions updated');
        }).catch((err) => {
            console.error(err);
        });
    }

    setFormatter(formatter) {
        Format = formatter;
    }
}

const db = new DataBase();
export default db;