import Vue from 'vue-native-core';
import Router from './phone/router';
import Installer from './install';

Vue.use(Router);
Vue.use(Installer);