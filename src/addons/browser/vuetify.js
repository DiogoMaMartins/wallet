import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import Colors from '@config/colors';

import fr from 'vuetify/src/locale/fr.ts';
import nl from 'vuetify/src/locale/nl.ts';
import en from 'vuetify/src/locale/en.ts';

Vue.use(Vuetify, {
  theme: Colors,
  // customProperties: true,
  iconfont: 'icon',
  icons: {
    lock: 'icon-lock',
    cancel: 'icon-close',
    menu: 'icon-menu',
    warning: 'icon-warning',
    error: 'icon-close',
    close: 'icon-close',
    delete: 'icon-trash',
    success: 'icon-check',
    dropdown: 'icon-angle-down',
    event: 'icon-calendar',
    calendar: 'icon-calendar',
    complete: 'icon-check',
    clear: 'icon-close',
    info: 'icon-info',
    prev: 'icon-angle-left',
    next: 'icon-angle-right',
    sort: 'icon-angle-up',
    checkboxOn: 'icon-checkbox-checked',
    checkboxOff: 'icon-checkbox-unchecked',
    expand: 'icon-arrows-expand',
    radioOn: 'icon-dot-circle-o',
    radioOff: 'icon-circle-o',
    edit: 'icon-write',
    /*
    'checkboxIndeterminate': '',
    'delimiter': '', // for carousel
    'subgroup': '',
    */
  },
  lang: {
    locales: { fr, nl, en },
  },
});