import * as Sentry from "@sentry/browser";

if (typeof(window) !== 'undefined' && process.env.NODE_ENV === "production") {
    try {
        Sentry.init({
            dsn: "https://f5d773d4238b43658d1e08ca199212e0@sentry.io/1275746",
            integrations: [new Sentry.Integrations.Vue({
                Vue
            })],
            //release: "v" + Version.version
        });
    } catch (e) {
        console.error("[Sentry]", "Could not be loaded", e);
    }

    try {
        (function (h, o, t, j, a, r) {
            h.hj =
                h.hj ||
                function () {
                    (h.hj.q = h.hj.q || []).push(arguments);
                };
            h._hjSettings = {
                hjid: 1016037,
                hjsv: 6
            };
            a = o.getElementsByTagName("head")[0];
            r = o.createElement("script");
            r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
        })(window, document, "https://static.hotjar.com/c/hotjar-", ".js?sv=");
    } catch (e) {
        console.error("[Hotjar]", "Could not be loaded", e);
    }
}