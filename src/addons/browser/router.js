import Vue from 'vue';
import Router from 'vue-router';
import ErrorPage from '@pages/error/_index.vue';
import HomePage from '@pages/home/_index.vue';
import serializeProperites from './pageProperties';

var routes = [];
// <ROUTING>
// push as last element because the wildcard match will catch all the unknown urls
function transformRoutes(oldRoutes) {
    const routes = [];

    for(var route of oldRoutes) {
        const props = serializeProperites(route.component);
        if(props.length === 0) {
            routes.push(route);
            continue;
        }

        for(var prop of props) {
            routes.push({
                path: route.path + prop,
                meta: route.meta,
                component: route.component,
                props: (props !== '')
            })
        }
    }

    return routes
}

routes.push({ path: '/', component: HomePage, meta: { status: 401 }});
routes.push({ path: '*', component: ErrorPage, meta: { status: 404 }});
routes = transformRoutes(routes);
Vue.use(Router);

var router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior(to, from, savedPosition) {
      if (to.hash) {
          return {
            selector: to.hash
          }
      }
  
      return { x: 0, y: 0 }
  }
});

export default router;

global = global || {};
global.$router = router;

export { router };