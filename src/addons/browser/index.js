import './vuetify.js';
import './sentry.js';
import './outdated';
import '@menu/backend/mixins/tabs.js';

import Vue from 'vue';
import Clipboard from './clipboard';
import Translator from '@addons/translator';

Vue.prototype.$phone = false;
Vue.use(Clipboard);

Translator.extends({
    en: require('@locales/backend/en.json'),
    fr: require('@locales/backend/fr.json'),
    nl: require('@locales/backend/nl.json'),
})