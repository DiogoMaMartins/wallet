import Vue from 'vue';
import VueAnalytics, { onAnalyticsReady } from "vue-analytics";
const DEBUG_GA = false;

global.$ga = function() {
    try {
        if (typeof(window) !== 'undefined') {
            if(!window.ga)
                setTimeout(() => $ga.apply(global, arguments), 250);
            else
                ga.apply(ga, arguments)
        }
    } catch(e) {
        console.error(e);
    }

    return global.$ga
}

export default (router) => {
  if (typeof(window) !== 'undefined') {
    try {
      Vue.use(VueAnalytics, {
          id: (process.env.NODE_ENV === 'production'
            ? 'UA-125332525-1' // Production app
            : 'UA-125332525-3' // Localhost app
          ),
          router: router,
          disabled: false,
          autoTracking: {
              screenview: false,
              exception: true,
              exceptionLogs: true,
              skipSamePath: false,
              page: true,
              transformQueryString: true,
              pageviewOnLoad: true,
              prependBase: false
          },
          linkers: (process.env.NODE_ENV === 'production' 
            ? [
                'www.bit4you.io',
                'bit4you.io',
                'www.bit4you.be',
                'bit4you.be',
            ] : [
              'test.bit4you.io',
              '0.0.0.0'
            ]
          ),
          debug: (process.env.NODE_ENV !== 'production' && DEBUG_GA
            ? {
              enabled: true,
              trace: true,
              sendHitTask: true,
            } : {
              enabled: false,
            }
          ),
          ready() {
              Vue.$ga.enable();
              Vue.$ga.enabled = true;
          },
      });

      onAnalyticsReady().then(() => {
          ga('require', 'ec');
      })
    } catch (e) {
      console.error("[Google Analytics]", "Could not be loaded", e);
    }
  }
};
