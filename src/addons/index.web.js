import Vue from 'vue';
import Installer from './install';
import Meta from 'vue-meta';

Vue.use(Meta);
Vue.use(Installer);