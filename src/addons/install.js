import colors from '@config/colors';
import Translator from './translator';
import ApiManager from './apiManager';
import Currencies from './currencies';
import Database from './database';
import Forms from './formSubmitter';
import Filters from './filters';
import { FormatterSetup } from './formatter';

export default {
    install(Vue, options) {
        Vue.prototype.$colors = Vue.prototype.$colors || colors;
        Vue.prototype.$db = Vue.prototype.$db || Database;
        Vue.prototype.$format = Vue.prototype.$format || FormatterSetup(Database, Translator);

        Vue.use(ApiManager);
        Vue.use(Filters);
        Vue.use(Translator);
        Vue.use(Currencies, {
            $db: Database,
            $api: ApiManager,
        });
        Vue.use(Forms, {
            $api: ApiManager
        });
    }
}