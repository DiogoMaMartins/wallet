function getDate(date) {
    const gmt = 0; // 2 * 3600; //2h
    return typeof date === "string" ?
        new Date(date) :
        new Date((date + gmt) * 1000);
}

function formatDate(date) {
    var d = getDate(date),
        yyyy = d.getFullYear(),
        dd = ("0" + d.getDate()).slice(-2),
        time;
    var month = new Array();
    month[0] = "Jan";
    month[1] = "Feb";
    month[2] = "Mar";
    month[3] = "Apr";
    month[4] = "May";
    month[5] = "Jun";
    month[6] = "Jul";
    month[7] = "Aug";
    month[8] = "Sep";
    month[9] = "Oct";
    month[10] = "Nov";
    month[11] = "Dec";
    var mm = month[d.getMonth()],
        time = dd + " " + mm + ". " + yyyy;

    return time;
}

function formatTime(date, withSeconds) {
    const d = getDate(date);
    var hours = d.getHours(),
        minutes = d.getMinutes(),
        seconds = d.getSeconds(),
        time =
        (hours < 10 ? "0" + hours : hours) +
        ":" +
        (minutes < 10 ? "0" + minutes : minutes);

    if (withSeconds) time = time + ":" + (seconds < 10 ? "0" + seconds : seconds);

    return time;
}

function formatDateTime(date, withSeconds) {
    return formatDate(date) + " " + formatTime(date, withSeconds);
}

export default {
    install(Vue, options) {
        Vue.mixin({
            filters: {
                ucwords(val = "") {
                    return String(val)
                        .toLowerCase()
                        .replace(/\b[a-z]/g, letter => {
                            return letter.toUpperCase();
                        });
                },

                padTime(number) {
                    number = Number(number);
                    return number < 10 ? `0${number}` : number;
                },

                dateTransform: function (inDate) {
                    if (inDate === undefined) return "";
                    if (inDate.length === 10)
                        return (
                            inDate.substr(8, 2) +
                            "/" +
                            inDate.substr(5, 2) +
                            "/" +
                            inDate.substr(0, 4)
                        );
                    return inDate;
                },

                formatDate: function (date) {
                    return formatDate(date);
                },

                formatDateTime: function (date) {
                    return formatDateTime(date);
                },

                formatDateTimeSec: function (date) {
                    return formatDateTime(date, true);
                },

                formatTime: function (date) {
                    return formatTime(date);
                },
                formatTimeSec: function (date) {
                    return formatTime(date, true);
                }
            },

            methods: {
                $translateHeaders(headers) {
                    return headers.map(obj => {
                        if (!obj.originalText) obj.originalText = obj.text;

                        if (obj.originalText && obj.originalText.length > 0)
                            obj.text = this.$translate(obj.originalText);

                        return obj;
                    });
                }
            }
        });
    }
}