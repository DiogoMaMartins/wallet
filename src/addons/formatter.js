var Database;
var Translator;

function getDate(date) {
    const gmt = 0;// 2 * 3600; //2h
    return (typeof date === 'string' ? new Date(date) : new Date((date + gmt) * 1000));
}

function getMonth(date) {
    var d = getDate(date);
    var month = new Array();
    month[0] = Translator.$("Jan");
    month[1] = Translator.$("Feb");
    month[2] = Translator.$("Mar");
    month[3] = Translator.$("Apr");
    month[4] = Translator.$("May");
    month[5] = Translator.$("Jun");
    month[6] = Translator.$("Jul");
    month[7] = Translator.$("Aug");
    month[8] = Translator.$("Sep");
    month[9] = Translator.$("Oct");
    month[10] = Translator.$("Nov");
    month[11] = Translator.$("Dec");
    return month[d.getMonth()];
}

function formatDate(date) {
    var d = getDate(date),
        yyyy = d.getFullYear(),
        dd = ('0' + d.getDate()).slice(-2),
        time;

    var mm = getMonth(date),
        time = dd + ' ' + mm + '. ' + yyyy;

    return time;
}

function formatTime(date, withSeconds) {
    const d = getDate(date);
    var hours   = d.getHours(),
        minutes = d.getMinutes(),
        seconds = d.getSeconds(),
        time = (hours < 10 ? '0' +hours : hours) + ':' + (minutes < 10 ? '0' +minutes : minutes);

    if(withSeconds)
        time = time + ':' + (seconds < 10 ? '0' +seconds : seconds);

    return time;
}

function formatDateTime(date, withSeconds) {
    return formatDate(date) + ' ' + formatTime(date, withSeconds);
}

function transformInput(input, name, def) {
    if(!input)
        return def;

    return input[name] || input;
}

const formatter = {
    change(input, showPlus = true) {
        input = transformInput(input, 'change', 0);
        //String(item.change).replace(/[^\d.,]/g, '')
        if(typeof(input) !== 'number')
            input = parseFloat(Number) || 0;

        return (input >= 0 && showPlus ? '+' : '') + (input || 0).toFixed(2) + (showPlus ? ' %' : '');
    },

    currency(input, precision, showPlus) {
        precision = precision !== 0 ? (precision || (input ? input.precision : null)) : 0;
        var n     = transformInput(input, 'value', 0);

        if(!precision && precision !== 0)
            precision = 2;

        if(typeof(input) !== 'number')
            input = parseFloat(Number) || 0;

        var c = precision,
            d = ".",
            t = " ",
            s = n < 0 ? "-" : (showPlus ? '+' : ''),
            i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
            j = (j = i.length) > 3 ? j % 3 : 0;

        return Database.getCurrency().symbol + ' ' + s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
  
    price(input, precision, showPlus) {
        precision = precision !== 0 ? (precision || (input ? input.precision : null)) : 0;
        var n     = transformInput(input, 'value', 0);
        n = Database.getRate(n);

        if(!precision && precision !== 0)
            precision = 2;

        if(typeof(input) !== 'number')
            input = parseFloat(Number) || 0;

            var c = precision,
                d = ".",
                t = " ",
                s = n < 0 ? "-" : (showPlus ? '+' : ''),
                i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
                j = (j = i.length) > 3 ? j % 3 : 0;
               
            return Database.getCurrency().symbol + ' ' + s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
    
    getDate,
    getMonth,
    
    date(input) {
        return formatDate(transformInput(input, 'time', 0));
    },
    
    dateTime(input) {
        return formatDateTime(transformInput(input, 'time', 0));
    },
    
    dateTimeSec(input) {
        return formatDateTime(transformInput(input, 'time', 0), true);
    },
    
    time(input) {
        return formatTime(transformInput(input, 'time', 0));
    },
    
    timeSec(input) {
        return formatTime(transformInput(input, 'time', 0), true);
    },

    phone(p) {
        //TODO format phone number with spaces
        return p
    }
};

export default formatter;

export function FormatterSetup(_Database, _Translator) {
  Database = _Database;
  Translator = _Translator;
  _Database.setFormatter(formatter);
  return formatter;
};
