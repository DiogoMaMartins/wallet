FROM node:11-alpine as builder

WORKDIR /var/www

COPY package.json /var/www/package.json
COPY src/dev_modules /var/www/src/dev_modules
COPY src/browser /var/www/src/browser
RUN cd /var/www/src/browser && npm install

COPY chart /var/www/chart
COPY src /var/www/src
COPY assets /var/www/assets
COPY ui /var/www/ui

# Build with all dev dependencies installed,
RUN npm run build
CMD npm run prod