- hitbtc orderbook abnormal spread




Setting:
========
- Force real mode & verified on required setting pages.

- Settings withdraw:
  //TODO on euro field edit => set USDT gray background/euro white background & visa versa
  //TODO autofill recommended bank account & swift based on last detected account
  //TODO set swift not mandadoty

- Notifications popups
- Colors (btn's etc) server side reload

Error sha1 fix (replace throw in package file throwing error)
=============================================================
...
file: metro/src/node-hast/DependencyGraph.js

const crypto = require('crypto')
const shasum = crypto.createHash('sha1');
shasum.update(fs.readFileSync(resolvedPath, { encoding: 'UTF-8' }));
return shasum.digest('hex');
...

Auto Calculate BIC from IBAN
==================================
https://www.nbb.be/doc/be/be/protocol/r_list_of_codes_current.pdf
https://www.npmjs.com/package/bic-from-iban